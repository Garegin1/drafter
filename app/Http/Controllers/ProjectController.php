<?php

namespace App\Http\Controllers;

use App\Models\Bookmark;
use App\Models\Business;
use App\Models\Category;
use App\Models\City;
use App\Models\Company;
use App\Models\Image;
use App\Models\Investment;
use App\Models\Region;
use App\Models\Survey;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Console\Input\Input;
use function GuzzleHttp\Promise\all;


class ProjectController extends Controller
{
    /**
     * @return string
     */
    public function index(Request $request)
    {



        if ( $request->region_search){

            $sql = "SELECT `cities`.id as id,`cities`.name as name,`regions`.region_name as region_name, `regions`.id as region_id  FROM `cities` JOIN `regions` ON `cities`.`region_id` = `regions`.id WHERE `cities`.name LIKE '%$request->region_search%' OR `regions`.region_name LIKE '$request->region_search%'";
            $city = DB::select($sql);
            $arr = [];


            foreach ($city as $cit) {

                $arr["$cit->region_id"][0] = [
                    'region_id' => $cit->region_id,
                    'region_name' => $cit->region_name,
                ];

                $arr["$cit->region_id"][] = [
                    'id' => $cit->id,
                    'region_id' => $cit->region_id,
                    'region_name' => $cit->region_name,
                    'name' => $cit->name,
                ] ;
            }

//            dd($arr);


            return response()->json($arr) ;

        }


        if ($request['category'] || $request->search || $request->orderBy || $request['cities']) {

            if ($request->orderBy && empty($request->search) && empty($request['category'])) {

                if ($request->orderBy == 1) {
                    $orderBy = $request->orderBy;
                    $categories = Category::all();
                    $surveys = Survey::all();
                    $region = Region::all();
                    $city = City::all();
                    $investment = Investment::all();
                    $results = $investment->concat($surveys);
                    $results = $results->sortByDesc('created_at');
                    $count = $results->count();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));

                    $images = Image::all();
                    return view('projects', compact('city','region','city','region','surveys', 'categories', 'results', 'images','orderBy'));
                }
                if ($request->orderBy == 2) {
                    $orderBy = $request->orderBy;
                    $categories = Category::all();
                    $surveys = Survey::orderBy('price')->get();
                    $region = Region::all();
                    $city = City::all();
//                    $investment = Investment::orderBy('am_required')->get();
                    $results = $surveys;
//                    $results = $results->sortBy('price');
                    $count = $results->count();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $images = Image::all();
                    return view('projects', compact('city','region','surveys', 'categories', 'results', 'images','orderBy'));
                }
                if ($request->orderBy == 3) {
                    $orderBy = $request->orderBy;
                    $categories = Category::all();
                    $region = Region::all();
                    $city = City::all();
                    $surveys = Survey::orderByDesc('price')->get();;
//                    $investment = Investment::orderBy('am_required')->get();
                    $results = $surveys;
//                    $results = $results->sortBy('price');
                    $count   = $results->count();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $images  = Image::all();

                    return view('projects', compact('city','region','surveys', 'categories', 'results', 'images','orderBy'));
                }
                if ($request->orderBy == 4) {
                    $orderBy = $request->orderBy;
                    $categories = Category::all();
                    $surveys = Survey::all();
                    $region = Region::all();
                    $city = City::all();
                    $investment = Investment::all();
                    $results = $investment->concat($surveys);
                    $results = $results->sortByDesc('created_at');
                    $count = $results->count();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));

                    $images = Image::all();
                    return view('projects', compact('city','region','surveys', 'categories', 'results', 'images','orderBy'));
                }
                if ($request->orderBy == 5) {
                    $orderBy = $request->orderBy;
                    $categories = Category::all();
                    $investment = Investment::orderByDesc('am_required')->get();
                    $results = $investment; $region = Region::all();
                    $city = City::all();
                    $results = $results->sortByDesc('am_required');
                    $count = $results->count();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $images = Image::all();
                    return view('projects', compact('city','region','categories', 'results', 'images','orderBy'));
                }
            }

//            if ($request['region']) {
//                $orderBy = $request->orderBy;
//                $regions = $request['region'];
//                $region = Region::all();
//                $city = City::all();
//                $surveys =  Survey::whereIn('region_id',$regions)->get();
////                $investments = Investment::whereIn('cat_id', $category_ids)->get();
//                $results = $surveys;
//                $results = $results->sortByDesc('created_at');
//                $count = $results->count();
//                $categories = Category::all();
//                $results = $results->forPage($request->get('page', 1), 8)->toArray();
//                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
//                $images = Image::all();
//                $old_path = $_SERVER['QUERY_STRING'];
//
//                return view('projects', compact('city','regions','region','surveys', 'categories', 'results', 'images','orderBy'));
//            }

            if ($request['cities'] && empty($request->search)) {

                $orderBy = $request->orderBy;
                $city_id = $request['cities'];

                $city_id = explode(',', $city_id);
                $region = Region::all();
                $city = City::all();
                $surveys =  Survey::whereIn('city_id',$city_id)->get();
//                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                $results = $surveys;
                $results = $results->sortByDesc('created_at');
                $count = $results->count();
                $categories = Category::all();
                $results = $results->forPage($request->get('page', 1), 8)->toArray();
                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                $images = Image::all();
                $old_path = $_SERVER['QUERY_STRING'];

                return view('projects', compact('city','city_id','region','surveys', 'categories', 'results', 'images','orderBy'));
            }
            if ($request['cities'] && empty($request['category'])) {

                $orderBy = $request->orderBy;
                $city_id = $request['cities'];

                $city_id = explode(',', $city_id);
                $region = Region::all();
                $city = City::all();
                $surveys =  Survey::whereIn('city_id',$city_id)->get();
//                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                $results = $surveys;
                $results = $results->sortByDesc('created_at');
                $count = $results->count();
                $categories = Category::all();
                $results = $results->forPage($request->get('page', 1), 8)->toArray();
                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                $images = Image::all();
                $old_path = $_SERVER['QUERY_STRING'];

                return view('projects', compact('city','city_id','region','surveys', 'categories', 'results', 'images','orderBy'));
            }

            if ($request['cities'] && $request->search) {

                $orderBy = $request->orderBy;
                $city_id = $request['cities'];
                $search = $request->search;
                $city_id = explode(',', $city_id);
                $region = Region::all();
                $city = City::all();
                $surveys =  Survey::whereIn('city_id',$city_id)->where('name', 'LIKE', "%$search%")->get();
//                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                $results = $surveys;
                $results = $results->sortByDesc('created_at');
                $count = $results->count();
                $categories = Category::all();
                $results = $results->forPage($request->get('page', 1), 8)->toArray();
                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                $images = Image::all();
                $old_path = $_SERVER['QUERY_STRING'];

                return view('projects', compact('city','city_id','search','region','surveys', 'categories', 'results', 'images','orderBy'));
            }

            if ($request['cities'] && $request['category']) {

                $orderBy = $request->orderBy;
                $city_id = $request['cities'];
                $category_ids = $request['category'];
                $city_id = explode(',', $city_id);
                $region = Region::all();
                $city = City::all();
                $surveys =  Survey::whereIn('city_id',$city_id)->whereIn('field_of_activity', $category_ids)->get();
//                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                $results = $surveys;
                $results = $results->sortByDesc('created_at');
                $count = $results->count();
                $categories = Category::all();
                $results = $results->forPage($request->get('page', 1), 8)->toArray();
                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                $images = Image::all();
                $old_path = $_SERVER['QUERY_STRING'];

                return view('projects', compact('city','city_id','region','surveys', 'categories', 'results', 'images','orderBy'));
            }

            if ($request['cities'] && $request['category'] && $request->search) {

                $orderBy = $request->orderBy;
                $city_id = $request['cities'];
                $search = $request->search;
                $category_ids = $request['category'];
                $city_id = explode(',', $city_id);
                $region = Region::all();
                $city = City::all();
                $surveys =  Survey::whereIn('city_id',$city_id)->whereIn('field_of_activity', $category_ids)->where('name', 'LIKE', "%{$search}%")->get();
//                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                $results = $surveys;
                $results = $results->sortByDesc('created_at');
                $count = $results->count();
                $categories = Category::all();
                $results = $results->forPage($request->get('page', 1), 8)->toArray();
                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                $images = Image::all();
                $old_path = $_SERVER['QUERY_STRING'];

                return view('projects', compact('city','city_id','region','surveys', 'categories', 'results', 'images','orderBy'));
            }


            if ($request['category'] && empty($request->search)) {
                $orderBy = $request->orderBy;
                $category_ids = $request['category'];
                $region = Region::all();
                $city = City::all();
                $surveys = Survey::whereIn('field_of_activity', $category_ids)->get();
                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                $results = $investments->concat($surveys);
                $results = $results->sortByDesc('created_at');
                $count = $results->count();
                $categories = Category::all();
                $results = $results->forPage($request->get('page', 1), 8)->toArray();
                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                $images = Image::all();
                $old_path = $_SERVER['QUERY_STRING'];

                return view('projects', compact('city','region','surveys', 'categories', 'results', 'images', 'category_ids','orderBy'));
            }

            if ($request->search && empty($request['category'])) {

                $request->validate([
                    'search' => 'required'
                ]);
                $orderBy = $request->orderBy;
                $search = $request->search;
                $region = Region::all();
                $city = City::all();
                $categories = Category::all();
                $surveys = Survey::where('name', 'LIKE', "%{$search}%")->get();
                $investment = Investment::where('company_name', 'LIKE', "%{$search}%")->get();
                $results = $investment->concat($surveys);
                $results = $results->sortByDesc('created.at');
                $count = $results->count();
                $results = $results->forPage($request->get('page', 1), 8)->toArray();
                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                $old_path = $_SERVER['QUERY_STRING'];
                $images = Image::all();

                return view('projects', compact('city','region','surveys', 'categories', 'results', 'images','search','orderBy'));
            }

            if ($request['category'] && $request->search) {

                $orderBy = $request->orderBy;
                $search = $request->search;
                $region = Region::all();
                $city = City::all();
                $category_ids = $request['category'];
                $surveys = Survey::whereIn('field_of_activity', $category_ids)->where('name', 'LIKE', "%{$search}%")->get();
                $investments = Investment::whereIn('cat_id', $category_ids)->where('company_name', 'LIKE', "%{$search}%")->get();
                $results = $investments->concat($surveys);
                $results = $results->sortByDesc('created_at');
                $count = $results->count();
                $categories = Category::all();
                $results = $results->forPage($request->get('page', 1), 8)->toArray();
                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                $images = Image::all();

                $old_path = $_SERVER['QUERY_STRING'];

                return view('projects', compact('city','region','surveys', 'categories', 'results', 'images', 'search', 'category_ids','orderBy'));

            }

        } else {
            $orderBy = $request->orderBy;
            $categories = Category::all();
            $city = City::all();
            $region = Region::all();
            $category_ids = $request['category'];
            $investments = Investment::all();
            $surveys = Survey::all();
            $businesses = Business::all();
            $images = Image::all();
            $results = $investments->concat($surveys);
            $results = $results->sortByDesc('created_at');
            $count = $results->count();
            $results = $results->forPage($request->get('page', 1), 8)->toArray();
            $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
            return view('projects', compact('city','region','businesses', 'categories', 'results', 'images','category_ids','orderBy'));
        }


//        if ($request->orderBy) {
//            if ($request->orderBy == 1) {
//                $search = $request->search;
//                $categories = Category::all();
//                $surveys = Survey::all()->orderBy('Asc');
//                $investment = Investment::where('company_name', 'LIKE', "%{$search}%")->get();
//                $results = $investment->concat($surveys);
//                $results = $results->sortByDesc('category');
//                $count = $results->count();
//                $results = $results->forPage($request->get('page', 1), 3)->toArray();
//                $results = new LengthAwarePaginator($results, $count, 3, $request->get('page', 1));
//
//                $images = Image::all();
//                return view('projects', compact('surveys', 'categories', 'results', 'images'));
//            }
//            if ($request->orderBy == 2) {
//                $categories = Category::all();
//                $surveys = Survey::orderBy('price')->get();
//                $investment = Investment::orderBy('am_required')->get();
//                $results = $investment->concat($surveys);
////                $results = $results->sortBy('price')->sortBy('am_required');
//                $count = $results->count();
//                $results = $results->forPage($request->get('page', 1), 3)->toArray();
//                $results = new LengthAwarePaginator($results, $count, 3, $request->get('page', 1));
//                $images = Image::all();
//                return view('projects', compact('surveys', 'categories', 'results', 'images'));
//            }
//            if ($request->orderBy == 3) {
//                $categories = Category::all();
//                $surveys = Survey::orderByDesc('price')->get();
//                $investment = Investment::orderByDesc('am_required')->get();
//                $results = $investment->concat($surveys);
//                $results = $results->sortByDesc('price');
//                $count = $results->count();
//                $results = $results->forPage($request->get('page', 1), 3)->toArray();
//                $results = new LengthAwarePaginator($results, $count, 3, $request->get('page', 1));
//                $images = Image::all();
//                return view('projects', compact('surveys', 'categories', 'results', 'images'));
//            }
//            if ($request->orderBy == 4) {
//
//            }
//            if ($request->orderBy == 5) {
//                $categories = Category::all();
////              $surveys = Survey::orderByDesc('price')->get();
//                $investment = Investment::orderByDesc('am_required')->get();
//                $results = $investment;
////              $results = $results->sortByDesc('price');
//                $count = $results->count();
//                $results = $results->forPage($request->get('page', 1), 3)->toArray();
//                $results = new LengthAwarePaginator($results, $count, 3, $request->get('page', 1));
//                $images = Image::all();
//                return view('projects', compact('categories', 'results', 'images'));
//            }
//        }


    }

    public function get_filters_path(Request $request)
    {
        $ssl = isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == '127.0.0.1' ? 'http://' : 'https://';
        $request->old_path = str_replace($ssl . $_SERVER['HTTP_HOST'] . '/', '', $_SERVER['HTTP_REFERER']);
        $old_path          = !empty($request->old_path) ? $request->old_path : '/?';





        if (!empty($request->city)) {

            $citys = implode(',', $request->city);


            $parsed = parse_url($request->old_path);

            if (isset($parsed['query'])) {

                $query = $parsed['query'];
                parse_str($query, $params);

                unset($params['cities']);
                $string = http_build_query($params);
                $old_path = '?' . $string . "&cities=" . $citys;

            } else {

                $old_path = "?cities=" . $citys;

            }


        }

//            for ($i = 0; $i < count($citys); $i++) {
//
//                if (!empty($request->old_path)) {
//
//                    $find = strpos($old_path, "city[]=" . $request->city[$i]);
//                    $find2 = strpos($old_path, "city=" . $request->city[$i]);
//                    $find3 = strpos($old_path, "city=" . $request->city[$i]);
//                    $find4 = strpos($old_path, "city=" . $request->city[$i]);
//                    $find5 = strpos($old_path, "city=" . $request->city[$i]);
//
//                    if ( $find === false && $find2 === false && $find3 === false && $find4 === false && $find5 === false  ) {
//
//                        $old_path .= "&city[]=" . $request->city[$i];
//
//                    }
//
//
//                } else {
//
//                    if (count($request->city) - 1 == $i) {
//
//                        $old_path .= "city[]=" . $request->city[$i];
//
//                    } else {
//                        $old_path .= "city[]=" . $request->city[$i] . "&";
//
//                    }
//
//                }
//
//            }


//            $parsed = parse_url($old_path);
//            $query = $parsed['query'];
//
//            parse_str($query, $params);
//            unset($params['page']);
//
//            $old_path = '?'.http_build_query($params);


//        }else{
//
//            if($request->city) {
//                $parsed = parse_url($request->old_path);
//                $query = $parsed['query'];
//                parse_str($query, $params);
//                unset($params['city']);
//                unset($params['page']);
//                $string = http_build_query($params);
//                $old_path = '?'.$string;
//
//
//            }
//        }












        if (!empty($request->region)) {

            for ($i = 0; $i < count($request->region); $i++) {

                if (!empty($request->old_path)) {

                    $find = strpos($old_path, "region[]=" . $request->region[$i]);
                    $find2 = strpos($old_path, "region=" . $request->region[$i]);
                    $find3 = strpos($old_path, "region=" . $request->region[$i]);
                    $find4 = strpos($old_path, "region=" . $request->region[$i]);
                    $find5 = strpos($old_path, "region=" . $request->region[$i]);

                    if ( $find === false && $find2 === false && $find3 === false && $find4 === false && $find5 === false  ) {

                        $old_path .= "&region[]=" . $request->region[$i];

                    }


                } else {

                    if (count($request->region) - 1 == $i) {

                        $old_path .= "region[]=" . $request->region[$i];

                    } else {
                        $old_path .= "region[]=" . $request->region[$i] . "&";

                    }

                }

            }


            $parsed = parse_url($old_path);
            $query = $parsed['query'];

            parse_str($query, $params);
            unset($params['page']);

            $old_path = '?'.http_build_query($params);


        }else{

            if($request->region) {

                $parsed = parse_url($request->old_path);
                $query = $parsed['query'];
                parse_str($query, $params);
                unset($params['region']);
                unset($params['page']);
                $string = http_build_query($params);
                $old_path = '?'.$string;


            }
        }








        if (!empty($request->category)) {

            for ($i = 0; $i < count($request->category); $i++) {

                if (!empty($request->old_path)) {

                    $find = strpos($old_path, "category[]=" . $request->category[$i]);
                    $find2 = strpos($old_path, "category%5B0%5D=" . $request->category[$i]);
                    $find3 = strpos($old_path, "category%5B1%5D=" . $request->category[$i]);
                    $find4 = strpos($old_path, "category%5B2%5D=" . $request->category[$i]);
                    $find5 = strpos($old_path, "category%5B3%5D=" . $request->category[$i]);

                    if ( $find === false && $find2 === false && $find3 === false && $find4 === false && $find5 === false  ) {

                        $old_path .= "&category[]=" . $request->category[$i];

                    }


                } else {

                    if (count($request->category) - 1 == $i) {

                        $old_path .= "category[]=" . $request->category[$i];

                    } else {
                        $old_path .= "category[]=" . $request->category[$i] . "&";

                    }

                }

            }


            $parsed = parse_url($old_path);
            $query = $parsed['query'];

            parse_str($query, $params);
            unset($params['page']);

            $old_path = '?'.http_build_query($params);


        }else{

            if(isset($request->action) && $request->action == 'category_form') {
                $parsed = parse_url($request->old_path);
                $query = $parsed['query'];
                parse_str($query, $params);
                unset($params['category']);
                unset($params['page']);
                $string = http_build_query($params);
                $old_path = '?'.$string;


            }
        }
        if (!empty($request->search)) {
            if (!empty($request->old_path)) {


                $x = $request->old_path;
                $parsed = parse_url($x);
                $query = $parsed['query'];
                parse_str($query, $params);
                unset($params['search']);
                unset($params['page']);
                $string = http_build_query($params);
                $params['search'] = '?' . $string . "&search=" . $request->search;
                $old_path = $params['search'];

            } else {
                $old_path .= "search=" . $request->search;
            }
        }
        if (!empty($request->orderBy)) {


            if (!empty($request->old_path)) {

                $x = $request->old_path;
                $parsed = parse_url($x);
                $query = $parsed['query'];
                parse_str($query, $params);
                unset($params['orderBy']);
                unset($params['page']);
                $string = http_build_query($params);
                $params['orderBy'] = '?' . $string . "&orderBy=" . $request->orderBy;
                $old_path = $params['orderBy'];


                    $old_path .= "&search=".$request->search;
            } else {
                $old_path .= "&orderBy=" . $request->orderBy;
            }
        }


//        dd($request->all(), $request->old_path, $old_path);


        return redirect($old_path);


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToBookmarks(Request $request)
    {
        if ($request->add == true) {
            Auth::user()->bookmarks()->create(['add_id' => $request->id, 'add_type' => $request->type]);
            return response()->json('success', '200');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove_bookmark(Request $request)
    {

        if ($request->remove == true) {
            $bookmark = Auth::user()->bookmarks()
                ->where('add_id', $request->id)
                ->where('add_type', $request->type)
                ->first();

            Auth::user()->bookmarks()->detach($bookmark->id);
            Bookmark::find($bookmark->id)->delete();
            return response()->json('success', '200');
        }
    }

    /**
     * @param $addType
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create($addType)
    {
        $categories = DB::table('categories')->get();
        $city = City::all();
        $region = Region::all();
        if ($addType == "business") {
            Session::forget('images');
            return view('create_add_survey', compact('categories','city','region'));
        } else {
            return view('create_add', compact('addType', 'categories'));
        }

    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function chooseType()
    {
        return view('add');
    }

    /**
     * @param $name
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function single($name, $id)
    {

        if ($name == 'investment') {
            $investment = Investment::find($id);
            $add_images = Image::query()
                ->where('add_id', "=", $id)
                ->where('add_type', "=", $name)
                ->get();
            return view('add_single_page', compact('investment', 'add_images'));
        } elseif ($name == 'business') {
            $business = Survey::find($id);
            $add_images = Image::query()
                ->where('add_id', "=", $id)
                ->where('add_type', "=", $name)
                ->get();
            return view('add_single_page', compact('business', 'add_images'));
        }
    }


    public function contactUs($name, $id, $user_id)
    {

        if ($name == 'investment') {
            $name = 'investment';
            $investment = Investment::find($id);
            $user = User::find($id);
            return view('contact_us', compact('investment', 'user', 'name'));
        } elseif ($name == 'business') {
            $user = User::find($id);
            $business = Survey::find($user_id);
            $name = 'business';
            return view('contact_us', compact('business', 'name', 'user'));
        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function myProjectsShow()
    {
        $categories = Category::all();
        $invests = Auth::user()->investedTo()->select('invests.investment_id')->groupBy('invests.investment_id')->with('investment')->get();
        $businesses = Auth::user()->businesses;
        return view('my_projects', compact('categories', 'invests', 'businesses'));
    }

    public function showInvestors($id)
    {
        $investors = Investment::find($id)->invests()->with('user')->get();
        return view('investors', compact('investors'));
    }

    public function filter(Request $request)
    {
        $arr = [];
        $data = $request->all();
        foreach ($data as $value) {
            $arr[] = Business::where('cat_id', $value)->get();
            $arr[] = Investment::where('cat_id', $value)->get();
        }

    }

}
