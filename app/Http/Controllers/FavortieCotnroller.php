<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\Category;
use App\Models\City;
use App\Models\Image;
use App\Models\Investment;
use App\Models\Region;
use App\Models\Survey;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class FavortieCotnroller extends Controller
{
    public function view(Request $request){


            if ($request['category'] || $request->search || $request->orderBy || $request['cities']) {

                if ($request->orderBy && empty($request->search) && empty($request['category'])) {

                    if ($request->orderBy == 1) {
                        $orderBy = $request->orderBy;
                        $categories = Category::all();
                        $surveys = Survey::all();
                        $region = Region::all();
                        $city = City::all();
                        $investment = Investment::all();
                        $results = $investment->concat($surveys);
                        $results = $results->sortByDesc('created_at');
                        $count = $results->count();
                        $results = $results->forPage($request->get('page', 1), 8)->toArray();
                        $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));

                        $images = Image::all();
                        return view('favorite', compact('city','region','city','region','surveys', 'categories', 'results', 'images','orderBy'));
                    }
                    if ($request->orderBy == 2) {
                        $orderBy = $request->orderBy;
                        $categories = Category::all();
                        $surveys = Survey::orderBy('price')->get();
                        $region = Region::all();
                        $city = City::all();
//                    $investment = Investment::orderBy('am_required')->get();
                        $results = $surveys;
//                    $results = $results->sortBy('price');
                        $count = $results->count();
                        $results = $results->forPage($request->get('page', 1), 8)->toArray();
                        $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                        $images = Image::all();
                        return view('favorite', compact('city','region','surveys', 'categories', 'results', 'images','orderBy'));
                    }
                    if ($request->orderBy == 3) {
                        $orderBy = $request->orderBy;
                        $categories = Category::all();
                        $region = Region::all();
                        $city = City::all();
                        $surveys = Survey::orderByDesc('price')->get();;
//                    $investment = Investment::orderBy('am_required')->get();
                        $results = $surveys;
//                    $results = $results->sortBy('price');
                        $count   = $results->count();
                        $results = $results->forPage($request->get('page', 1), 8)->toArray();
                        $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                        $images  = Image::all();

                        return view('favorite', compact('city','region','surveys', 'categories', 'results', 'images','orderBy'));
                    }
                    if ($request->orderBy == 4) {
                        $orderBy = $request->orderBy;
                        $categories = Category::all();
                        $surveys = Survey::all();
                        $region = Region::all();
                        $city = City::all();
                        $investment = Investment::all();
                        $results = $investment->concat($surveys);
                        $results = $results->sortByDesc('created_at');
                        $count = $results->count();
                        $results = $results->forPage($request->get('page', 1), 8)->toArray();
                        $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));

                        $images = Image::all();
                        return view('favorite', compact('city','region','surveys', 'categories', 'results', 'images','orderBy'));
                    }
                    if ($request->orderBy == 5) {
                        $orderBy = $request->orderBy;
                        $categories = Category::all();
                        $investment = Investment::orderByDesc('am_required')->get();
                        $results = $investment; $region = Region::all();
                        $city = City::all();
                        $results = $results->sortByDesc('am_required');
                        $count = $results->count();
                        $results = $results->forPage($request->get('page', 1), 8)->toArray();
                        $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                        $images = Image::all();
                        return view('favorite', compact('city','region','categories', 'results', 'images','orderBy'));
                    }
                }

//            if ($request['region']) {
//                $orderBy = $request->orderBy;
//                $regions = $request['region'];
//                $region = Region::all();
//                $city = City::all();
//                $surveys =  Survey::whereIn('region_id',$regions)->get();
////                $investments = Investment::whereIn('cat_id', $category_ids)->get();
//                $results = $surveys;
//                $results = $results->sortByDesc('created_at');
//                $count = $results->count();
//                $categories = Category::all();
//                $results = $results->forPage($request->get('page', 1), 8)->toArray();
//                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
//                $images = Image::all();
//                $old_path = $_SERVER['QUERY_STRING'];
//
//                return view('projects', compact('city','regions','region','surveys', 'categories', 'results', 'images','orderBy'));
//            }

                if ($request['cities'] && empty($request->search)) {

                    $orderBy = $request->orderBy;
                    $city_id = $request['cities'];

                    $city_id = explode(',', $city_id);
                    $region = Region::all();
                    $city = City::all();
                    $surveys =  Survey::whereIn('city_id',$city_id)->get();
//                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                    $results = $surveys;
                    $results = $results->sortByDesc('created_at');
                    $count = $results->count();
                    $categories = Category::all();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $images = Image::all();
                    $old_path = $_SERVER['QUERY_STRING'];

                    return view('favorite', compact('city','city_id','region','surveys', 'categories', 'results', 'images','orderBy'));
                }
                if ($request['cities'] && empty($request['category'])) {

                    $orderBy = $request->orderBy;
                    $city_id = $request['cities'];

                    $city_id = explode(',', $city_id);
                    $region = Region::all();
                    $city = City::all();
                    $surveys =  Survey::whereIn('city_id',$city_id)->get();
//                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                    $results = $surveys;
                    $results = $results->sortByDesc('created_at');
                    $count = $results->count();
                    $categories = Category::all();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $images = Image::all();
                    $old_path = $_SERVER['QUERY_STRING'];

                    return view('favorite', compact('city','city_id','region','surveys', 'categories', 'results', 'images','orderBy'));
                }

                if ($request['cities'] && $request->search) {

                    $orderBy = $request->orderBy;
                    $city_id = $request['cities'];
                    $search = $request->search;
                    $city_id = explode(',', $city_id);
                    $region = Region::all();
                    $city = City::all();
                    $surveys =  Survey::whereIn('city_id',$city_id)->where('name', 'LIKE', "%$search%")->get();
//                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                    $results = $surveys;
                    $results = $results->sortByDesc('created_at');
                    $count = $results->count();
                    $categories = Category::all();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $images = Image::all();
                    $old_path = $_SERVER['QUERY_STRING'];

                    return view('favorite', compact('city','city_id','search','region','surveys', 'categories', 'results', 'images','orderBy'));
                }

                if ($request['cities'] && $request['category']) {

                    $orderBy = $request->orderBy;
                    $city_id = $request['cities'];
                    $category_ids = $request['category'];
                    $city_id = explode(',', $city_id);
                    $region = Region::all();
                    $city = City::all();
                    $surveys =  Survey::whereIn('city_id',$city_id)->whereIn('field_of_activity', $category_ids)->get();
//                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                    $results = $surveys;
                    $results = $results->sortByDesc('created_at');
                    $count = $results->count();
                    $categories = Category::all();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $images = Image::all();
                    $old_path = $_SERVER['QUERY_STRING'];

                    return view('favorite', compact('city','city_id','region','surveys', 'categories', 'results', 'images','orderBy'));
                }

                if ($request['cities'] && $request['category'] && $request->search) {

                    $orderBy = $request->orderBy;
                    $city_id = $request['cities'];
                    $search = $request->search;
                    $category_ids = $request['category'];
                    $city_id = explode(',', $city_id);
                    $region = Region::all();
                    $city = City::all();
                    $surveys =  Survey::whereIn('city_id',$city_id)->whereIn('field_of_activity', $category_ids)->where('name', 'LIKE', "%{$search}%")->get();
//                $investments = Investment::whereIn('cat_id', $category_ids)->get();
                    $results = $surveys;
                    $results = $results->sortByDesc('created_at');
                    $count = $results->count();
                    $categories = Category::all();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $images = Image::all();
                    $old_path = $_SERVER['QUERY_STRING'];

                    return view('favorite', compact('city','city_id','region','surveys', 'categories', 'results', 'images','orderBy'));
                }


                if ($request['category'] && empty($request->search)) {
                    $orderBy = $request->orderBy;
                    $category_ids = $request['category'];
                    $region = Region::all();
                    $city = City::all();
                    $surveys = Survey::whereIn('field_of_activity', $category_ids)->get();
                    $investments = Investment::whereIn('cat_id', $category_ids)->get();
                    $results = $investments->concat($surveys);
                    $results = $results->sortByDesc('created_at');
                    $count = $results->count();
                    $categories = Category::all();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $images = Image::all();
                    $old_path = $_SERVER['QUERY_STRING'];

                    return view('favorite', compact('city','region','surveys', 'categories', 'results', 'images', 'category_ids','orderBy'));
                }

                if ($request->search && empty($request['category'])) {

                    $request->validate([
                        'search' => 'required'
                    ]);
                    $orderBy = $request->orderBy;
                    $search = $request->search;
                    $region = Region::all();
                    $city = City::all();
                    $categories = Category::all();
                    $surveys = Survey::where('name', 'LIKE', "%{$search}%")->get();
                    $investment = Investment::where('company_name', 'LIKE', "%{$search}%")->get();
                    $results = $investment->concat($surveys);
                    $results = $results->sortByDesc('created.at');
                    $count = $results->count();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $old_path = $_SERVER['QUERY_STRING'];
                    $images = Image::all();

                    return view('favorite', compact('city','region','surveys', 'categories', 'results', 'images','search','orderBy'));
                }

                if ($request['category'] && $request->search) {

                    $orderBy = $request->orderBy;
                    $search = $request->search;
                    $region = Region::all();
                    $city = City::all();
                    $category_ids = $request['category'];
                    $surveys = Survey::whereIn('field_of_activity', $category_ids)->where('name', 'LIKE', "%{$search}%")->get();
                    $investments = Investment::whereIn('cat_id', $category_ids)->where('company_name', 'LIKE', "%{$search}%")->get();
                    $results = $investments->concat($surveys);
                    $results = $results->sortByDesc('created_at');
                    $count = $results->count();
                    $categories = Category::all();
                    $results = $results->forPage($request->get('page', 1), 8)->toArray();
                    $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                    $images = Image::all();

                    $old_path = $_SERVER['QUERY_STRING'];

                    return view('favorite', compact('city','region','surveys', 'categories', 'results', 'images', 'search', 'category_ids','orderBy'));

                }

            } else {
                $orderBy = $request->orderBy;
                $categories = Category::all();
                $city = City::all();
                $region = Region::all();
                $category_ids = $request['category'];
                $investments = Investment::all();
                $surveys = Survey::all();
                $businesses = Business::all();
                $images = Image::all();
                $results = $investments->concat($surveys);
                $results = $results->sortByDesc('created_at');
                $count = $results->count();
                $results = $results->forPage($request->get('page', 1), 8)->toArray();
                $results = new LengthAwarePaginator($results, $count, 8, $request->get('page', 1));
                return view('favorite', compact('city','region','businesses', 'categories', 'results', 'images','category_ids','orderBy'));
            }


//        if ($request->orderBy) {
//            if ($request->orderBy == 1) {
//                $search = $request->search;
//                $categories = Category::all();
//                $surveys = Survey::all()->orderBy('Asc');
//                $investment = Investment::where('company_name', 'LIKE', "%{$search}%")->get();
//                $results = $investment->concat($surveys);
//                $results = $results->sortByDesc('category');
//                $count = $results->count();
//                $results = $results->forPage($request->get('page', 1), 3)->toArray();
//                $results = new LengthAwarePaginator($results, $count, 3, $request->get('page', 1));
//
//                $images = Image::all();
//                return view('projects', compact('surveys', 'categories', 'results', 'images'));
//            }
//            if ($request->orderBy == 2) {
//                $categories = Category::all();
//                $surveys = Survey::orderBy('price')->get();
//                $investment = Investment::orderBy('am_required')->get();
//                $results = $investment->concat($surveys);
////                $results = $results->sortBy('price')->sortBy('am_required');
//                $count = $results->count();
//                $results = $results->forPage($request->get('page', 1), 3)->toArray();
//                $results = new LengthAwarePaginator($results, $count, 3, $request->get('page', 1));
//                $images = Image::all();
//                return view('projects', compact('surveys', 'categories', 'results', 'images'));
//            }
//            if ($request->orderBy == 3) {
//                $categories = Category::all();
//                $surveys = Survey::orderByDesc('price')->get();
//                $investment = Investment::orderByDesc('am_required')->get();
//                $results = $investment->concat($surveys);
//                $results = $results->sortByDesc('price');
//                $count = $results->count();
//                $results = $results->forPage($request->get('page', 1), 3)->toArray();
//                $results = new LengthAwarePaginator($results, $count, 3, $request->get('page', 1));
//                $images = Image::all();
//                return view('projects', compact('surveys', 'categories', 'results', 'images'));
//            }
//            if ($request->orderBy == 4) {
//
//            }
//            if ($request->orderBy == 5) {
//                $categories = Category::all();
////              $surveys = Survey::orderByDesc('price')->get();
//                $investment = Investment::orderByDesc('am_required')->get();
//                $results = $investment;
////              $results = $results->sortByDesc('price');
//                $count = $results->count();
//                $results = $results->forPage($request->get('page', 1), 3)->toArray();
//                $results = new LengthAwarePaginator($results, $count, 3, $request->get('page', 1));
//                $images = Image::all();
//                return view('projects', compact('categories', 'results', 'images'));
//            }
//        }


        }

    public function get_filters_path_for_favorite(Request $request)
    {
        $ssl = isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == '127.0.0.1' ? 'http://' : 'https://';
        $request->old_path = str_replace($ssl . $_SERVER['HTTP_HOST'] . '/', '', $_SERVER['HTTP_REFERER']);
        $old_path          = !empty($request->old_path) ? $request->old_path : '/?';





        if (!empty($request->city)) {

            $citys = implode(',', $request->city);


            $parsed = parse_url($request->old_path);

            if (isset($parsed['query'])) {

                $query = $parsed['query'];
                parse_str($query, $params);

                unset($params['cities']);
                $string = http_build_query($params);
                $old_path = '?' . $string . "&cities=" . $citys;

            } else {

                $old_path = "?cities=" . $citys;

            }


        }

        if (!empty($request->region)) {

            for ($i = 0; $i < count($request->region); $i++) {

                if (!empty($request->old_path)) {

                    $find = strpos($old_path, "region[]=" . $request->region[$i]);
                    $find2 = strpos($old_path, "region=" . $request->region[$i]);
                    $find3 = strpos($old_path, "region=" . $request->region[$i]);
                    $find4 = strpos($old_path, "region=" . $request->region[$i]);
                    $find5 = strpos($old_path, "region=" . $request->region[$i]);

                    if ( $find === false && $find2 === false && $find3 === false && $find4 === false && $find5 === false  ) {

                        $old_path .= "&region[]=" . $request->region[$i];

                    }


                } else {

                    if (count($request->region) - 1 == $i) {

                        $old_path .= "region[]=" . $request->region[$i];

                    } else {
                        $old_path .= "region[]=" . $request->region[$i] . "&";

                    }

                }

            }


            $parsed = parse_url($old_path);
            $query = $parsed['query'];

            parse_str($query, $params);
            unset($params['page']);

            $old_path = '?'.http_build_query($params);


        }else{

            if($request->region) {

                $parsed = parse_url($request->old_path);
                $query = $parsed['query'];
                parse_str($query, $params);
                unset($params['region']);
                unset($params['page']);
                $string = http_build_query($params);
                $old_path = '?'.$string;


            }
        }








        if (!empty($request->category)) {

            for ($i = 0; $i < count($request->category); $i++) {

                if (!empty($request->old_path)) {

                    $find = strpos($old_path, "category[]=" . $request->category[$i]);
                    $find2 = strpos($old_path, "category%5B0%5D=" . $request->category[$i]);
                    $find3 = strpos($old_path, "category%5B1%5D=" . $request->category[$i]);
                    $find4 = strpos($old_path, "category%5B2%5D=" . $request->category[$i]);
                    $find5 = strpos($old_path, "category%5B3%5D=" . $request->category[$i]);

                    if ( $find === false && $find2 === false && $find3 === false && $find4 === false && $find5 === false  ) {

                        $old_path .= "&category[]=" . $request->category[$i];

                    }


                } else {

                    if (count($request->category) - 1 == $i) {

                        $old_path .= "category[]=" . $request->category[$i];

                    } else {
                        $old_path .= "category[]=" . $request->category[$i] . "&";

                    }

                }

            }


            $parsed = parse_url($old_path);
            $query = $parsed['query'];

            parse_str($query, $params);
            unset($params['page']);

            $old_path = '?'.http_build_query($params);


        }else{

            if(isset($request->action) && $request->action == 'category_form') {
                $parsed = parse_url($request->old_path);
                $query = $parsed['query'];
                parse_str($query, $params);
                unset($params['category']);
                unset($params['page']);
                $string = http_build_query($params);
                $old_path = '?'.$string;


            }
        }
        if (!empty($request->search)) {
            if (!empty($request->old_path)) {


                $x = $request->old_path;
                $parsed = parse_url($x);
                $query = $parsed['query'];
                parse_str($query, $params);
                unset($params['search']);
                unset($params['page']);
                $string = http_build_query($params);
                $params['search'] = '?' . $string . "&search=" . $request->search;
                $old_path = $params['search'];

            } else {
                $old_path .= "search=" . $request->search;
            }
        }
        if (!empty($request->orderBy)) {


            if (!empty($request->old_path)) {
                $x = $request->old_path;
                $parsed = parse_url($x);
                $query = $parsed['path'];
                parse_str($query, $params);
                unset($params['orderBy']);
                unset($params['page']);
                $string = http_build_query($params);
                $params['orderBy'] = '?' . $string . "&orderBy=" . $request->orderBy;
                $old_path = $params['orderBy'];


                $old_path .= "&search=".$request->search;
            } else {
                $old_path .= "&orderBy=" . $request->orderBy;
            }
        }


        return redirect($old_path);


    }
}
