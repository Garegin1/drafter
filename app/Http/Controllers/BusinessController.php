<?php

namespace App\Http\Controllers;

use App\Http\Requests\BusinessRequest;
use App\Models\Business;
use App\Models\City;
use App\Models\Image;
use App\Models\Investment;
use App\Models\Region;
use App\Models\Survey;
use App\Models\User;
use App\Http\Requests\SurveyRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;


class BusinessController extends Controller
{
    /**
     * Create Business advert
     * @param BusinessRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BusinessRequest $request)
    {

//        if ($request->company_logo) {
//            $imageName = auth()->user()->id . '-' . time() . '.' . $request->company_logo->extension();
//            $request->company_logo->storeAs('public/uploads', $imageName);
//        } else {
//            $imageName = "";
//        }
//        if (!$urgently = $request->urgently) {
//            $urgently = 0;
//        } else {
//            $urgently = 1;
//        }
//        $business = new Business();
//        $business->company_name = $request->company_name;
//        $business->user_id = auth()->user()->id;
//        $business->cat_id = $request->category;
//        $business->company_logo = $imageName;
//        $business->description = $request->description;
//        $business->cost = $request->cost;
//        $business->monthly_income = $request->monthly_income;
//        $business->city = $request->city;
//        $business->urgently = $urgently;
//        $business->save();
//        if ($request->hasfile('images')) {
//            $images = $request->file('images');
//            foreach ($images as $image) {
//                $name = auth()->user()->id . '-' . uniqid() . '-' . time() . '.' . $image->getClientOriginalName();
//                Image::create([
//                    'name' => $name,
//                    'add_id' => $business->id,
//                    'add_type' => 'business'
//                ]);
//                $image->storeAs('public/business_gallery', $name);
//            }
//        }
//        return redirect()->route('business.preview', $business->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function preview($id)
    {
        $business = Business::where('id', '=', $id)->first();
        $images = Image::where('add_id', '=', $id)
            ->where('add_type', '=', 'business')
            ->get();
        return view('add_preview', compact('business', 'images'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change_status($id)
    {
        $business = Business::where('id', '=', $id)->first();
        $business->status = '1';
        if ($business->save()) {
            return redirect()->route('business.single', ['name' => 'business', 'id' => $business->id]);
        } else {
            return redirect()->back()->withErrors(['status' => 'Что то пошло не так']);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function purchase($id)
    {
        if (!Auth::user()->business($id)->first()) {
            Auth::user()->business()->attach($id);
        }
        $business = Business::find($id);
        $user_id = $business->user_id;
        return view('purchase_business', compact('user_id'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function sentence($id)
    {
        $sentences = Business::find($id)->users;
        return view('business_sentence', compact('sentences'));
    }

    public function showSurvey()
    {


        return view('create_add_survey');
    }

    public function createSurvey(Request $request)
    {
        if ($request->val) {
            $sql = "SELECT `cities`.id as id,`cities`.name as name,`regions`.region_name as region_name, `regions`.id as region_id  FROM `cities` JOIN `regions` ON `cities`.`region_id` = `regions`.id WHERE `cities`.name LIKE '%$request->val%' ";
            $city = DB::select($sql);
            $arr = [];
            foreach ($city as $cit) {

                $arr["$cit->region_id"][0] = [
                    'region_id' => $cit->region_id,
                    'region_name' => $cit->region_name,
                ];

                $arr["$cit->region_id"][] = [
                    'id' => $cit->id,
                    'region_id' => $cit->region_id,
                    'region_name' => $cit->region_name,
                    'name' => $cit->name,
                ];
            }


            return response($arr);
        }


        if ($request->step == 'step1') {

            $date = Carbon::now()->format('d.m.y');

            $input = $request->except(['_token', 'images', 'images1']);

            $messages = [
                'name.required' => 'Обязательное поле',
                'data.required' => 'Обязательное поле',
                'price.required' => 'Обязательное поле',
                'price.numeric' => 'должен быть числом',
                'reason_sale.required' => 'Обязательное поле',
            ];

            $validator = Validator::make($request->all(), [
//                'name' => 'required',
//                'data' => 'required',
//                'business_activity' => 'required',
//                'reason_sale' => 'required',
//                'price' => 'required',
//                'images' => 'required'
            ], $messages);


//            if ($request->hasfile('logo')) {
//                $logo = $request->logo;
//                $logoName = time() . rand(1, 100) . '.' . $logo->extension();
//                $logo->storeAs('public/uploads', $logoName);
//                $input['logo'] = $logoName;
//            }
//            if (empty($request->urgently)){
//                $request->urgently = 0;
//            }
//            if (empty($request->phone)){
//                $request->phone = 0;
//            }
            if ($request->hasfile('images1')) {
//
//                $image = $request->file('profile_image');
//                $fileExtension   = strtolower($image->getClientOriginalExtension());
//                $file_name       = sha1(uniqid().$image.uniqid()).'.'.$fileExtension;
//                $destinationPath = 'uploads/profile_image/';
//                $img = Image::make($image->getRealPath());
//                $img->resize(140, 140, function ($constraint) {
//                    $constraint->aspectRatio();
//                })->save($destinationPath.$file_name);


                $images = $request->images;
                $img_arr = [];
                foreach ($images as $image) {
                    $name = auth()->user()->id . '-' . uniqid() . '-' . time() . '.' . $image->getClientOriginalName();
                    $img_arr[] = $name;
                    $image->storeAs('public/business_gallery', $name);

                }

                Session::put('images', $img_arr);


            }


            if (!$validator->passes()) {
                return response()->json(['error' => $validator->errors()]);
            }
            Session::put('business_info_step1', $input);


            return response()->json(['success' => 'Added new records.', 'input' => $input,'images' => $img_arr]);

        }
        if ($request->step == 'step2') {
            $messages = [
                'field_of_activity.required' => 'Обязательное поле',
                'status_business.required' => 'Обязательное поле',
                'services_provided.required' => 'Обязательное поле',
                'selling_product.required' => 'Обязательное поле',
                'amount_of_workers.numeric' => 'должен быть числом',
                'number_of_management_personnel.numeric' => 'должен быть числом',
            ];
            $validator = Validator::make($request->all(), [
                'field_of_activity' => 'required',
                'status_business' => 'required',
                'services_provided' => 'required',
                'selling_product' => 'required',
                'amount_of_workers' => 'nullable|numeric',
                'number_of_management_personnel' => 'nullable|numeric',
            ], $messages);
            if (!$validator->passes()) {
                return response()->json(['error' => $validator->errors()]);
            }
            $input = $request->except(['_token']);

            Session::put('business_info_step2', $input);
            return response()->json(['success' => 'Added new records.', 'input' => $input]);
        }
        if ($request->step == 'step3') {
            $messages = [
                'object_which_activity_carried_out.required' => 'Обязательное поле',
                'purpose_real_estate_object_where_activity_carried_out.required' => 'Обязательное поле',
                'type_ownership.required' => 'Обязательное поле',
                'floor.numeric' => 'должен быть числом',
            ];
            $validator = Validator::make($request->all(), [
                'object_which_activity_carried_out' => 'required',
                'purpose_real_estate_object_where_activity_carried_out' => 'required',
                'type_ownership' => 'required',
            ], $messages);
            if (!$validator->passes()) {
                return response()->json(['error' => $validator->errors()]);
            }
            $input = $request->except(['_token']);

            Session::put('business_info_step3', $input);

            return response()->json(['success' => 'Added new records.', 'input' => $input]);
        }
        if ($request->step == 'step4') {
            $messages = [
                'total_area.required' => 'Обязательное поле',
            ];
            $validator = Validator::make($request->all(), [
                'total_area' => 'required',
            ], $messages);
            if (!$validator->passes()) {
                return response()->json(['error' => $validator->errors()]);
            }
            $input = $request->except(['_token']);
            Session::put('business_info_step4', $input);

            return response()->json(['success' => 'Added new records.', 'input' => $input]);
        }
        if ($request->step == 'step5') {
            $messages = [
                'taxation_system.required' => 'Обязательное поле',
                'average_check.required' => 'Обязательное поле',
                'average_monthly_turnover.required' => 'Обязательное поле',
                'general_expenses.required' => 'Обязательное поле',
                'net_profit_per_month.required' => 'Обязательное поле',
                'business_payback_period.required' => 'Обязательное поле',
                'debts_fines.required' => 'Обязательное поле',
                'average_monthly_turnover.numeric' => 'должен быть числом',
                'general_expenses.numeric' => 'должен быть числом',
                'net_profit_per_month.numeric' => 'должен быть числом',
                'business_payback_period.numeric' => 'должен быть числом',
            ];
            $validator = Validator::make($request->all(), [
                'taxation_system' => 'required',
                'average_check' => 'required',
                'average_monthly_turnover' => 'required',
                'general_expenses' => 'required',
                'net_profit_per_month' => 'required',
                'business_payback_period' => 'required',
                'debts_fines' => 'required',
            ], $messages);
            if (!$validator->passes()) {
                return response()->json(['error' => $validator->errors()]);
            }
            $input = $request->except(['_token']);
            Session::put('business_info_step5', $input);

            return response()->json(['success' => 'Added new records.', 'input' => $input]);
        }

        if ($request->step == 'step6') {
            $messages = [
                'subject.required' => 'Обязательное поле',
                'equipment.required' => 'Обязательное поле',
                'licenses.required' => 'Обязательное поле',
                'area_m2.required' => 'Обязательное поле',
                'area_m2.numeric' => 'должен быть числом',
            ];
            $validator = Validator::make($request->all(), [
                'adress' => 'required',
                'phone_number' => 'required|min : 18',
            ], $messages);
            if (!$validator->passes()) {
                return response()->json(['error' => $validator->errors()]);
            }
            $input = $request->except(['_token']);
            Session::put('business_info_step6', $input);
            return response()->json(['success' => 'Added new records.', 'input' => $input]);
        }

        if ($request->step == 'step7') {

            $input = $request->except(['_token']);


                $link_video = json_decode(Session::get('business_info_step1')['link_video']);
                $link_vid = '';
            if (!empty(Session::get('business_info_step1')['link_video'])){
                for ($i = 0; $i < count($link_video); $i++) {

                    if (count($link_video) > 1 && $i < count($link_video) - 1) {
                        $link_vid .= $link_video[$i]->value . ',';
                    } else {
                        $link_vid .= $link_video[$i]->value ;
                    }

                }
            }


            $website = json_decode(Session::get('business_info_step6')['website']);
            $websites = '';

            if (!empty(Session::get('business_info_step6')['website'])){
                for ($i = 0; $i < count($website); $i++) {

                    if (count($website) > 1 && $i < count($website) - 1) {
                        $websites .= $website[$i]->value . ',';
                    } else {
                        $websites .= $website[$i]->value ;
                    }

                }
            }


            $social_network = json_decode(Session::get('business_info_step6')['social_network']);
            $social_networks = '';
            if(!empty(Session::get('business_info_step6')['social_network'])){
                for ($i = 0; $i < count($social_network ); $i++) {

                    if (count($social_network ) > 1 && $i < count($social_network ) - 1) {
                        $social_networks .= $social_network [$i]->value . ',';
                    } else {
                        $social_networks .= $social_network [$i]->value ;
                    }

                }
            }
            $price = '';
            if (Session::get('business_info_step1')['price']){
                $pr = Session::get('business_info_step1')['price'];
                $price .= str_replace(' ','',$pr);
            }



            Session::put('business_info_step7', $input);
            $survey = Survey::create([
                'user_id' => auth()->user()->id,
                'name' => Session::get('business_info_step1')['name'],
                'date_foundation_company' => Session::get('business_info_step1')['data'],
                'business_activity' => Session::get('business_info_step1')['business_activity'],
                'reason_for_sale' => Session::get('business_info_step1')['reason_sale'],
                'link_video' =>  $link_vid,
                'price' => $price,
                'field_of_activity' => Session::get('business_info_step2')['field_of_activity'],
                'organizational_and_legal_form' => Session::get('business_info_step2')['status_business'],
                'other_activities_of_company' => Session::get('business_info_step2')['organizational_legal_form'],
                'services_provided' => Session::get('business_info_step2')['services_provided'],
                'selling_product' => Session::get('business_info_step2')['selling_product'],
                'amount_of_workers' => Session::get('business_info_step2')['amount_of_workers'],
                'number_of_management_personnel' => Session::get('business_info_step2')['number_of_management_personnel'],
                'object_in_which_the_activity_is_carried_out' => Session::get('business_info_step3')['object_which_activity_carried_out'],
                'purpose_of_the_real_estate_object_where_activity_is_carried_out' => Session::get('business_info_step3')['purpose_real_estate_object_where_activity_carried_out'],
                'type_of_ownership' => Session::get('business_info_step3')['type_ownership'],
                'floor' => Session::get('business_info_step3')['floor'],
                'properties_for_sale' => Session::get('business_info_step4')['properties_for_sale'],
                'equipment_sold_at_salefloor' => Session::get('business_info_step4')['equipment_sold_sale'],
                'total_area' => Session::get('business_info_step4')['total_area'],
                'availability_of_licenses_certificates' => Session::get('business_info_step4')['licenses'],
                'taxation_system' => Session::get('business_info_step5')['taxation_system'],
                'average_check' => Session::get('business_info_step5')['average_check'],
                'average_monthly_turnover' => Session::get('business_info_step5')['average_monthly_turnover'],
                'salary_fund' => Session::get('business_info_step5')['salary_fund'],
                'rent' => Session::get('business_info_step5')['rentals'],
                'cost_of_utility_bills' => Session::get('business_info_step5')['cost_utility_bills'],
                'general_expenses' => Session::get('business_info_step5')['general_expenses'],
                'net_profit_per_month' => Session::get('business_info_step5')['net_profit_per_month'],
                'business_payback_period' => Session::get('business_info_step5')['business_payback_period'],
                'debts_and_fines' => Session::get('business_info_step5')['debts_fines'],
                'address' => Session::get('business_info_step6')['adress'],
                'region_id' => Session::get('business_info_step6')['region_id'],
                'city_id' => Session::get('business_info_step6')['city_id'],
                'website' =>  $websites,
                'social_website' => $social_networks,
                'phone_number' => Session::get('business_info_step6')['phone_number'],
                'barring_calls' => isset(Session::get('business_info_step6')['barring_calls']) ? Session::get('business_info_step6')['barring_calls'] : '0',
                'additional_information' => $request->additional_information,
            ]);

            $images = Session::get('images');
            if ($images) {
                foreach ($images as $image) {
                    Image::create([
                        'name' => $image,
                        'add_id' => $survey->id,
                        'add_type' => 'business'
                    ]);
                }
            }


//            return response()->json(['success' => 'Added new records.', 'input' => $input]);

            Session::forget('images');

            return redirect('/');

        }

//        $data = Arr::except($request->all,'_token');
//        Business::find($id)->survey()->create($data);


    }

    public function search(Request $request)
    {

        $search = $request->search;

        $business = Business::where('company_name', "like", '%' . $request->search . '%')->get();
        $investment = Investment::where('company_name', "like", '%' . $request->search . '%')->get();


        return view('search_item', [
            'products' => $business,
            '$investment' => $investment,
        ])->render();
//                ->with([
//                'investment_s' => $investment,
//                'business' => $business,
//
//            ]);
        //  return view('/', compact('investment','business'));

//                $data=Business::where('company_name',"like",'%'.$request->search.'%')->get();
//                $data1=Investment::where('company_name',"like",'%'.$request->search.'%')->get();


    }

    public function images()
    {
        return $this->hasMany(Image::class, 'add_id')->where('images.add_type', '=', 'business');
    }
}
