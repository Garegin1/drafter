<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class City extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'region_id',
        'name',
    ];



    public function region(){
        return $this->hasMany(Region::class,'region_id')->where('region.region_id','=','region');
    }
}
