@php
if (isset($_SERVER['QUERY_STRING'])){
    $old_path = $_SERVER['QUERY_STRING'];
}else{
    $old_path = '';
}

@endphp

@if ($paginator->hasPages())
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
        <div class="pagination_svg_wrapper left_arrow" class="disabled">
            <a class="pagination_number ">
                <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9 1L1 9L9 17" stroke="#4c4949" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round"></path>
                </svg>
            </a>
        </div>
    @else

        @php
            $parsed = parse_url($old_path);
            $query = $parsed['path'];
            parse_str($query, $params);
            unset($params['page']);
            $string = http_build_query($params);

            if(strlen($string) > 0) {
                $params['page'] = $paginator->previousPageUrl().'&'.$string;
            } else {
                $params['page'] = $paginator->previousPageUrl();
            }
            
        @endphp

        <div class="pagination_svg_wrapper left_arrow">
            <a class="pagination_number" href="{{ $params['page'] }}" rel="prev">


                <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9 1L1 9L9 17" stroke="#F0F0F0" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round"></path>
                </svg>
            </a>
        </div>
    @endif



    {{-- Pagination Elements --}}
    @if(count($elements) >3 )
        @php
            unset($elements[4][array_key_first($elements[4])]);
            unset($elements[0][2]);

        @endphp

    @elseif(count($elements) >2)
        @php
            unset($elements[4][array_key_first($elements[4])]);
        @endphp

    @endif

    @foreach ($elements as $element)

        {{-- "Three Dots" Separator --}}
        @if (is_string($element))
            <div class="l pagination_num_wrapper_dot " style="">
                <span class="pagination_number">{{ $element }}</span>
            </div>
        @endif


        {{-- Array Of Links --}}
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <div class="pagination_num_wrapper active ">
                        <a class="pagination_number">{{ $page }}</a>
                    </div>
                @else

                    @php


                    $parsed = parse_url($old_path);
                    $query = $parsed['path'];
                    parse_str($query, $params);
                    unset($params['page']);
                    $string = http_build_query($params);
                    $params['page'] = '/?'.$string."&page=" .$page;



                    @endphp

                    <div class="pagination_num_wrapper">
                        <a class="pagination_number" href="{{$params['page'] }}">{{ $page }}</a>
                    </div>
                @endif
            @endforeach
        @endif
    @endforeach


    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())

        @php
            $parsed = parse_url($old_path);
            $query = $parsed['path'];
            parse_str($query, $params);
            unset($params['page']);
            $string = http_build_query($params);
            $params['page'] = $paginator->nextPageUrl().'&'.$string;
        @endphp

        <div class="pagination_svg_wrapper right_arrow">
            <a href="{{ $params['page'] }}" rel="next" class="pagination_number">
                <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 17L9 9L1 1" stroke="#F0F0F0" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round"></path>
                </svg>
            </a>
        </div>
    @else
        <div class="pagination_svg_wrapper right_arrow" class="disabled">
            <a class="pagination_number" href="">
                <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 17L9 9L1 1" stroke="#4c4949" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round"></path>
                </svg>
            </a>
        </div>
    @endif
@endif
