@include('include.header')
    <link rel="stylesheet" href="../website/css/recover.css">
    <main>
        <section class="recover_password">
            <div class="recover_password_wrapper">
                <form action="{{ route('recover_password_step1') }}" method="post" class="recover_password_form_wrapper">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

                    <h1 class="recover_password_form_title">    Восстановление пароля</h1>

{{--                    @if ($errors->has('phone'))--}}
{{--                        <span class="error_text">{{$errors->first('phone')}}</span>--}}
{{--                        <div class="recover_password_inputs_wrapper error_text_input">--}}
{{--                    @elseif($errors->has('phone_not_exist'))--}}
{{--                        <span class="error_text">{{$errors->first('phone_not_exist')}}</span>--}}
{{--                        <div class="recover_password_inputs_wrapper error_text_input">--}}
{{--                    @else--}}
{{--                        <div class="recover_password_inputs_wrapper">--}}
{{--                    @endif--}}
{{--                        <input type="number" name="phone" class="recover_password_input" value="" placeholder="Номер телефона">--}}
{{--                    </div>--}}


                    @if ($errors->has('password'))
                        <span class="error_text">{{$errors->first('password')}}</span>
                        <div class="recover_password_inputs_wrapper error_text_input log_in_input_wrapper">
                    @else
                        <div class="recover_password_inputs_wrapper log_in_input_wrapper">
                    @endif
                        <input type="password" id="password_input" name="password" class="recover_password_input" placeholder="Новый пароль">
                        <img src="{{ asset('images/glaz.png') }}" alt="" class="glaz show" style="top: 8px">

                    </div>

                    @if ($errors->has('password_confirmation'))
                        <span class="error_text">{{$errors->first('password_confirmation')}}</span>
                        <div class="recover_password_inputs_wrapper error_text_input log_in_input_wrapper">
                    @else
                        <div class="recover_password_inputs_wrapper log_in_input_wrapper">
                    @endif
                        <input type="password" id="confirm_password" name="password_confirmation" class="recover_password_input" placeholder="Повторите пароль">
                        <img src="{{ asset('images/glaz.png') }}" alt="" class="glaz show" style="top: 8px">

                    </div>

                    <button class="recover_password_form_btn">
                        Далее
                    </button>
                </form>
            </div>
        </section>

    </main>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="../website/js/confirm.js"></script>

<script>
    $('#password_input').on('keyup', function () {

        let password_input = $(this).val();
        let reg = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$');
        if (reg.test(password_input)) {
            $('#password_input ').css('border','1px solid #e5d6a0')

        } else if (password_input == '')
        {
            $('#password_input ').css('border','1px solid #e5d6a0')

        } else {
            $('#password_input ').css('border','1px solid #c34d4d')
        }
    })

    $('#confirm_password').on('keyup',function (){
        let pass = $('#password_input').val()
        let conf_pass = $(this).val()
        if(conf_pass == pass){
            $(".recover_password_form_btn ").css({'opacity': 'unset'})
            $(".recover_password_form_btn ").attr('disabled',false)
        }

    })

</script>


@include('include.footer')
