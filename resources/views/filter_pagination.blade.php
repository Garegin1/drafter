@if ($paginator->lastPage() > 1)

    <div class="pagination_svg_wrapper {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="pagination_number " href="{{  $paginator->url(1) }}">

            <svg width="10" height="18" viewBox="0 0 10 18" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <path d="M9 1L1 9L9 17" stroke="#F0F0F0" stroke-width="2" stroke-linecap="round"
                      stroke-linejoin="round"></path>
            </svg>

        </a>
    </div>

    @for ($i = 1; $i <= $paginator->lastPage(); $i++)

        @if($i >= $paginator->currentPage() && $i < $paginator->currentPage()+3 )
            <div class="pagination_num_wrapper  {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                <a class="pagination_number" href="{{ $paginator->url($i) }}">{{ $i }}</a>
            </div>
        @endif


    @endfor
{{--    @if( $paginator->currentPage() == $paginator->lastPage()-2 || $paginator->currentPage() == $paginator->lastPage()-1 || $paginator->currentPage() == $paginator->lastPage())--}}

{{--    @else--}}
        <div class="l pagination_num_wrapper ">
            <a class="pagination_number" href="" disabled="true">...</a>
        </div>

        <div class="l pagination_num_wrapper  {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' active' : '' }}">
            <a class="pagination_number" href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a>
        </div>
{{--    @endif--}}


    <div
        class="pagination_svg_wrapper {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
        <a class="pagination_number" href="{{ $paginator->url($paginator->currentPage()+1) }}">

            <svg width="10" height="18" viewBox="0 0 10 18" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <path d="M1 17L9 9L1 1" stroke="#F0F0F0" stroke-width="2" stroke-linecap="round"
                      stroke-linejoin="round"></path>
            </svg>

        </a>
    </div>
@endif





