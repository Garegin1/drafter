@include('include.header')

<link rel="stylesheet" href="{{ asset('/website/css/news.css' )}}">
<main>
    <section class="news">
        <div class="header_second_part">
            <div class="header_second_wrapper">
                <nav class="header_second_nav_list">
                    <ul class="second_nav_ul_list">
                        <li class="second_nav_ul_li">
                            <a href="/" class="second_nav_link">Главная</a>
                        </li>
                        <li class="second_nav_ul_li">
                            <a href="{{ route('news.index') }}" class="second_nav_link">Новости</a>
                        </li>
                        <li class="second_nav_ul_li">
                            <a href="{{ route('my_projects.show') }}" class="second_nav_link">Мои проекты</a>
                        </li>
                        <li class="second_nav_ul_li">
                            <a href="{{ route('messages.show') }}" class="second_nav_link">Мои сообщения</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="news_wrapper">
            <h1 class="news_title">Новости</h1>
            <div class="system_news">
                 <div class="system_news_wrapper">
                     <h1 class="system_news_title">
                         Системные новости
                     </h1>
                     <div class="system_news_swiper_btns_wrapper">
                        <div class="swiper-button-prev system_news_prev"></div>
                        <div class="swiper-button-next system_news_next"></div>
                       <div class="swiper-container" id="system_news_slider">
                        
                          <div class="swiper-wrapper">
                            
                            <div class="swiper-slide">
                                <div class="system_news_item">
                                    <div class="system_news_item_svg_title">
                                        <div class="system_news_item_svg">
                                           <img src="{{ asset('website/images') }}/settings.png">

                                        </div>
                                        <p class="system_news_item_title">Обновление 21.3</p>
                                    </div>
                                    <p class="system_news_item_text">
                                        Вышла новая версия программного обеспечения. Вышла новая версия программного обеспечения ...
                                    </p>
                                    <p class="system_news_item_text2">
                                        14.07.202
                                    </p>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                 <div class="system_news_item">
                                    <div class="system_news_item_svg_title">
                                        <div class="system_news_item_svg">
                                           <img src="{{ asset('website/images') }}/settings.png">

                                        </div>
                                        <p class="system_news_item_title">Обновление 21.3</p>
                                    </div>
                                    <p class="system_news_item_text">
                                        Вышла новая версия программного обеспечения. Вышла новая версия программного обеспечения ...
                                    </p>
                                    <p class="system_news_item_text2">
                                        14.07.202
                                    </p>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                 <div class="system_news_item">
                                    <div class="system_news_item_svg_title">
                                        <div class="system_news_item_svg">
                                           <img src="{{ asset('website/images') }}/settings.png">

                                        </div>
                                        <p class="system_news_item_title">Обновление 21.3</p>
                                    </div>
                                    <p class="system_news_item_text">
                                        Вышла новая версия программного обеспечения. Вышла новая версия программного обеспечения ...
                                    </p>
                                    <p class="system_news_item_text2">
                                        14.07.202
                                    </p>
                                </div>
                            </div>
                             <div class="swiper-slide">
                                 <div class="system_news_item">
                                    <div class="system_news_item_svg_title">
                                        <div class="system_news_item_svg">
                                           <img src="{{ asset('website/images') }}/settings.png">

                                        </div>
                                        <p class="system_news_item_title">Обновление 21.3</p>
                                    </div>
                                    <p class="system_news_item_text">
                                        Вышла новая версия программного обеспечения. Вышла новая версия программного обеспечения ...
                                    </p>
                                    <p class="system_news_item_text2">
                                        14.07.202
                                    </p>
                                </div>
                            </div>

                            
                          </div>
                      </div>
                         
                     </div>


                     <div class="system_news_item_wrapper">
                        <div class="system_news_item_second_wrapper">
                           <div class="system_news_item">
                                    <div class="system_news_item_svg_title">
                                        <div class="system_news_item_svg">
                                           <img src="{{ asset('website/images') }}/settings.png">

                                        </div>
                                        <p class="system_news_item_title">Обновление 21.3</p>
                                    </div>
                                    <p class="system_news_item_text">
                                        Вышла новая версия программного обеспечения. Вышла новая версия программного обеспечения ...
                                    </p>
                                    <p class="system_news_item_text2">
                                        14.07.202
                                    </p>
                                </div>
                            <div class="system_news_item">
                                    <div class="system_news_item_svg_title">
                                        <div class="system_news_item_svg">
                                           <img src="{{ asset('website/images') }}/settings.png">

                                        </div>
                                        <p class="system_news_item_title">Обновление 21.3</p>
                                    </div>
                                    <p class="system_news_item_text">
                                        Вышла новая версия программного обеспечения. Вышла новая версия программного обеспечения ...
                                    </p>
                                    <p class="system_news_item_text2">
                                        14.07.202
                                    </p>
                                </div>
                              <div class="system_news_item">
                                    <div class="system_news_item_svg_title">
                                        <div class="system_news_item_svg">
                                           <img src="{{ asset('website/images') }}/settings.png">

                                        </div>
                                        <p class="system_news_item_title">Обновление 21.3</p>
                                    </div>
                                    <p class="system_news_item_text">
                                        Вышла новая версия программного обеспечения. Вышла новая версия программного обеспечения ...
                                    </p>
                                    <p class="system_news_item_text2">
                                        14.07.202
                                    </p>
                            </div>
                              <div class="system_news_item">
                                    <div class="system_news_item_svg_title">
                                        <div class="system_news_item_svg">
                                           <img src="{{ asset('website/images') }}/settings.png">

                                        </div>
                                        <p class="system_news_item_title">Обновление 21.3</p>
                                    </div>
                                    <p class="system_news_item_text">
                                        Вышла новая версия программного обеспечения. Вышла новая версия программного обеспечения ...
                                    </p>
                                    <p class="system_news_item_text2">
                                        14.07.202
                                    </p>
                             </div>
                        </div>

                     </div>
                             <!-- If we need navigation buttons -->
                 

                    <a href="" class="more_system_news">
                        Смотреть все 
                    </a>
                    
              </div>
            </div>
            <div class="completed_projects_title_select_wrapper desctop">
                <h1 class="completed_projects_title">Информационные новости</h1>
                <div class="complected_projects_btns_select_wrapper">
                    <div class="complected_projects_bnts_main_parent">
                        <div class="completed_projects_btn1 active_bt">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.5" y="0.5" width="31" height="31" stroke="#E5D6A0"/>

                                <rect class='btn1_black' x="7.5" y="7.5" width="17" height="3" rx="0.5"
                                      stroke="#E5D6A0"/>
                                <rect class='btn1_black' x="7.5" y="14.5" width="17" height="3" rx="0.5"
                                      stroke="#E5D6A0"/>
                                <rect class='btn1_black' x="7.5" y="21.5" width="17" height="3" rx="0.5"
                                      stroke="#E5D6A0"/>
                            </svg>
                        </div>
                        <div class="completed_projects_btn2">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.5" y="0.5" width="31" height="31" stroke="#E5D6A0"/>
                                <rect class='btn2_black' x="7.5" y="7.5" width="7" height="7" rx="0.5"
                                      stroke="#E5D6A0"/>
                                <rect class='btn2_black' x="7.5" y="17.5" width="7" height="7" rx="0.5"
                                      stroke="#E5D6A0"/>
                                <rect class='btn2_black' x="17.5" y="7.5" width="7" height="7" rx="0.5"
                                      stroke="#E5D6A0"/>
                                <rect class='btn2_black' x="17.5" y="17.5" width="7" height="7" rx="0.5"
                                      stroke="#E5D6A0"/>
                            </svg>
                        </div>
                    </div>
                    <form action="">
                        
                        <input type="hidden" name="old_path">

                        <div class="completed_projects_select_wrapper">
                            <div class="completed_projects_title_svg_wrapper">


                                <div class="dropdown">

                                    <p class="completed_projects_name ">
                                         Сначала новые
                                    </p>

                                    <div class="completed_projects_info_wrapper dropdown-content">
                                        <ul>
                                            <li class="completed_projects_info"
                                                value="1">

                                                <div class="completed_projects_info_link">
                                                    Сначала новые
                                                </div>
                                                <div class="completed_projects_select_line">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                              d="M13.7069 0.293031C13.8944 0.480558 13.9997 0.734866 13.9997 1.00003C13.9997 1.26519 13.8944 1.5195 13.7069 1.70703L5.70692 9.70703C5.51939 9.8945 5.26508 9.99982 4.99992 9.99982C4.73475 9.99982 4.48045 9.8945 4.29292 9.70703L0.292919 5.70703C0.110761 5.51843 0.00996641 5.26583 0.0122448 5.00363C0.0145233 4.74143 0.119692 4.49062 0.3051 4.30521C0.490508 4.1198 0.741321 4.01464 1.00352 4.01236C1.26571 4.01008 1.51832 4.11087 1.70692 4.29303L4.99992 7.58603L12.2929 0.293031C12.4804 0.10556 12.7348 0.000244141 12.9999 0.000244141C13.2651 0.000244141 13.5194 0.10556 13.7069 0.293031Z"
                                                              fill="#e5d6a0"/>
                                                    </svg>
                                                </div>
                                            </li>
                                            <li class="completed_projects_info"
                                                value="2"
                                                data-value="Сначала дешевые">
                                                <div class="completed_projects_info_link">
                                                 
                                                    Сначала дешевые
                                                </div>
                                                <div class="completed_projects_select_line">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                              d="M13.7069 0.293031C13.8944 0.480558 13.9997 0.734866 13.9997 1.00003C13.9997 1.26519 13.8944 1.5195 13.7069 1.70703L5.70692 9.70703C5.51939 9.8945 5.26508 9.99982 4.99992 9.99982C4.73475 9.99982 4.48045 9.8945 4.29292 9.70703L0.292919 5.70703C0.110761 5.51843 0.00996641 5.26583 0.0122448 5.00363C0.0145233 4.74143 0.119692 4.49062 0.3051 4.30521C0.490508 4.1198 0.741321 4.01464 1.00352 4.01236C1.26571 4.01008 1.51832 4.11087 1.70692 4.29303L4.99992 7.58603L12.2929 0.293031C12.4804 0.10556 12.7348 0.000244141 12.9999 0.000244141C13.2651 0.000244141 13.5194 0.10556 13.7069 0.293031Z"
                                                              fill="#e5d6a0"/>
                                                    </svg>
                                                </div>
                                            </li>
                                            <li class="completed_projects_info"
                                                value="3"
                                                data-value="Сначала дорогие">
                                                <div class="completed_projects_info_link">
                                                    Сначала дорогие
                                                </div>
                                                <div class="completed_projects_select_line">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                              d="M13.7069 0.293031C13.8944 0.480558 13.9997 0.734866 13.9997 1.00003C13.9997 1.26519 13.8944 1.5195 13.7069 1.70703L5.70692 9.70703C5.51939 9.8945 5.26508 9.99982 4.99992 9.99982C4.73475 9.99982 4.48045 9.8945 4.29292 9.70703L0.292919 5.70703C0.110761 5.51843 0.00996641 5.26583 0.0122448 5.00363C0.0145233 4.74143 0.119692 4.49062 0.3051 4.30521C0.490508 4.1198 0.741321 4.01464 1.00352 4.01236C1.26571 4.01008 1.51832 4.11087 1.70692 4.29303L4.99992 7.58603L12.2929 0.293031C12.4804 0.10556 12.7348 0.000244141 12.9999 0.000244141C13.2651 0.000244141 13.5194 0.10556 13.7069 0.293031Z"
                                                              fill="#e5d6a0"/>
                                                    </svg>
                                                </div>
                                            </li>
                                            <li class="completed_projects_info"
                                                value="4"
                                                data-value="Сначала с высоким">
                                                <div class="completed_projects_info_link">
                                                    Сначала с высоким рейтингом
                                                </div>
                                                <div class="completed_projects_select_line">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                              d="M13.7069 0.293031C13.8944 0.480558 13.9997 0.734866 13.9997 1.00003C13.9997 1.26519 13.8944 1.5195 13.7069 1.70703L5.70692 9.70703C5.51939 9.8945 5.26508 9.99982 4.99992 9.99982C4.73475 9.99982 4.48045 9.8945 4.29292 9.70703L0.292919 5.70703C0.110761 5.51843 0.00996641 5.26583 0.0122448 5.00363C0.0145233 4.74143 0.119692 4.49062 0.3051 4.30521C0.490508 4.1198 0.741321 4.01464 1.00352 4.01236C1.26571 4.01008 1.51832 4.11087 1.70692 4.29303L4.99992 7.58603L12.2929 0.293031C12.4804 0.10556 12.7348 0.000244141 12.9999 0.000244141C13.2651 0.000244141 13.5194 0.10556 13.7069 0.293031Z"
                                                              fill="#e5d6a0"/>
                                                    </svg>
                                                </div>
                                            </li>
                                            <li class="completed_projects_info"
                                                value="5"
                                                data-value="Сначала с высоким процентом">
                                                <div class="completed_projects_info_link">
                                                    Сначала с высоким процентом
                                                </div>
                                                <div class="completed_projects_select_line">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                              d="M13.7069 0.293031C13.8944 0.480558 13.9997 0.734866 13.9997 1.00003C13.9997 1.26519 13.8944 1.5195 13.7069 1.70703L5.70692 9.70703C5.51939 9.8945 5.26508 9.99982 4.99992 9.99982C4.73475 9.99982 4.48045 9.8945 4.29292 9.70703L0.292919 5.70703C0.110761 5.51843 0.00996641 5.26583 0.0122448 5.00363C0.0145233 4.74143 0.119692 4.49062 0.3051 4.30521C0.490508 4.1198 0.741321 4.01464 1.00352 4.01236C1.26571 4.01008 1.51832 4.11087 1.70692 4.29303L4.99992 7.58603L12.2929 0.293031C12.4804 0.10556 12.7348 0.000244141 12.9999 0.000244141C13.2651 0.000244141 13.5194 0.10556 13.7069 0.293031Z"
                                                              fill="#e5d6a0"/>
                                                    </svg>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="completed_projects_select_line selected">
                                    <svg width="10" height="6" viewBox="0 0 10 6" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1 0.5L5 4.5L9 0.5" stroke="#6E7C87"/>
                                    </svg>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
           <div class="news_links_parent_wrapper">
               <div class="news_links_wrapper">
                <a href="news_details.html" target="_parent" class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('/images') }}/news_link_img5.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                            <h3 class="news_link_title">Обновление</h3>
                          <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                           Вышла новая версия ...
                       </p>

                       <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                </a>
                <a href="news_details.html"  target="_parent" class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img2.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                         <h3 class="news_link_title">Акция</h3>
                         <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                       <p class="hidden_news_link_info">
                           Иван Иванов проводит акцию...
                       </p>
                        <p class="hidden_news_link_num">14.07.2021</p>
                        
                    </div>
                </a>
                <a href="news_details.html" target="_parent"  class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img3.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                          <h3 class="news_link_title">Акция</h3>
                          <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                         <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                         </p>
                          <p class="hidden_news_link_num">14.07.2021</p>
                    </div>   
                </a>
                <a href="news_details.html" target="_parent"  class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img2.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Новости проекта</h3>
                     <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                          Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>               
                </a>
                <a href="news_details.html"  target="_parent" class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img4.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                         <h3 class="news_link_title">Акция</h3>
                     <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                       <p class="hidden_news_link_info">
                          Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>

                    </div>
                 </a>
                <a href="news_details.html"  target="_parent" class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img2.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                         <h3 class="news_link_title">Акция</h3>
                          <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                              Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                 
                </a>
                <a href="news_details.html" target="_parent"  class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img5.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Новости проекта</h3>
                       <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                     
                </a>
                <a href="news_details.html"  target="_parent" class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img2.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Новости проекта</h3>
                           <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                    
                </a>
                <a href="news_details.html"  target="_parent" class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img3.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Обновление</h3>
                        <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                  
                </a>
                <a href="news_details.html" target="_parent"  class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img2.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Акция</h3>
                        <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                  
                </a>
                <a href="news_details.html" target="_parent"  class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img3.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Акция</h3>
                         <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                
                </a>
                <a href="news_details.html" target="_parent"  class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img2.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Новости проекта</h3>
                        <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                 
                </a>
                <a href="news_details.html"  target="_parent" class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img4.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Акция</h3>
                          <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                        </p> 
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                    
                </a>
                <a href="news_details.html"  target="_parent" class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img2.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Акция</h3>
                           <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                 
                </a>
                <a href="news_details.html" target="_parent"  class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img5.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Новости проекта</h3>
                            <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                  
                </a>
                <a href="news_details.html" target="_parent"  class="news_link">
                    <div class="news_link_img_wrapper news_link_img">
                        <img src="{{ asset('images\\') }}news_link_img2.png" alt="">
                    </div>
                    <div class="news_link_title_info_wrapper">
                        <h3 class="news_link_title">Новости проекта</h3>
                        <p class="news_link_info">
                            Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван Иванов проводит марафон акций Иван <span class="news_link_info_second_prt">
                                Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон акций Иван Иванов проводит марафон акцийИван Иванов проводит марафон ...
                            </span> 
                        </p>
                        <p class="hidden_news_link_info">
                            Иван Иванов проводит акцию...
                        </p>
                         <p class="hidden_news_link_num">14.07.2021</p>
                    </div>
                   
                </a>
              </div>    
           </div>
           <div class="pagination_wrapper">
             <div class="pagination_svg_wrapper left_arrow">
                <a class="pagination_number ">
                <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9 1L1 9L9 17" stroke="#4c4949" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                </svg>
                </a>
                </div>
                 <div class="pagination_num_wrapper active ">
                        <a class="pagination_number">1</a>
                </div>
                                            
                    
                <div class="pagination_num_wrapper">
                        <a class="pagination_number" href="/?&amp;page=2">2</a>
                 </div>
                                            
                    
                <div class="pagination_num_wrapper">
                        <a class="pagination_number" href="/?&amp;page=3">3</a>
                </div>                                        
                    
                <div class="pagination_num_wrapper">
                        <a class="pagination_number" href="/?&amp;page=4">4</a>
                </div>
             <div class="pagination_svg_wrapper right_arrow">
            <a href="/?page=2&amp;" rel="next" class="pagination_number">
                <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 17L9 9L1 1" stroke="#F0F0F0" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                </svg>
            </a>
            </div>
    
    </div>
        </div>
    </section>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('website/js/news.js') }}"></script>

@include('include.footer')

<script>
      $(".completed_projects_btn1").click(function () {
        localStorage.setItem('click', 'completed_projects_btn1');
    });

    $(".completed_projects_btn2").click(function () {
        localStorage.setItem('click', 'completed_projects_btn2');
    });

    if (localStorage.getItem('click') == 'completed_projects_btn1') {
        $('.news_links_wrapper').removeClass('second_variant')
        $('.news_links_wrapper').addClass('first_variant')

        $('.completed_projects_btn1').addClass('active_bt')
        $('.completed_projects_btn2').removeClass('active_bt')

       
    }

    if (localStorage.getItem('click') == 'completed_projects_btn2') {
        $('.news_links_wrapper').removeClass('first_variant')
        $('.news_links_wrapper').addClass('second_variant')
        $('.completed_projects_btn2').addClass('active_bt')
        $('.completed_projects_btn1').removeClass('active_bt')


    }

    $('.completed_projects_btn1').on('click', function () {
        $('.news_links_wrapper').removeClass('second_variant')
        $('.news_links_wrapper').addClass('first_variant')
        $('.completed_projects_btn1').addClass('active_bt')
        $('.completed_projects_btn2').removeClass('active_bt')

       
    })

    $('.completed_projects_btn2').on('click', function () {
        $('.news_links_wrapper').removeClass('first_variant')
        $('.news_links_wrapper').addClass('second_variant')
        $('.completed_projects_btn2').addClass('active_bt')
        $('.completed_projects_btn1').removeClass('active_bt')


      

    })
   

   const swiper_system = new Swiper('#system_news_slider', {
  // Optional parameters
  direction: 'horizontal',
  loop: true,
  slidesPerView: 4,
  navigation: {
    nextEl: '.system_news_next',
    prevEl: '.system_news_prev',
  },

  breakpoints: {
            320: {
                slidesPerView: 1
            },
            324: {
                slidesPerView: 1
            },
            330: {
                slidesPerView: 1
            },
            340: {
                slidesPerView: 1
            },
            350: {
                slidesPerView: 1
            },
            360: {
                slidesPerView: 1
            },
            370: {
                slidesPerView: 1
            },
            380: {
                slidesPerView: 1
            },
            400: {
                slidesPerView: 1
            },
            420: {
                slidesPerView: 1
            },
            426: {
                slidesPerView: 1
            },
            427: {
                slidesPerView: 1
            },
            440: {
                slidesPerView: 1
            },
            450: {
                slidesPerView: 1
            },
            460: {
                slidesPerView: 1
            },
            470: {
                slidesPerView: 1
            },
            480: {
                slidesPerView: 1
            },
            500: {
                slidesPerView: 1
            },
            524: {
                slidesPerView: 1
            },
            525: {
                slidesPerView: 1
            },
            600: {
                slidesPerView: 1
            },
            700: {
                slidesPerView: 1
            },
            768: {
                slidesPerView: 1
            },
            769: {
                slidesPerView: 1
            },
            800: {
                slidesPerView: 1
            },
            900: {
                slidesPerView: 3
            },
            1000: {
                slidesPerView: 3
            },
            1100: {
                slidesPerView: 3
            },
            1120: {
                slidesPerView: 3
            },
            1130: {
                slidesPerView: 3
            },
            1140: {
                slidesPerView: 3
            },
            1150: {
                slidesPerView: 3
            },
            1152: {
                slidesPerView: 3
            },
            1155: {
                slidesPerView: 3
            },
            1157: {
                slidesPerView: 3
            },
            1158: {
                slidesPerView: 3

            },
            1160: {
                slidesPerView: 3
            },
            1170: {
                slidesPerView: 3
            },
            1180: {
                slidesPerView: 3
            },
            1170: {
                slidesPerView: 3
            },
            1180: {
                slidesPerView: 3
            },
            1190: {
                slidesPerView: 3
            },
            1200: {
                slidesPerView: 3
            },
            1216: {
                slidesPerView: 1
            },
            1218: {
                slidesPerView: 3
            },
            1220: {
                slidesPerView: 3
            },
            1230: {
                slidesPerView: 3
            },
            1240: {
                slidesPerView: 3
            },
            1250: {
                slidesPerView: 3
            },
            1264: {
                slidesPerView: 3
            },
             1265: {
                slidesPerView: 4
            },
            1270: {
                slidesPerView: 4
            },
            1280: {
                slidesPerView: 4
            },
            1281: {
                slidesPerView: 4
            },
            1299: {
                slidesPerView: 4
            },
            1500: {
                slidesPerView: 4
            },
            1600: {
                slidesPerView: 4
            },
            1700: {
                slidesPerView: 4
            },
            1800: {
                slidesPerView: 4
            },
            1920: {
                slidesPerView: 4
            }
        }

});

</script>