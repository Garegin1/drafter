@include('include.header')
<link rel="stylesheet" href="{{ asset('/website/css/aplication_form.css' )}}">
<link rel="stylesheet" href="{{ asset('/website/css/investment.css' )}}">
<link rel="stylesheet" type="text/css" href="{{ asset('/website/css/jquery.tagsinput.css' )}}"/>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
      href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css"/>
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<main>
    <section class="aplication_form">
        <div class="aplication_form_wrapper">
            <div class="ankettop">
                <div class="ankettop_left">
                    <a href='/add/choose-type' class="back_link">
                        <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9 1L0.999999 9L9 17" stroke="#E5D6A0" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round"/>
                        </svg>

                        Назад</a>
                    <h1 class="aplication_form_title">Анкета</h1>
                    <h2 class="aplication_form_title_second">
                        Продажа бизнеса</h2>
                </div>
                <div class="business_services business_services2">
                    <div class="business_services2_cont">
                        <div class="business_services_item ">
                            <h5>Юридические услуги</h5>
                            <ul>
                                <li><a href="#">Открытие, закрытие ИП и ООО</a></li>
                                <li><a href="#">Банкротство</a></li>
                                <li><a href="#">Составление договоров</a></li>
                                <li><a href="#">Подключение ЭЦП</a></li>
                            </ul>
                        </div>
                        <h4>Услуги для бизнеса</h4>
                        <div class="business_services_item">
                            <h5>Бухгалтерские услуги</h5>
                            <ul>
                                <li><a href="#">Абонентское бухгалтерское обслуживание</a></li>
                                <li><a href="#">Экспресс Аудит</a></li>
                                <li><a href="#">Срочная сдача отчетностей в ИФНС</a></li>
                                <li><a href="#">Учет бухгалтерии</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="aplication_form_items_wrapper">

                <div class="aplication_form_items_titles_second_wrapper">
                    <div class="aplication_form_items_titles_wrapper">
                        <button type="button" class="swiperprev" name="button">
                            <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <rect width="40" height="40" transform="matrix(-1 0 0 1 40 0)" fill="#0D151E"/>
                                <path d="M24 12L16.3463 19.9865C16.1551 20.186 16.162 20.5028 16.3617 20.6938L24 28"
                                      stroke="#9B8E6C" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                        </button>
                        <div class="swiper-container mySwiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide swiper-slidefirst">
                                    <button value="0" class="aplication_form_items_title selectform afit active"
                                            data-id="open_form1" id="opn1">Общая информация
                                    </button>
                                </div>
                                <div class="swiper-slide">
                                    <button value="1" class="aplication_form_items_title " data-id="open_form2"
                                            id="opn2">Сведения о деятельности
                                    </button>
                                </div>
                                <div class="swiper-slide">
                                    <button value="2" class="aplication_form_items_title " data-id="open_form3"
                                            id="opn3">Сведения об объекте
                                    </button>
                                </div>
                                <div class="swiper-slide">
                                    <button value="3" class="aplication_form_items_title " data-id="open_form4"
                                            id="opn4">Сведения об имуществе
                                    </button>
                                </div>
                                <div class="swiper-slide">
                                    <button value="4" class="aplication_form_items_title " data-id="open_form5"
                                            id="opn5">Финансовая информация
                                    </button>
                                </div>
                                <div class="swiper-slide">
                                    <button value="5" class="aplication_form_items_title afit" data-id="open_form6"
                                            id="opn6">Контакты
                                    </button>
                                </div>
                                <div class="swiper-slide">
                                    <button value="6" class="aplication_form_items_title " data-id="open_form7"
                                            id="opn7">Дополнительная информация
                                    </button>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="swipernext" name="button">
                            <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <rect width="40" height="40" fill="#0D151E"/>
                                <path d="M16 12L23.6537 19.9865C23.8449 20.186 23.838 20.5028 23.6383 20.6938L16 28"
                                      stroke="#9B8E6C" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="anketblock">
                    <div class="aplication_form_item open" id="open_form1">
                        <form id="multi-file-upload-ajax" action="javascript:void(0)" accept-charset="utf-8"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="step1" name="step">
                            <div class="aplication_form_item_first_child">
                                <div class="aplication_form_inputs_hint_wrapper">
                                    <div class="aplication_form_inputs_field">
                                        <span class="aplication_form_input_title">Название компании *</span>
                                        <div class="error_input_wrapper">
                                            <input type="text" class="aplication_form_input first_type_input obinp"
                                                   id="name" name="name">
                                            <span class="error_text1">Введите корректный адрес электронной почты</span>
                                        </div>
                                        <div class="aplication_form_hint_wrapper">
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_hint_wrapper">
                                    <div class="aplication_form_inputs_field">
                                        <span class="aplication_form_input_title">Дата основания компании *</span>
                                        <div class="error_input_wrapper">
                                            <input type="text" class="aplication_form_input first_type_input obinp"
                                                   id="data1" name="data">
                                            <span class="error_text1">Введите корректный адрес электронной почты</span>
                                        </div>
                                        <div class="aplication_form_hint_wrapper">
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Активность бизнеса *</span>

                                    <select name="business_activity" id="business_activity" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Действующий</option>
                                        <option value="1">Приостановлен</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="category_select">
                                            <p class="select_title" id=""></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Действующий">
                                                    Действующий
                                                </p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1" data-name="Приостановлен">
                                                    Приостановлен
                                                </p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Причина продажи *</span>
                                    <select name="reason_sale" id="reason_sale1" class="form_select"
                                            class="aplication_form_input">
                                        <div class="error_input_wrapper">
                                            <option value=""></option>
                                            <option value="0">Переезд</option>
                                            <option value="1">Не хватает времени</option>
                                            <option value="2">Разногласия с партнерами</option>
                                            <option value="3">Не могу набрать сотрудников</option>
                                            <option value="4">Нужны деньги</option>
                                            <option value="5">Хочу отдыхать</option>
                                            <option value="6">Другое</option>
                                        </div>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="status_business_select">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Переезд">
                                                    Переезд</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="Не хватает времени">Не хватает времени</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="2"
                                                   data-name="Разногласия с партнерами">Разногласия с партнерами</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="3"
                                                   data-name="Не могу набрать сотрудников">Не могу набрать
                                                    сотрудников</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="4" data-name="Нужны деньги">
                                                    Нужны деньги</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="5" data-name="Хочу отдыхать">
                                                    Хочу отдыхать</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>

                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="6" data-name="Другое">
                                                    Другое</p>

                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="reason_sale">
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Ссылка на видео</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" class="aplication_form_input sillkainp" id="link_video1"
                                               name="link_video">
                                        <p class="sillkatext">
                                            <span></span>
                                            <button class="clearlink" type="button" name="button">
                                                <svg width="12" height="12" viewBox="0 0 12 12" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M8.82842 3.17188L3.17157 8.82873" stroke="#0E161E"
                                                          stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M3.17157 3.17188L8.82842 8.82873" stroke="#0E161E"
                                                          stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>
                                            </button>
                                        </p>
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Если у вас есть видео презентация вашего бизнеса, укажите ссылку на это
                                                видео
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Фото *</span>
                                    <label class=" write_us_input file_label">

                                        <svg width="30" height="26" viewBox="0 0 30 26" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M25 13V6.55556C25 5.96619 24.7701 5.40095 24.361 4.98421C23.9518 4.56746 23.3968 4.33333 22.8182 4.33333H18.4545L16.2727 1H9.72727L7.54545 4.33333H3.18182C2.60316 4.33333 2.04821 4.56746 1.63904 4.98421C1.22987 5.40095 1 5.96619 1 6.55556V18.7778C1 19.3671 1.22987 19.9324 1.63904 20.3491C2.04821 20.7659 2.60316 21 3.18182 21H17"
                                                stroke="#D2BE8B" stroke-width="1.5" stroke-linecap="round"
                                                stroke-linejoin="round"/>
                                            <path
                                                d="M13 17C15.2091 17 17 15.2091 17 13C17 10.7909 15.2091 9 13 9C10.7909 9 9 10.7909 9 13C9 15.2091 10.7909 17 13 17Z"
                                                stroke="#D2BE8B" stroke-width="1.5" stroke-linecap="round"
                                                stroke-linejoin="round"/>
                                            <path d="M29 21H21" stroke="#D2BE8B" stroke-width="1.5"
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round"/>
                                            <path d="M25 17V25" stroke="#D2BE8B" stroke-width="1.5"
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round"/>
                                        </svg>
                                        <span class="file_span">Добавить фотографии</span>

                                    </label>

                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Фото в хорошем качестве и с правильным ракурсом повышают интерес
                                                покупателей

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="galerycont">
                                    <input type="file" id="fileinput_form_photo" hidden maxlength="10"
                                           name="images1[]" accept="image/png, image/gif, image/jpeg">
                                    <div class="swiper-container galerycontswiper">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <div class="photo_view1 gallery" data-name="0">
                                                    <label for="fileinput_form_photo">
                                                        <div class="addimage">
                                                            <svg width="74" height="74" viewBox="0 0 74 74" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <rect x="0.899902" y="0.5" width="72.2" height="72.2"
                                                                      fill="#1C242D" stroke="#9B8E6C"/>
                                                                <path d="M29.3999 37H45.3999" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                                <path d="M37.3999 45V29" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                            </svg>
                                                        </div>
                                                        <input type="file" id="fileinput_form_photo" hidden
                                                               maxlength="10"
                                                               name="images1[]"
                                                               accept="image/png, image/gif, image/jpeg">
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="photo_view1 gallery" data-name="1">
                                                    <label for="fileinput_form_photo">
                                                        <div class="addimage">
                                                            <svg width="74" height="74" viewBox="0 0 74 74" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <rect x="0.899902" y="0.5" width="72.2" height="72.2"
                                                                      fill="#1C242D" stroke="#9B8E6C"/>
                                                                <path d="M29.3999 37H45.3999" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                                <path d="M37.3999 45V29" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                            </svg>
                                                        </div>

                                                    </label>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="photo_view1 gallery" data-name="2">
                                                    <label for="fileinput_form_photo">
                                                        <div class="addimage">
                                                            <svg width="74" height="74" viewBox="0 0 74 74" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <rect x="0.899902" y="0.5" width="72.2" height="72.2"
                                                                      fill="#1C242D" stroke="#9B8E6C"/>
                                                                <path d="M29.3999 37H45.3999" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                                <path d="M37.3999 45V29" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                            </svg>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="swiper-slide" >
                                                <div class="photo_view1 gallery" data-name="3">
                                                    <label for="fileinput_form_photo">
                                                        <div class="addimage">
                                                            <svg width="74" height="74" viewBox="0 0 74 74" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <rect x="0.899902" y="0.5" width="72.2" height="72.2"
                                                                      fill="#1C242D" stroke="#9B8E6C"/>
                                                                <path d="M29.3999 37H45.3999" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                                <path d="M37.3999 45V29" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                            </svg>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="photo_view1 gallery" data-name="4">
                                                    <label for="fileinput_form_photo">
                                                        <div class="addimage">
                                                            <svg width="74" height="74" viewBox="0 0 74 74" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <rect x="0.899902" y="0.5" width="72.2" height="72.2"
                                                                      fill="#1C242D" stroke="#9B8E6C"/>
                                                                <path d="M29.3999 37H45.3999" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                                <path d="M37.3999 45V29" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                            </svg>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="photo_view1 gallery" data-name="5">
                                                    <label for="fileinput_form_photo">
                                                        <div class="addimage">
                                                            <svg width="74" height="74" viewBox="0 0 74 74" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <rect x="0.899902" y="0.5" width="72.2" height="72.2"
                                                                      fill="#1C242D" stroke="#9B8E6C"/>
                                                                <path d="M29.3999 37H45.3999" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                                <path d="M37.3999 45V29" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                            </svg>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="photo_view1 gallery" data-name="6">
                                                    <label for="fileinput_form_photo">
                                                        <div class="addimage">
                                                            <svg width="74" height="74" viewBox="0 0 74 74" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <rect x="0.899902" y="0.5" width="72.2" height="72.2"
                                                                      fill="#1C242D" stroke="#9B8E6C"/>
                                                                <path d="M29.3999 37H45.3999" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                                <path d="M37.3999 45V29" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                            </svg>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="photo_view1 gallery" data-name="7">
                                                    <label for="fileinput_form_photo">
                                                        <div class="addimage">
                                                            <svg width="74" height="74" viewBox="0 0 74 74" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <rect x="0.899902" y="0.5" width="72.2" height="72.2"
                                                                      fill="#1C242D" stroke="#9B8E6C"/>
                                                                <path d="M29.3999 37H45.3999" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                                <path d="M37.3999 45V29" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                            </svg>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="photo_view1 gallery" data-name="8">
                                                    <label for="fileinput_form_photo">
                                                        <div class="addimage">
                                                            <svg width="74" height="74" viewBox="0 0 74 74" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <rect x="0.899902" y="0.5" width="72.2" height="72.2"
                                                                      fill="#1C242D" stroke="#9B8E6C"/>
                                                                <path d="M29.3999 37H45.3999" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                                <path d="M37.3999 45V29" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                            </svg>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="photo_view1 gallery" data-name="9">
                                                    <label for="fileinput_form_photo">
                                                        <div class="addimage">
                                                            <svg width="74" height="74" viewBox="0 0 74 74" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <rect x="0.899902" y="0.5" width="72.2" height="72.2"
                                                                      fill="#1C242D" stroke="#9B8E6C"/>
                                                                <path d="M29.3999 37H45.3999" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                                <path d="M37.3999 45V29" stroke="#9B8E6C"
                                                                      stroke-linecap="round"/>
                                                            </svg>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Цена, ₽ *</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" maxlength="13" class="aplication_form_input maskprice"
                                               id="price" name="price">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_btn_wrapper" id="open_form1">
                                    <button class="aplication_form_btn btn_step1">Далее</button>
                                </div>
                            </div>

                        </form>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Название компании *
                                </p>
                                <p class="aplication_form_input_answer_info" id="name1">
                                </p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Дата основания компании *
                                </p>
                                <p class="aplication_form_input_answer_info" id="data"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Активность бизнеса *
                                </p>
                                <p class="aplication_form_input_answer_info" id="business_activity1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Причина продажи *
                                </p>
                                <p class="aplication_form_input_answer_info" id="reason_sale"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Ссылка на видео
                                </p>
                                <p class="aplication_form_input_answer_info" id="site_link"></p>
                            </div>
                            {{--                        <div class="aplication_form_inputs_answers">--}}
                            {{--                            <p class="aplication_form_input_answer_title">Лого</p>--}}
                            {{--                            <p class="aplication_form_input_answer_info"></p>--}}
                            {{--                        </div>--}}
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Фото</p>
                                {{--  <p class="aplication_form_input_answer_info" id="img">--}}
                                <div class="photo_view1 photo_view12 gallery" style="margin-left: 0!important ;width: 147%!important">


                                </div>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Цена
                                </p>
                                <p class="aplication_form_input_answer_info" id="price1"></p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>

                    </div>
                    <div class="aplication_form_item" id="open_form2">
                        <form id="ajax_step2" accept-charset="utf-8">

                            <input type="hidden" value="step2" name="step">
                            <div class="aplication_form_item_first_child">
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Сфера деятельности *</span>
                                    <select name="field_of_activity" class="form_select" class="aplication_form_input">
                                        <div class="error_input_wrapper">
                                            <option value=""></option>
                                            @foreach($categories as $cat)
                                                <option value="{{ $cat->id }}">
                                                    {{ $cat->name }}
                                                </option>
                                            @endforeach
                                        </div>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="field_of_activity1">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>

                                        </div>
                                        <div class="select_items_wrapper">

                                            @foreach($categories as $cat)

                                                <div class="select_item">
                                                    <p class="select_item_title" data-value="{{ $cat->id }}"
                                                       data-name="{{ $cat->name }}">
                                                        {{ $cat->name }}
                                                    </p>
                                                    <div class="select_item_svg">
                                                        <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                                  stroke-linecap="round" stroke-linejoin="round"/>
                                                        </svg>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="field_of_activity">
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Организационно-правовая форма *</span>
                                    <select name="status_business" class="form_select" class="aplication_form_input">
                                        <div class="error_input_wrapper">
                                            <option value=""></option>
                                            <option value="0">АО</option>
                                            <option value="1">ЗАО</option>
                                            <option value="2">ООО</option>
                                            <option value="3">ИП</option>
                                            <option value="4">Самозанятость</option>
                                            <option value="5">Другое</option>
                                        </div>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="status_business1">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="АО">АО</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1" data-name="ЗАО">ЗАО</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="2" data-name="ООО">ООО</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="3" data-name="ИП">ИП</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="4" data-name="Самозанятость">
                                                    Самозанятость</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="5" data-name="Другое">
                                                    Другое</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="status_business">
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span
                                        class="aplication_form_input_title">Другие направления деятельности компании</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" class="aplication_form_input" id="organizational_legal_form"
                                               name="organizational_legal_form">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Если у вашей компании есть другие направления деятельности которые
                                                ведутся параллельно основной деятельности, укажите их, это повысит
                                                интерес покупателей к вашему бизнесу
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field nomask">
                                    <span class="aplication_form_input_title">Оказываемые услуги *</span>
                                    <select name="selling_product" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Нет</option>
                                        <option value="1">Есть</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="selling_product">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Нет">
                                                    Нет</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="Другое">Есть</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="selling_product">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Укажите чем занимается ваша компания
                                            </p>
                                        </div>
                                    </div>

                                </div>
                                <div class="aplication_form_inputs_field nomask">
                                    <span class="aplication_form_input_title">Продаваемый товар *</span>
                                    <select name="services_provided" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Нет</option>
                                        <option value="1">Есть</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="services_provided">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Нет">
                                                    Нет</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="Другое">Есть</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="services_provided">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Перечислите какие товары реализует ваша компания
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Количество работников</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" class="aplication_form_input" id="amount_of_workers"
                                               name="amount_of_workers">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Заполнение только цифрами
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Количество руководящего персонала</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" class="aplication_form_input"
                                               id="number_of_management_personnel"
                                               name="number_of_management_personnel">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Заполнение только цифрами
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_btn_wrapper">
                                    <button class="aplication_form_btn btn_step2">Далее</button>
                                </div>
                            </div>

                        </form>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сфера деятельности*</p>
                                <p class="aplication_form_input_answer_info"
                                   id="field_of_activity"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Организационно-правовая форма*
                                </p>
                                <p class="aplication_form_input_answer_info" id="status_business"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Другие направления деятельности компании
                                </p>
                                <p class="aplication_form_input_answer_info" id="organizational_legal_form1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Оказываемые услуги*
                                </p>
                                <p class="aplication_form_input_answer_info" id="services_provided1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Продаваемый товар*
                                </p>
                                <p class="aplication_form_input_answer_info"
                                   id="selling_product1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Количество работников
                                </p>
                                <p class="aplication_form_input_answer_info"
                                   id="amount_of_workers1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">
                                    Количество руководящего персонала
                                </p>
                                <p class="aplication_form_input_answer_info"
                                   id="number_of_management_personnel1"></p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </div>
                    <div class="aplication_form_item" id="open_form3">
                        <form id="ajax_step3" accept-charset="utf-8">

                            <input type="hidden" value="step3" name="step">
                            <div class="aplication_form_item_first_child">
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Объект, в котором осуществляется деятельность *</span>
                                    <select name="object_which_activity_carried_out" class="form_select"
                                            id="object_which_activity_carried_out1"
                                            class="aplication_form_input">
                                        <div class="error_input_wrapper">
                                            <option value=""></option>
                                            <option value="0">Торговый центр</option>
                                            <option value="1">Офис здание</option>
                                            <option value="2">Производственная база</option>
                                            <option value="3">Ферма</option>
                                            <option value="4">Дом</option>
                                            <option value="5">Другое</option>
                                        </div>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="object_which_activity_carried_out2">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Торговый центр">
                                                    Торговый центр</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1" data-name="Офис здание">Офис
                                                    здание</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="2"
                                                   data-name="Производственная база">Производственная база</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="3" data-name="Ферма">Ферма</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="4" data-name="Дом">Дом</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="5" data-name="Другое">
                                                    Другое</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="object_which_activity_carried_out" >
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Если ваша компания располагается в многоквартирном доме или вы работаете
                                                у себя в квартире/доме нужно выбрать пункт «Дом». Если в списке нет
                                                объекта, в котором вы осуществляете бизнес-деятельность, выберете пункт
                                                «Другое» и заполните его самостоятельно
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Назначение объекта недвижимости, где осуществляется деятельность *</span>
                                    <select name="purpose_real_estate_object_where_activity_carried_out"
                                            class="form_select" id="residential_nonresidential_select">
                                        <option value=""></option>
                                        <option value="0">Жилое</option>
                                        <option value="1">Нежилое</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Жилое">Жилое</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1" data-name="Нежилое">
                                                    Нежилое</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Если ваша деятельность осуществляется в многоквартирном доме или у себя
                                                в квартире, доме выберете пункт - Жилое
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Форма собственности *</span>
                                    <select name="type_ownership"
                                            class="form_select" id="type_ownership">
                                        <option value=""></option>
                                        <option value="0">Аренда</option>
                                        <option value="1">Собственность</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Аренда">
                                                    Аренда</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1" data-name="Собственность">
                                                    Собственность</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Если вашим бизнесом можно заниматься у себя дома или в квартире выберете
                                                пункт - «Собственность». Если вы платите аренду за землю или помещение,
                                                в котором ведете деятельность выберете пункт - «Аренда»
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Этаж</span>
                                    <select name="floor" id="floor"
                                            class="form_select">
                                        <option value=""></option>
                                        <option value="0">Цокольный</option>
                                        <option value="1">Другое</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>

                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Цокольный">
                                                    Цокольный</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1" data-name="Другое">
                                                    Другое</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="floor">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Если ваша компания располагается в офисном здании, торговом центре или
                                                многоквартирном доме укажите этаж ЦИФРАМИ.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_btn_wrapper">
                                    <button class="aplication_form_btn btn_step3">Далее</button>
                                </div>
                            </div>
                        </form>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Объект, в котором осуществляется
                                    деятельность*</p>
                                <p class="aplication_form_input_answer_info" id="object_which_activity_carried_out"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Назначение объекта недвижимости, где
                                    осуществляется деятельность*
                                </p>
                                <p class="aplication_form_input_answer_info"
                                   id="residential_nonresidential_select1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Форма собственности*
                                </p>
                                <p class="aplication_form_input_answer_info" id="type_ownership1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Этаж</p>
                                <p class="aplication_form_input_answer_info" id="floor1"></p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </div>
                    <div class="aplication_form_item" id="open_form4">
                        <form id="ajax_step4" accept-charset="utf-8">

                            <input type="hidden" value="step4" name="step">

                            <div class="aplication_form_item_first_child">
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Объекты недвижимости, которые входят в продажу</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" class="aplication_form_input" name="properties_for_sale"
                                               id="properties_for_sale"
                                               autocomplete="off">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>

                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span
                                        class="aplication_form_input_title">Оборудование, передаваемое при продаже</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" class="aplication_form_input" name="equipment_sold_sale"
                                               id="equipment_sold_sale">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Общая площадь,(м²) *</span>
                                    <select name="total_area" id="total_area" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Нет</option>
                                        <option value="1">Есть</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Нет">
                                                    Нет</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="Есть">Есть</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="total_area">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Информацию о площади нужно указать только ЦИФРАМИ которая на текущий
                                                момент используется бизнесом. Если на данный момент у вас нет помещения
                                                или вашим бизнесом можно заниматься у себя дома, в квартире выберете
                                                пункт – Нет.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Наличие лицензий, сертификатов</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" class="aplication_form_input" name="licenses" id="licenses">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Если у вас есть лицензия или/и сертификат укажите что есть и какие сроки
                                                у этих документов
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_btn_wrapper">
                                    <button class="aplication_form_btn btn_step4">Далее</button>
                                </div>
                            </div>

                        </form>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Объекты недвижимости, которые входят в
                                    продажу
                                </p>
                                <p class="aplication_form_input_answer_info" id="properties_for_sale1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Оборудование, передаваемое при продаже</p>
                                <p class="aplication_form_input_answer_info" id="equipment_sold_sale1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Общая площадь (м²)*</p>
                                <p class="aplication_form_input_answer_info" id="total_area1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Наличие лицензий, сертификатов
                                </p>
                                <p class="aplication_form_input_answer_info" id="licenses1"></p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </div>
                    <div class="aplication_form_item" id="open_form5">
                        <form id="ajax_step5" accept-charset="utf-8">

                            <input type="hidden" value="step5" name="step">

                            <div class="aplication_form_item_first_child">
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Система налогооблажения *</span>
                                    <select name="taxation_system" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Нет</option>
                                        <option value="1">ОСН</option>
                                        <option value="2">УСН</option>
                                        <option value="3">ЕСХН</option>
                                        <option value="4">ПСН</option>
                                        <option value="5">НПД</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="taxation_system1">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Нет">
                                                    Нет</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="ОСН">ОСН</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="2" data-name="УСН">
                                                    УСН</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="3"
                                                   data-name="ЕСХН">ЕСХН</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="4" data-name="ПСН">
                                                    ПСН</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="5"
                                                   data-name="НПД">НПД</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Если по каким-то причинам вы не платите налог выберете пункт – Нет. <span>При
                                                выборе пункта – Нет, ваш рейтинг будет снижен</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Средний чек, ₽ * </span>
                                    <select name="average_check" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Нет</option>
                                        <option value="1">Есть</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="average_check1">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Нет">
                                                    Нет</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="Есть">Есть</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="average_check">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Указывайте реальный оборот в месяц, для того чтобы покупатель при
                                                проверке документов и оценки не отказался от покупки. Информацию нужно
                                                указать в ЦИФРАХ. Если ваш бизнес еще не работал и не приносит прибыль
                                                выберете пункт – Нет. <span>При выборе пункта – Нет, ваш рейтинг будет снижен</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Средний оборот в месяц, ₽ *</span>
                                    <select name="average_monthly_turnover" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Нет</option>
                                        <option value="1">Есть</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="average_monthly_turnover">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Нет">
                                                    Нет</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="Есть">Есть</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="average_monthly_turnover">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Указывайте реальный оборот в месяц, для того чтобы покупатель при
                                                проверке документов и оценки не отказался от покупки. Информацию нужно
                                                указать в ЦИФРАХ. Если ваш бизнес еще не работал и не приносит прибыль
                                                выберете пункт – Нет. <span>При выборе пункта – Нет, ваш рейтинг будет снижен</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Фонд заработной платы, ₽</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" maxlength="13" class="aplication_form_input maskprice"
                                               name="salary_fund" id="wage_fund">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Аренда, ₽</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" maxlength="13" class="aplication_form_input maskprice"
                                               id="rentals" name="rentals">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Стоимость коммунальных платежей, ₽</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" class="aplication_form_input maskprice" maxlength="13"
                                               id="cost_utility_bills"
                                               name="cost_utility_bills">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Общие расходы, ₽ *</span>
                                    <select name="general_expenses" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Нет</option>
                                        <option value="1">Есть</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="general_expenses">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Нет">
                                                    Нет</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="Есть">Есть</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="general_expenses">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Рекомендуем вам указать актуальную информацию на текущий период.
                                                Информацию нужно указать в ЦИФРАХ. Если ваш бизнес еще не работал и не
                                                было расходов выберете пункт – Нет. <span>При выборе пункта – Нет, ваш рейтинг будет снижен</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Чистая прибыль в месяц, ₽ *</span>
                                    <select name="net_profit_per_month" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Нет</option>
                                        <option value="1">Есть</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="net_profit_per_month">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Нет">
                                                    Нет</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="Есть">Есть</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="net_profit_per_month">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Рекомендуем вам указать актуальную информацию на текущий период.
                                                Информацию нужно указать в ЦИФРАХ. Если ваш бизнес еще не работал и не
                                                приносит прибыль выберете пункт – Нет. <span>При выборе пункта – Нет, ваш рейтинг будет снижен</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Срок окупаемости бизнеса *</span>
                                    <select name="business_payback_period" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Нет</option>
                                        <option value="1">Есть</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="business_payback_period">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Нет">
                                                    Нет</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="Есть">Есть</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="business_payback_period">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Рекомендуем вам указать актуальную информацию на текущий период.
                                                Информацию нужно указать в ЦИФРАХ. Если ваш бизнес еще не работал и не
                                                было расходов выберете пункт – Нет. <span>При выборе пункта – Нет, ваш рейтинг будет снижен</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Наличие задолженностей и штрафов *</span>
                                    <select name="debts_fines" class="form_select">
                                        <option value=""></option>
                                        <option value="0">Нет</option>
                                        <option value="1">Есть</option>
                                    </select>
                                    <div class="select_wrapper">
                                        <div class="select_title_svg_wrapper" id="debts_fines">
                                            <p class="select_title"></p>
                                            <div class="select_svg">
                                                <svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L17 1" stroke="#E5D6A0" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="select_items_wrapper">
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="0" data-name="Нет">
                                                    Нет</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="select_item">
                                                <p class="select_item_title" data-value="1"
                                                   data-name="Есть">Есть</p>
                                                <div class="select_item_svg">
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="text"class="aplication_form_input hideninpandshow" name="debts_fines">
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Если у вашей компании нет задолженностей и штрафов выберете пункт – Нет.
                                                Если есть, опишите подробно за что, сколько и кому должна ваша компания
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_btn_wrapper">
                                    <button class="aplication_form_btn btn_step5">Далее</button>
                                </div>
                            </div>
                        </form>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Система налогооблажения *</p>
                                <p class="aplication_form_input_answer_info" id="taxation_system"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Средний чек *</p>
                                <p class="aplication_form_input_answer_info" id="average_check"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Средний оборот в месяц ₽ *</p>
                                <p class="aplication_form_input_answer_info" id="average_monthly_turnover1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Фонд заработной платы.
                                </p>
                                <p class="aplication_form_input_answer_info" id="salary_fund"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Аренда</p>
                                <p class="aplication_form_input_answer_info" id="rentals1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Стоимость коммунальных платежей
                                </p>
                                <p class="aplication_form_input_answer_info" id="cost_utility_bills1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Общие расходы *
                                </p>
                                <p class="aplication_form_input_answer_info" id="general_expenses1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Чистая прибыль в месяц *
                                </p>
                                <p class="aplication_form_input_answer_info" id="net_profit_per_month1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Срок окупаемости бизнеса *
                                </p>
                                <p class="aplication_form_input_answer_info" id="business_payback_period1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Наличие задолженностей и штрафов *
                                </p>
                                <p class="aplication_form_input_answer_info" id="debts_fines1"></p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </div>
                    <div class="aplication_form_item" id="open_form6">
                        <form id="ajax_step6" accept-charset="utf-8">

                            <input type="hidden" value="step6" name="step">

                            <div class="aplication_form_item_first_child">
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Адрес *</span>
                                    <div class="adresbox">
                                        <input type="text" class="ardes_input" name="adress" autocomplete="off">
                                        <input type="text" hidden name="region_id" id="region">
                                        <input type="text" hidden name="city_id" id="city">
                                        <svg class="firstsvg" width="18" height="10" viewBox="0 0 18 10" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 1L9 9L17 1" stroke="#D2BE8B" stroke-linecap="round"
                                                  stroke-linejoin="round"/>
                                        </svg>
                                        <div class="adresbox_drop">

                                            @foreach($region as $regions)
                                                <div class="adresbox_drop_item">
                                                    <p class="title" data-name="{{$regions->region_name}}"
                                                       data-id="{{$regions->id}}">{{ $regions->region_name }}</p>

                                                    @foreach($city as $citys)
                                                        @if($citys->region_id == $regions->id)


                                                            <p data-name="@if($citys->id == 1)
                                                            {{$citys->name}}
                                                            @else
                                                            {{ $regions->region_name }},{{$citys->name}}
                                                            @endif"
                                                               data-id="{{$citys->id}}"
                                                               data-region_id="{{$citys->region_id}}"
                                                               class="btn">{{$citys->name}}
                                                                <svg width="14" height="10" viewBox="0 0 14 10"
                                                                     fill="none"
                                                                     xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M1 5L5 9L13 1" stroke="#383B39"
                                                                          stroke-width="1.5"
                                                                          stroke-linecap="round"
                                                                          stroke-linejoin="round"/>
                                                                </svg>
                                                            </p>

                                                        @endif
                                                    @endforeach
                                                </div>
                                            @endforeach
                                            <div class="adresbox_drop_item">
                                                @foreach($region as $regions)

                                                    <p class="title" data-name="{{$regions->region_name}}"
                                                       data-id="{{$regions->id}}">{{ $regions->region_name }}</p>
                                                    @foreach($city as $citys)

                                                        @if($citys->region_id == $regions->id)
                                                            <p data-name="@if($citys->id == 1)
                                                            {{$citys->name}}
                                                            @else
                                                            {{ $regions->region_name }},{{$citys->name}}
                                                            @endif"
                                                               data-id="{{$citys->id}}"
                                                               data-region_id="{{$citys->region_id}}"
                                                               class="btn">{{$citys->name}}
                                                                <svg width="14" height="10" viewBox="0 0 14 10"
                                                                     fill="none"
                                                                     xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M1 5L5 9L13 1" stroke="#383B39"
                                                                          stroke-width="1.5"
                                                                          stroke-linecap="round"
                                                                          stroke-linejoin="round"/>
                                                                </svg>
                                                            </p>

                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Если ваш бизнес находится в столице или в региональном центре указывайте
                                                только город. Если ваш бизнес находится в регионе или области, в этом
                                                случае сначала укажите регион или область, затем город
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Сайт</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" class="aplication_form_input sillkainp" id="website"
                                               name="website">
                                        <p class="sillkatext">
                                            <span></span>
                                            <button class="clearlink" type="button" name="button">
                                                <svg width="12" height="12" viewBox="0 0 12 12" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M8.82842 3.17188L3.17157 8.82873" stroke="#0E161E"
                                                          stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M3.17157 3.17188L8.82842 8.82873" stroke="#0E161E"
                                                          stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>
                                            </button>
                                        </p>
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>

                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Соц.сети</span>
                                    <div class="error_input_wrapper">
                                        <input type="text" class="aplication_form_input sillkainp" id="social_network"
                                               name="social_network">
                                        <p class="sillkatext">
                                            <span></span>
                                            <button class="clearlink" type="button" name="button">
                                                <svg width="12" height="12" viewBox="0 0 12 12" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M8.82842 3.17188L3.17157 8.82873" stroke="#0E161E"
                                                          stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M3.17157 3.17188L8.82842 8.82873" stroke="#0E161E"
                                                          stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>
                                            </button>
                                        </p>
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>

                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                    </div>
                                </div>
                                <div class="aplication_form_inputs_field">
                                    <span class="aplication_form_input_title">Номер телефона *</span>
                                    <div class="error_input_wrapper">
                                        <input placeholder="+7 (_ _ _) _ _ _  _ _  _ _" type="text"
                                               class="aplication_form_input inpnummask" name="phone_number"
                                               id="phone_number">
                                        <span class="error_text1">Введите корректный адрес электронной почты</span>
                                    </div>
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Вы можете скрыть номер и общаться с покупателями только в чате. Для
                                                этого нужно выбрать «Запретить звонки»
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex inputcheck">
                                    <div class="aplication_form_inputs_field form_inp1">

                                        <label for="check_input_" class="check_label" name="barring_calls">
                                            <input type="checkbox" checked class="area_check_input" value="0"
                                                   name="barring_calls" id="check_input_">
                                            <span class="checkboxspan"></span>
                                            <p class="check_text">Запретить звонки</p>
                                        </label>

                                    </div>
                                </div>
                                <div class="aplication_form_btn_wrapper">
                                    <button class="aplication_form_btn btn_step6">Далее</button>
                                </div>
                            </div>

                        </form>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Адрес *
                                </p>
                                <p class="aplication_form_input_answer_info" id="adress1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сайт</p>
                                <p class="aplication_form_input_answer_info" id="website1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Соц.сети
                                </p>
                                <p class="aplication_form_input_answer_info" id="social_network1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Номер телефона *

                                </p>
                                <p class="aplication_form_input_answer_info" id="phone_number1"></p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Запретить звонки
                                </p>
                                <p class="aplication_form_input_answer_info" id="phone_number2"></p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </div>
                    <div class="aplication_form_item" id="open_form7">
                        <form id="ajax_step7" accept-charset="utf-8">

                            <input type="hidden" value="step7" name="step">

                            <div class="aplication_form_item_first_child">
                                <div class="aplication_form_inputs_field aplication_form_inputs_fieldtextarea">
                                    <span class="aplication_form_input_title">Дополнительная информация </span>
                                    <textarea name="additional_information" rows="8" cols="80"></textarea>
                                    <div class="aplication_form_hint_wrapper">
                                        <div class="aplication_form_hint_svg">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="8" cy="8" r="7.5" stroke="#E5D6A0"/>
                                                <path
                                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                                    fill="#E5D6A0"/>
                                            </svg>
                                        </div>
                                        <div class="aplication_form_hint_box_wrapper">
                                            <p class="aplication_form_hint_text">
                                                Вы можете указать информацию в свободной форме. Напишите то, что
                                                считаете нужным и то, что поможет вам продать ваш бизнес быстрее.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="aplication_form_btn_wrapper">
                                    <button class="aplication_form_save_btn">Сохранить</button>
                                </div>
                            </div>
                        </form>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Название компании</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сфера деятельности</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Активность бизнеса</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Причина продажи</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Адрес</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            {{--                            <div class="aplication_form_inputs_answers">--}}
                            {{--                                <p class="aplication_form_input_answer_title">Какие объекты находятся рядом</p>--}}
                            {{--                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>--}}
                            {{--                            </div>--}}
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сайт, соцсети</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Цена</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Фото</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mobile_aplication_form_items_wrapper">

                    <form class="mobile_aplication_form_item">
                        <p class="mobile_aplication_form_items_title">Общая информация</p>
                        <div class="aplication_form_item_first_child open-display1">
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Название компании</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Сфера деятельности</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Активность бизнеса</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Причина продажи</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Адрес</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            {{--                            <div class="aplication_form_inputs_field">--}}
                            {{--                                <span class="aplication_form_input_title">Какие объекты находятся рядом</span>--}}
                            {{--                                <input type="text" class="aplication_form_input">--}}
                            {{--                            </div>--}}
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Сайт, соцсети</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Цена</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Фото</span>
                                <label for="fileinput_form2" class=" write_us_input file_label">

                                    <svg width="30" height="26" viewBox="0 0 30 26" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M25 13V6.55556C25 5.96619 24.7701 5.40095 24.361 4.98421C23.9518 4.56746 23.3968 4.33333 22.8182 4.33333H18.4545L16.2727 1H9.72727L7.54545 4.33333H3.18182C2.60316 4.33333 2.04821 4.56746 1.63904 4.98421C1.22987 5.40095 1 5.96619 1 6.55556V18.7778C1 19.3671 1.22987 19.9324 1.63904 20.3491C2.04821 20.7659 2.60316 21 3.18182 21H17"
                                            stroke="#D2BE8B" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"/>
                                        <path
                                            d="M13 17C15.2091 17 17 15.2091 17 13C17 10.7909 15.2091 9 13 9C10.7909 9 9 10.7909 9 13C9 15.2091 10.7909 17 13 17Z"
                                            stroke="#D2BE8B" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"/>
                                        <path d="M29 21H21" stroke="#D2BE8B" stroke-width="1.5" stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                        <path d="M25 17V25" stroke="#D2BE8B" stroke-width="1.5" stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                    </svg>
                                    <span class="file_span">Добавить фотографии</span>
                                    <input type="file" id="fileinput_form2" hidden>
                                </label>
                            </div>

                        </div>
                        <div class="aplication_form_item_second_child  hide-display1">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Название компании</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сфера деятельности</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Активность бизнеса</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Причина продажи</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Адрес</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            {{--                            <div class="aplication_form_inputs_answers">--}}
                            {{--                                <p class="aplication_form_input_answer_title">Какие объекты находятся рядом</p>--}}
                            {{--                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>--}}
                            {{--                            </div>--}}
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сайт, соцсети</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Цена</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Фото</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </form>
                    <form class="mobile_aplication_form_item">
                        <p class="mobile_aplication_form_items_title">Сведения о деятельности</p>
                        <div class="aplication_form_item_first_child">
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Организационно-правовая форма</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Основные направления деятельности предприятия, виды выпускаемой продукции, оказываемых услуг</span>
                                <textarea class="aplication_form_textarea_field"></textarea>
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Количество работников</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Количество руководящего персонала</span>
                                <input type="text" class="aplication_form_input">
                            </div>

                        </div>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Название компании</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сфера деятельности</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Активность бизнеса</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Причина продажи</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Адрес</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            {{--                            <div class="aplication_form_inputs_answers">--}}
                            {{--                                <p class="aplication_form_input_answer_title">Какие объекты находятся рядом</p>--}}
                            {{--                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>--}}
                            {{--                            </div>--}}
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сайт, соцсети</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Цена</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Фото</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </form>
                    <form class="mobile_aplication_form_item">
                        <p class="mobile_aplication_form_items_title">Сведения об объекте</p>
                        <div class="aplication_form_item_first_child">
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Аренда/собственность</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Жилое/нежилое</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Этаж</span>
                                <input type="text" class="aplication_form_input">
                            </div>

                        </div>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Название компании</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сфера деятельности</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Активность бизнеса</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Причина продажи</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Адрес</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            {{--                            <div class="aplication_form_inputs_answers">--}}
                            {{--                                <p class="aplication_form_input_answer_title">Какие объекты находятся рядом</p>--}}
                            {{--                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>--}}
                            {{--                            </div>--}}
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сайт, соцсети</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Цена</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Фото</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </form>
                    <form class="mobile_aplication_form_item">
                        <p class="mobile_aplication_form_items_title">Сведения об имуществе</p>

                        <div class="aplication_form_item_first_child">
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Перечень объектов недвижимости, которые находятся в собственности</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Общая  площадь (м²)</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                            <span class="aplication_form_input_title">Перечень оборудования,
                                передаваемого при продаже</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Наличие лицензий, сертификатов</span>
                                <input type="text" class="aplication_form_input">
                            </div>

                        </div>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Название компании</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сфера деятельности</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Активность бизнеса</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Причина продажи</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Адрес</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            {{--                            <div class="aplication_form_inputs_answers">--}}
                            {{--                                <p class="aplication_form_input_answer_title">Какие объекты находятся рядом</p>--}}
                            {{--                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>--}}
                            {{--                            </div>--}}
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Сайт, соцсети</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Цена</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Фото</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </form>
                    <form class="mobile_aplication_form_item">
                        <p class="mobile_aplication_form_items_title">Финансовая информация</p>
                        <div class="aplication_form_item_first_child">
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Средний оборот в месяц ₽ *</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Фонд заработной платы, руб.</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                            <span class="aplication_form_input_title">
                                Стоимость аренды</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Стоимость коммунальных платежей</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Операционные расходы, руб.</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Чистая прибыль в месяц, руб.</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Срок окупаемости бизнеса</span>
                                <input type="text" class="aplication_form_input">
                            </div>
                            <div class="aplication_form_inputs_field">
                                <span class="aplication_form_input_title">Наличие задолжностей, штрафов</span>
                                <input type="text" class="aplication_form_input">
                            </div>

                            <div class="aplication_form_btn_wrapper">
                                <button class="aplication_form_save_btn">Сохранить</button>
                            </div>
                        </div>
                        <div class="aplication_form_item_second_child">
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Средний оборот в месяц ₽ *</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Фонд заработной платы, руб.</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Стоимость аренды</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Стоимость коммунальных платежей</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Операционные расходы, руб.</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Чистая прибыль в месяц, руб.</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Срок окупаемости бизнеса</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answers">
                                <p class="aplication_form_input_answer_title">Наличие задолжностей, штрафов</p>
                                <p class="aplication_form_input_answer_info">Lorem Ipsum</p>
                            </div>
                            <div class="aplication_form_inputs_answer_btn_wrapper">
                                <button class="aplication_form_inputs_answer_btn">Редактировать</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </section>
</main>

@include('include.footer')
<script src="{{ asset('website/js/form.js') }}"></script>
<script src="{{ asset('website/js/jquery.tagsinput.js' )}}"></script>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js'></script>
<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery.maskedinput@1.4.1/src/jquery.maskedinput.min.js"
        type="text/javascript"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tagify/4.6.0/tagify.min.js"
        integrity="sha512-xsrKai0DCgM+KcdCFeCWihZg8GZpDmtBH/boyC/blTmeceRRXYQzbJyGMYJD0TqyBvjbQlsYUjs25zfp4EB2Zw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tagify/4.6.0/tagify.min.css"
      integrity="sha512-PORIIhNf+D+ZYNDS1gC01CA7YEGkzNzPZu4Ja0m7ljo4SHJ2+xwRL04ysc0qCVh/brYgAbGjIHtqdwKzvDfQ3g=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>

<script type="text/javascript">


    $("input[name='data']").on('keyup', function () {
        let val = $(this).val()
        let d = val.substring(0, 2)
        let m = val.substring(3, 5)
        let y = val.substring(6, 10)
        let now = new Date().toLocaleDateString();
        let nowy = now.substring(6, 10)

        if (d > 31 || m > 12 || y > nowy || val.length < 10) {
            $('#data1').css('border', '1px solid #c34d4d')
            $(".btn_step1").attr('disabled',true)
        } else {
            $('#data1').css('border','1px solid #9B8E6C')
            $(".btn_step1").attr('disabled',false)
        }

    })


    $('input').attr('autocomplete', 'off')

    $("input[name='adress']").on('keyup', function () {
        let val = $(this).val()
        console.log(val)
        $.ajax({
            url: "{{ route('create_survey') }}",
            method: "POST",
            data: {
                val: val
            },
            // dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                var new_content = '';

                $.each(data, function (i, value) {


                    new_content += `

                            <div class="adresbox_drop_item">
                                          <p class="title" data-name="` + value[0].region_id + `"
                                              data-id="` + value[0].region_id + `">` + value[0].region_name + `</p>



                        `;
                    for (var j = 1; j < value.length; j++) {
                        if (value[j].name == 'Москва' || value[j].name == "Санкт-Петербург" || value[j].name == "Севастополь") {
                            new_content += `

                                    <p data-name="` + value[j].name + `"  data-region_id="` + value[0].region_id + `" data-id="` + value[j].id + `" class="btn">` + value[j].name +
                                `<svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                  stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </p>`
                        } else {
                            new_content += `

                                    <p data-name="` + value[j].region_name + `,` + value[j].name + `"  data-region_id="` + value[0].region_id + `" data-id="` + value[j].id + `" class="btn">` + value[j].name +
                                `<svg width="14" height="10" viewBox="0 0 14 10" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 5L5 9L13 1" stroke="#383B39" stroke-width="1.5"
                                  stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </p>`
                        }

                    }

                    new_content += `</div>`


                })

                $('.adresbox_drop').html(new_content)
                var datavalu = ""
                var dataregionid = ""
                var datacityid = ""
                $(".adresbox_drop_item .btn").click(function (event) {
                    datavalu = $(this).data("name")
                    dataregionid = $(this).data("region_id")
                    datacityid = $(this).data("id")
                    $(".ardes_input").val(datavalu);
                    $("#region").val(dataregionid);
                    $("#city").val(datacityid);
                    $(".adresbox_drop").slideUp(500)
                })
            }

        })
    })


    $('.aplication_form_inputs_answer_btn').on('click', function () {
        $(this).parents('.aplication_form_item').find('.aplication_form_item_first_child').show()
        $(this).parents('.aplication_form_item_second_child').hide()
    })
    // $('.select_item').on('click', function () {
    //     let val = $(this).children("p").data('name')
    //
    //     if (val == 'Есть') {
    //         let name = $(this).parent().parent().parent().find('select')
    //         let names = name.attr('name')
    //         $(this).parent().parent().html(`<input name="` + names + `" type="text"class="aplication_form_input" style='display: block!important' autofocus>`)
    //         $(".nomask .maskprice").removeClass("maskprice")
    //         $(".maskprice").mask('# ### ###', {
    //             reverse: true,
    //         });
    //
    //     }
    //     if (val == 'Другое') {
    //         let name = $(this).parent().parent().parent().find('select')
    //         let names = name.attr('name')
    //         $(this).parent().parent().html(`<input name="` + names + `" type="text"class="aplication_form_input" style='display: block!important' autofocus>`)
    //
    //     }
    //
    // })
    var swiper = new Swiper(".mySwiper", {
        slidesPerView: "auto",
        navigation: {
            nextEl: ".swipernext",
            prevEl: ".swiperprev",
        },

    });
    var titlearray = $(".select_title_svg_wrapper");
    swiper.on('slideChange', function () {
        if ($(".swiper-slidefirst").hasClass("swiper-slide-active")) {
            $(".swiperprev").hide()
        } else {
            $(".swiperprev").show()
        }
    });
    $(".inpnummask").mask("+7 (999) 999 99 99");
    $("#data1").mask("99.99.9999")
    $(".maskprice").mask('# ### ###', {
        reverse: true,
    });
    var titlearray = $(".aplication_form_items_title");
    var settitle = 0;
    var setleft = 0;
    var setleft2 = 0;
    var formsarray = $(".aplication_form_item");

    // $(".selectform").click(function(){
    //     $(".aplication_form_item").css("width","0");
    //     $(formsarray[$(this).val()]).css("width","100%");
    //     $(".aplication_form_items_title").removeClass("active")

    //     $(this).addClass("active")
    //     i=0;
    //     setleft2=0;
    //     settitle=$(this).val()
    //
    // })

    $(".prevformsbtn").click(function () {
        if (settitle > 1) {
            setleft = $(titlearray[settitle - 1]).width();
            setleft2 -= setleft + 45;
            settitle -= 1;
            $(".aplication_form_item").css("width", "0");
            $(formsarray[settitle]).css("width", "100%");
            $(".aplication_form_items_title").removeClass("active")
            $(titlearray[settitle]).addClass("active")
        } else {
            setleft2 = 0;
            $(".aplication_form_item").css("width", "0");
            $(formsarray[0]).css("width", "100%");
            $(".aplication_form_items_title").removeClass("active")
            $(titlearray[0]).addClass("active")
        }
    })
    var images = [];
    var setaddimg = 0;
    var datavalu =0;
    var addimgarray = $(".photo_view1")
    var g =0;
    var dataval = 0;
    let imagesPreview = function (input, placeToInsertImagePreview) {

        if (input.files) {
            let filesAmount = input.files.length;
            let images_amount = images.length;

            if (images_amount < 10) {
                for (let i = 0; i < filesAmount; i++) {
                    let reader = new FileReader();

                    reader.onload = function (event) {

                        let image = '<a class="delete"><img class="images" src=' + event.target.result + '><span class="cl_span">&#10006;</span></a>';
                        var  b = "sadjkhckajsc"
                        $(".photo_view1").click(function(){
                          setaddimg = $(this).data("name");
                          datavalu=$(this).data("name")
                          $(this).children(".delete").attr("data-value",""+datavalu+"")
                          g+=1
                          // console.log(images)
                        })
                        $($.parseHTML(image)).appendTo(addimgarray[setaddimg]);

                        $(".delete").click(function(e){
                          $(this).parent().children("label").show()
                          $(this).children("img").attr("data-name",""+input.files[i].name+"")
                          dataval = $(this).children("img").data("name")
                          // images.splice(images.indexOf(dataval));
                          for (var ggg = 0; ggg < images.length; ggg++) {
                            if(images[ggg].name==dataval){
                              images.splice(e, ggg-1);;
                              console.log(ggg)
                            }
                          }
                          console.log(images)
                          $(this).remove()
                        })

                        $(addimgarray[setaddimg]).children("label").hide()
                    }

                    reader.readAsDataURL(input.files[i]);
                    let images_amount2 = images.length;
                    if (images_amount2 < 10) {
                        images.push(input.files[i]);
                    }


                }
            } else {
                console.log('Maximum 10 image')
            }

        }

    }

    $('#fileinput_form_photo').on('change', function () {
        if (images.length < 10) {
            imagesPreview(this, 'div.gallery');
        }
        console.log(images)
    })
    // $("div.gallery").on('click', '.delete', function (e) {
    //
    //     console.log(images)
    // });
       // if (!empty(images)){
       //
       // }

    $(document).on('click', '.selectform', function () {

        var id = $(this).data('id');

        if ($(this).hasClass('afit')) {
            $('.aplication_form_item').removeClass('open');
            $('#' + id).addClass('open')
        }


    })


    // $('.btn_step1').on('click',function (){


    // })
    $('#multi-file-upload-ajax').submit(function (event) {

        event.preventDefault();

        let textData = new FormData(this)
        textData.delete('images1');
        textData.set('images', images);

        for (let i of images) {
            textData.append('images[]', i)
        }

        $.ajax({
            url: "{{ route('create_survey') }}",
            method: "POST",
            data: textData,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                if (data.error) {

                    if (data.error['name']) {
                        $('#name').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['data']) {
                        $('#data1').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['business_activity']) {
                        $('#category_select').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['reason_sale']) {
                        $('#status_business_select').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['price']) {
                        $('#price').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['images']) {
                        $('.file_label').css('border', '1px solid #c34d4d')
                    }

                } else {
                    $(titlearray[1]).addClass("selectform")

                    // $(".selectform").click(function(){
                    //
                    //     $(".aplication_form_item").css("width","0");
                    //     $(formsarray[$(this).val()]).css("width","100%");
                    //     $(".aplication_form_items_title").removeClass("active")
                    //     $(this).addClass("active")
                    //     i=0;
                    //     setleft2=0;
                    //     settitle=$(this).val()
                    //
                    // })

                    $("#opn1").removeClass('active')
                    $("#opn2").addClass('afit')
                    $("#opn2").addClass('active')


                    $("#open_form2").addClass('open')
                    $("#open_form1").removeClass('open')
                    $("#open_form1 .aplication_form_item_first_child").css('display', 'none')
                    $("#open_form1 .aplication_form_item_second_child").css('display', 'block')

                    // $('#business_info_name').text(data.input.name)
                    // if (data.input.category == 1) {
                    //     $('#business_info_category').text('IT-бизнес')
                    // }
                    // if (data.input.category == 2) {
                    //     $('#business_info_category').text('Интернет-магазин')
                    // }
                    // if (data.input.category == 3) {
                    //     $('#business_info_category').text('Общественное питание')
                    // }
                    // if (data.input.category == 4) {
                    //     $('#business_info_category').text('Производство')
                    // }
                    // if (data.input.category == 5) {
                    //     $('#business_info_category').text('Развлечение')
                    // }
                    // if (data.input.category == 6) {
                    //     $('#business_info_category').text('Сельское хозяйство')
                    // }
                    // if (data.input.category == 7) {
                    //     $('#business_info_category').text('Строительство')
                    // }
                    // if (data.input.category == 8) {
                    //     $('#business_info_category').text('Сфера услуг')
                    // }
                    // if (data.input.category == 9) {
                    //     $('#business_info_category').text('Торговля')
                    // }
                    // if (data.input.category == 10) {
                    //     $('#business_info_category').text('Другое')
                    // }
                    //
                    // if (data.input.status_business == 0) {
                    //     $('#business_info_status_business').text('Действующий')
                    // } else {
                    //     $('#business_info_status_business').text('Приостановлен')
                    // }


                    $('#name1').text(data.input.name)
                    $('#data').text(data.input.data)
                    if (data.input.business_activity == 0) {
                        $('#business_activity1').text('Действующий')
                    } else {
                        $('#business_activity1').text('Приостановлен')
                    }
                    $('#reason_sale').text(data.input.reason_sale)
                    if (data.input.reason_sale == 0) {
                        $('#reason_sale').text('Переезд')
                    }
                    if (data.input.reason_sale == 1) {
                        $('#reason_sale').text('Не хватает времени')
                    }
                    if (data.input.reason_sale == 2) {
                        $('#reason_sale').text('Разногласия с партнерами')
                    }
                    if (data.input.reason_sale == 3) {
                        $('#reason_sale').text('Не могу набрать сотрудников')
                    }
                    if (data.input.reason_sale == 4) {
                        $('#reason_sale').text('Нужны деньги')
                    }
                    if (data.input.reason_sale == 5) {
                        $('#reason_sale').text('Хочу отдыхать')
                    }


                    if (data.input.link_video) {
                        var link_video = JSON.parse(data.input.link_video);
                        var site_link = ''

                        for (let i = 0; i < link_video.length; i++) {

                            if (link_video.length > 1 && i < link_video.length - 1) {
                                site_link += link_video[i].value + ','
                            } else {
                                site_link += link_video[i].value
                            }

                        }

                        $('#site_link').text(site_link)
                    }
                    $('#price1').text(data.input.price)
                    console.log(data.images);
                    let image = '';
                    for(i = 0;i < data.images.length;i++){
                       image +=`<img src="`+ `/storage/business_gallery/`+ data.images[i] +`"/>`

                    }
                    console.log(image);
                    $(".photo_view12").html(image);

                    // let img = data.input.file
                    // for(i = 0; i < img.length;i++){
                    //     $('.photo_view1').append(`<img src="`+ img[i].name +`" alt="">`)
                    //
                    // }

                    // if (data.input.phone == 0) {
                    //     $('#phone').html(`<span>&#10005;</span>`)
                    // } else {
                    //     $('#phone').html(`<span>&#10004;</span>`)
                    // }
                    // $('#business_info_price').text(data.input.price)
                    // if (data.input.urgently == 0) {
                    //     $('#urgently').html(`<span>&#10005;</span>`)
                    // } else {
                    //     $('#urgently').html(`<span>&#10004;</span>`)
                    // }

                }
            }
        });
    });
    // $('.btn_step2').on('click',function (){
    //     $('#ajax_step2').submit()
    // })
    $('#ajax_step2').submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "{{ route('create_survey') }}",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                if (data.error) {
                    if (data.error['field_of_activity']) {
                        $('#field_of_activity1').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['status_business']) {
                        $('#status_business1').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['services_provided']) {
                        $('#services_provided').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['amount_of_workers']) {
                        $('#amount_of_workers').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['number_of_management_personnel']) {
                        $('#number_of_management_personnel').css('border', '1px solid #c34d4d')
                    }
                } else {


                    $(titlearray[2]).addClass("selectform")

                    // $(".selectform").click(function(){
                    //     $(".aplication_form_item").css("width","0");
                    //     $(formsarray[$(this).val()]).css("width","100%");
                    //     $(".aplication_form_items_title").removeClass("active")
                    //     $(this).addClass("active")
                    //     i=0;
                    //     setleft2=0;
                    //     settitle=$(this).val()
                    //
                    // })

                    $("#opn2").removeClass('active')
                    $("#opn3").addClass('afit')
                    $("#opn3").addClass('active')
                    $("#open_form3").addClass('open')
                    $("#open_form2").removeClass('open')
                    $("#open_form2 .aplication_form_item_first_child").css('display', 'none')
                    $("#open_form2 .aplication_form_item_second_child").css('display', 'block')

                    $('#field_of_activity').text(data.input.field_of_activity);

                    if (data.input.field_of_activity == 13) {
                        $('#field_of_activity').text('IT-бизнес')
                    }
                    if (data.input.field_of_activity == 14) {
                        $('#field_of_activity').text('Интернет-магазин')
                    }
                    if (data.input.field_of_activity == 15) {
                        $('#field_of_activity').text('Общественное питание')
                    }
                    if (data.input.field_of_activity == 16) {
                        $('#field_of_activity').text('Производство')
                    }
                    if (data.input.field_of_activity == 17) {
                        $('#field_of_activity').text('Развлечение')
                    }
                    if (data.input.field_of_activity == 18) {
                        $('#field_of_activity').text('Сельское хозяйство')
                    }
                    if (data.input.field_of_activity == 19) {
                        $('#field_of_activity').text('Строительство')
                    }
                    if (data.input.field_of_activity == 20) {
                        $('#field_of_activity').text('Сфера услуг')
                    }
                    if (data.input.field_of_activity == 21) {
                        $('#field_of_activity').text('Торговля')
                    }

                    $('#business_info_organizational_legal_form').text(data.input.organizational_legal_form)

                    $('#status_business').text(data.input.status_business)

                    if (data.input.status_business == 0) {
                        $('#status_business').text('АО')
                    }
                    if (data.input.status_business == 1) {
                        $('#status_business').text('ЗАО')
                    }
                    if (data.input.status_business == 2) {
                        $('#status_business').text('ООО')
                    }
                    if (data.input.status_business == 3) {
                        $('#status_business').text('ИП')
                    }
                    if (data.input.status_business == 4) {
                        $('#status_business').text('Самозанятость')
                    }
                    $('#organizational_legal_form1').text(data.input.organizational_legal_form)

                    $('#services_provided1').text(data.input.services_provided)
                    if (data.input.services_provided == 0) {
                        $('#services_provided1').text('Нет')
                    }
                    $('#selling_product1').text(data.input.selling_product)

                    if (data.input.selling_product == 0) {
                        $('#selling_product1').text('Нет')
                    }

                    $('#amount_of_workers1').text(data.input.amount_of_workers)
                    $('#number_of_management_personnel1').text(data.input.number_of_management_personnel)
                }
            }
        });
    });
    // $('.btn_step3').on('click',function (){
    //     $('#ajax_step3').submit()
    // })
    $(".aplication_form_items_title").addClass("selectform")
    $('#ajax_step3').submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "{{ route('create_survey') }}",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                if (data.error) {
                    if (data.error['object_which_activity_carried_out']) {
                        $('#object_which_activity_carried_out2').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['purpose_real_estate_object_where_activity_carried_out']) {
                        $('#residential_nonresidential_select').css('border', '1px solid #c34d4d')
                    }
                    if (data.error['type_ownership']) {
                        $('#type_ownership').css('border', '1px solid #c34d4d')
                    }
                } else {
                    $(titlearray[3]).addClass("selectform")
                    // $(".selectform").click(function(){
                    //     $(".aplication_form_item").css("width","0");
                    //     $(formsarray[$(this).val()]).css("width","100%");
                    //     $(".aplication_form_items_title").removeClass("active")
                    //     $(this).addClass("active")
                    //     i=0;
                    //     setleft2=0;
                    //     settitle=$(this).val()
                    //
                    // })
                    $("#opn3").removeClass('active')
                    $("#opn4").addClass('afit')
                    $("#opn4").addClass('active')
                    $("#open_form4").addClass('open')
                    $("#open_form3").removeClass('open')
                    $("#open_form3 .aplication_form_item_first_child").css('display', 'none')
                    $("#open_form3 .aplication_form_item_second_child").css('display', 'block')

                    $('#object_which_activity_carried_out').text(data.input.object_which_activity_carried_out)
                    if (data.input.object_which_activity_carried_out == 0) {
                        $('#object_which_activity_carried_out').text('Торговый центр')
                    }
                    if (data.input.object_which_activity_carried_out == 1) {
                        $('#object_which_activity_carried_out').text('Офис здание')
                    }
                    if (data.input.object_which_activity_carried_out == 2) {
                        $('#object_which_activity_carried_out').text('Производственная база')
                    }
                    if (data.input.object_which_activity_carried_out == 3) {
                        $('#object_which_activity_carried_out').text('Ферма')
                    }
                    if (data.input.object_which_activity_carried_out == 4) {
                        $('#object_which_activity_carried_out').text('Дом')
                    }
                    $('#residential_nonresidential_select1').text(data.input.purpose_real_estate_object_where_activity_carried_out)
                    if (data.input.purpose_real_estate_object_where_activity_carried_out == 0) {
                        $('#residential_nonresidential_select1').text('Жилое')
                    }
                    if (data.input.purpose_real_estate_object_where_activity_carried_out == 1) {
                        $('#residential_nonresidential_select1').text('Нежилое')
                    }
                    $('#type_ownership1').text(data.input.type_ownership)
                    if (data.input.type_ownership == 0) {
                        $('#type_ownership1').text('Аренда')
                    }
                    if (data.input.type_ownership == 1) {
                        $('#type_ownership1').text('Собственность')
                    }
                    $('#floor1').text(data.input.floor)
                    if (data.input.floor == 0) {
                        $('#floor1').text('Цокольный')
                    }
                }
            }
        });
    });
    // $('.btn_step4').on('click',function (){
    //     $('#ajax_step4').submit()
    // })
    $('#ajax_step4').submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "{{ route('create_survey') }}",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                if (data.error) {
                    if (data.error['properties_for_sale']) {
                        $('#properties_for_sale').css('border', '1px solid #c34d4d')
                    } else {
                        $('#properties_for_sale').css('border', '1px solid #9b8e6c')
                    }
                    if (data.error['total_area']) {
                        $('#total_area').css('border', '1px solid #c34d4d')
                    } else {
                        $('#total_area').css('border', '1px solid #9b8e6c')
                    }
                    if (data.error['licenses']) {
                        $('#licenses').css('border', '1px solid #c34d4d')
                    } else {
                        $('#licenses').css('border', '1px solid #9b8e6c')
                    }
                } else {
                    $(titlearray[4]).addClass("selectform")
                    // $(".selectform").click(function(){
                    //     $(".aplication_form_item").css("width","0");
                    //     $(formsarray[$(this).val()]).css("width","100%");
                    //     $(".aplication_form_items_title").removeClass("active")
                    //     $(this).addClass("active")
                    //     i=0;
                    //     setleft2=0;
                    //     settitle=$(this).val()
                    //
                    // })
                    $("#opn4").removeClass('active')
                    $("#opn5").addClass('afit')
                    $("#opn5").addClass('active')
                    $("#open_form5").addClass('open')
                    $("#open_form4").removeClass('open')
                    $("#open_form4 .aplication_form_item_first_child").css('display', 'none')
                    $("#open_form4 .aplication_form_item_second_child").css('display', 'block')

                    $('#properties_for_sale1').text(data.input.properties_for_sale)
                    $('#equipment_sold_sale1').text(data.input.equipment_sold_sale)
                    $('#total_area1').text(data.input.total_area)
                    if (data.input.total_area == 0) {
                        $('#total_area1').text('Нет')
                    }
                    $('#licenses1').text(data.input.licenses)
                }
            }
        });
    });
    // $('.btn_step5').on('click',function (){
    //     $('#ajax_step5').submit()
    // })
    $('#ajax_step5').submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "{{ route('create_survey') }}",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                if (data.error) {
                    if (data.error['taxation_system']) {
                        $('#taxation_system1').css('border', '1px solid #c34d4d')
                    } else {
                        $('#taxation_system1').css('border', '1px solid #9b8e6c')
                    }
                    if (data.error['average_check']) {
                        $('#average_check1').css('border', '1px solid #c34d4d')
                    } else {
                        $('#average_check1').css('border', '1px solid #9b8e6c')
                    }
                    if (data.error['average_monthly_turnover']) {
                        $('#average_monthly_turnover').css('border', '1px solid #c34d4d')
                    } else {
                        $('#average_monthly_turnover').css('border', '1px solid #9b8e6c')
                    }
                    if (data.error['rentals']) {
                        $('#rentals').css('border', '1px solid #c34d4d')
                    } else {
                        $('#rentals').css('border', '1px solid #9b8e6c')
                    }
                    if (data.error['general_expenses']) {
                        $('#general_expenses').css('border', '1px solid #c34d4d')
                    } else {
                        $('#general_expenses').css('border', '1px solid #9b8e6c')
                    }
                    if (data.error['net_profit_per_month']) {
                        $('#net_profit_per_month').css('border', '1px solid #c34d4d')
                    } else {
                        $('#net_profit_per_month').css('border', '1px solid #9b8e6c')
                    }
                    if (data.error['business_payback_period']) {
                        $('#business_payback_period').css('border', '1px solid #c34d4d')
                    } else {
                        $('#business_payback_period').css('border', '1px solid #9b8e6c')
                    }
                    if (data.error['debts_fines']) {
                        $('#debts_fines').css('border', '1px solid #c34d4d')
                    } else {
                        $('#debts_fines').css('border', '1px solid #9b8e6c')
                    }
                } else {
                    $(titlearray[5]).addClass("selectform")
                    // $(".selectform").click(function(){
                    //     $(".aplication_form_item").css("width","0");
                    //     $(formsarray[$(this).val()]).css("width","100%");
                    //     $(".aplication_form_items_title").removeClass("active")
                    //     $(this).addClass("active")
                    //     i=0;
                    //     setleft2=0;
                    //     settitle=$(this).val()
                    //
                    // })
                    $("#opn5").removeClass('active')
                    $("#opn6").addClass('afit')
                    $("#opn6").addClass('active')
                    $("#open_form6").addClass('open')
                    $("#open_form5").removeClass('open')
                    $("#open_form5 .aplication_form_item_first_child").css('display', 'none')
                    $("#open_form5 .aplication_form_item_second_child").css('display', 'block')

                    $('#taxation_system').text(data.input.taxation_system)
                    if (data.input.taxation_system == 0) {
                        $('#taxation_system').text('Нет')
                    }
                    if (data.input.taxation_system == 1) {
                        $('#taxation_system').text('ОСН')
                    }
                    if (data.input.taxation_system == 2) {
                        $('#taxation_system').text('УСН')
                    }
                    if (data.input.taxation_system == 3) {
                        $('#taxation_system').text('ЕСХН')
                    }
                    if (data.input.taxation_system == 4) {
                        $('#taxation_system').text('ПСН')
                    }
                    if (data.input.taxation_system == 5) {
                        $('#taxation_system').text('НПД')
                    }
                    $('#average_check').text(data.input.average_check)
                    if (data.input.average_check == 0) {
                        $('#average_check').text('Нет')
                    }
                    $('#average_monthly_turnover1').text(data.input.average_monthly_turnover)
                    if (data.input.average_monthly_turnover == 0) {
                        $('#average_monthly_turnover1').text('Нет')
                    }
                    $('#salary_fund').text(data.input.salary_fund)
                    $('#rentals1').text(data.input.rentals)
                    $('#cost_utility_bills1').text(data.input.cost_utility_bills)
                    $('#general_expenses1').text(data.input.general_expenses)
                    if (data.input.general_expenses == 0) {
                        $('#general_expenses1').text('Нет')
                    }
                    $('#net_profit_per_month1').text(data.input.net_profit_per_month)
                    if (data.input.net_profit_per_month == 0) {
                        $('#net_profit_per_month1').text('Нет')
                    }
                    $('#business_payback_period1').text(data.input.business_payback_period)
                    if (data.input.business_payback_period == 0) {
                        $('#business_payback_period1').text('Нет')
                    }
                    $('#debts_fines1').text(data.input.debts_fines)
                    if (data.input.debts_fines == 0) {
                        $('#debts_fines1').text('Нет')
                    }

                }
            }
        });
    });
    // $('.btn_step6').on('click',function (){
    //     $('#ajax_step6').submit()
    // })
    $('#ajax_step6').submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "{{ route('create_survey') }}",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                if (data.error) {
                    if (data.error['adress']) {
                        $('#adress').css('border', '1px solid #c34d4d')
                    } else {
                        $('#adress').css('border', '1px solid #9b8e6c')
                    }
                    if (data.error['phone_number']) {
                        $('#phone_number').css('border', '1px solid #c34d4d')
                    } else {
                        $('#phone_number').css('border', '1px solid #9b8e6c')
                    }

                } else {
                    $(titlearray[6]).addClass("selectform")

                    // $(".selectform").click(function(){
                    //     $(".aplication_form_item").css("width","0");
                    //     $(formsarray[$(this).val()]).css("width","100%");
                    //     $(".aplication_form_items_title").removeClass("active")
                    //     $(this).addClass("active")
                    //     i=0;
                    //     setleft2=0;
                    //     settitle=$(this).val()
                    //
                    // })


                    $("#opn6").removeClass('active')
                    $("#opn7").addClass('afit')
                    $("#opn7").addClass('active')
                    $("#open_form7").addClass('open')
                    $("#open_form6").removeClass('open')
                    $("#open_form6 .aplication_form_item_first_child").css('display', 'none')
                    $("#open_form6 .aplication_form_item_second_child").css('display', 'block')

                    if (data.input.barring_calls == 1) {
                        $('#phone_number2').html(`<span>&#10005;</span>`)
                    } else {
                        $('#phone_number2').html(`<span>&#10004;</span>`)
                    }
                    $('#adress1').text(data.input.adress)


                    var website = ''
                    if(data.input.website){
                        var websites = JSON.parse(data.input.website);
                        for (let i = 0; i < websites.length; i++) {

                            if (websites.length > 1 && i < websites.length - 1) {
                                website += websites[i].value + ','
                            } else {
                                website += websites[i].value
                            }

                        }
                    }

                    $('#website1').text(website)


                    var social_networks = ''
                    if(data.input.social_network){
                        var social_network = JSON.parse(data.input.social_network);
                        for (let i = 0; i < social_network.length; i++) {

                            if (social_network.length > 1 && i < social_network.length - 1) {
                                social_networks += social_network[i].value + ','
                            } else {
                                social_networks += social_network[i].value
                            }

                        }
                    }



                    $('#social_network1').text(social_networks)
                    $('#phone_number1').text(data.input.phone_number)


                }
            }
        });
    });
    $('#ajax_step7').submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "{{ route('create_survey') }}",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                if (data.error) {

                } else {
                    location.href = "/";
                }
            }
        });
    });
    var link = '<p class="sillkatext"><span></span><button class="clearlink" type="button" name="button"><svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.82842 3.17188L3.17157 8.82873" stroke="#0E161E" stroke-linecap="round" stroke-linejoin="round"/><path d="M3.17157 3.17188L8.82842 8.82873" stroke="#0E161E" stroke-linecap="round" stroke-linejoin="round"/></svg></button></p>'
    // $(".sillkainp").on("input", function () {
    //     $(this).siblings(".silkaflex").append(link)
    //     $(this).siblings(".silkaflex").css("z-index", "2")
    //     $(this).siblings(".silkaflex").children(".sillkatext").children("span").text($(this).val());
    //     $(this).siblings(".sillkatext").show()
    //     $(".clearlink").click(function () {
    //         $(this).siblings("span").text("");
    //         $(this).parent().siblings(".sillkainp").val("")
    //         $(this).parent().hide()
    //         console.log("das")
    //     })
    //     $(".silkaflex").click(function () {
    //         $(this).siblings(".sillkainp").focus()
    //     })
    // })
    // $('.select_item').click(function () {
    //     var b = setInterval(function () {
    //         for (var i = 0; i <= titlearray.length; i++) {
    //             if ($(titlearray[i]).children("p").text() == "Другое") {
    //                 $(titlearray[i]).children("input").show();
    //                 console.log($(titlearray[i]).children("p").text())
    //                 $(titlearray[i]).children(".select_title").text("")
    //             } else {
    //                 $(titlearray[i]).children("input").hide();
    //             }
    //         }
    //         var g = clearInterval(b, 200)
    //     }, 10)
    //     var titlearray = $(".select_title_svg_wrapper");
    //
    //     $('.select_item').click(function () {
    //         var b = setInterval(function () {
    //             for (var i = 0; i <= titlearray.length; i++) {
    //                 if ($(titlearray[i]).children("p").text() == "Другое") {
    //                     $(titlearray[i]).children("input").show();
    //                     console.log($(titlearray[i]).children("p").text())
    //                     $(titlearray[i]).children(".select_title").text("")
    //                 } else {
    //                     $(titlearray[i]).children("input").hide();
    //                 }
    //             }
    //             var g = clearInterval(b, 200)
    //         }, 10)
    //
    //     })
    // });
    // $(".sillkainp").on("input", function () {
    //     $(this).siblings(".sillkatext").children("span").text($(this).val());
    //     $(this).siblings(".sillkatext").show()
    // })
    // $(".clearlink").click(function () {
    //     $(this).siblings("span").text("");
    //     $(this).parent().siblings(".sillkainp").val("")
    //     $(this).parent().hide()
    // })


    var input = document.querySelector('input[name=link_video]');
    new Tagify(input)
    var website = document.querySelector('input[name=website]');
    new Tagify(website)
    var social_network = document.querySelector('input[name=social_network]');
    new Tagify(social_network)
    var testinp = 0;
    tagTextProp: 'value',
        // $(".aplication_form_btn").click(function(){
        //     testinp = $(".open .obinp");
        //     for(var i = 0; i <=testinp.length; i++){
        //
        //       if($(testinp[i]).val()!=""){
        //         if(settitle<titlearray.length-1){
        //             settitle++;
        //
        //             $(".aplication_form_items_title").removeClass("active")
        //             $(titlearray[settitle]).addClass("active")
        //             $(".aplication_form_item").css("width","0");
        //             $(formsarray[settitle]).css("width","100%");
        //             $(".aplication_form_item").removeClass("open");
        //             $(formsarray[settitle]).addClass("open");
        //         }
        //       }else{
        //         return
        //       }
        //     }
        //
        // })
        $(".firstsvg").click(function (event) {
            $(".adresbox_drop").slideDown(500)
            event.stopPropagation()
            $("body").click(function () {
                $(".adresbox_drop").slideUp(500)
            })
        })
    $(".ardes_input").on("input", function (event) {
        $(".adresbox_drop").slideDown(500)
        event.stopPropagation()
        $("body").click(function () {
            $(".adresbox_drop").slideUp(500)
        })
    })
    $(".adresbox_drop_item .title").click(function (event) {
        event.stopPropagation()
    })

    var datavalu = ""
    var dataregionid = ""
    var datacityid = ""
    $(".adresbox_drop_item .btn").click(function (event) {
        datavalu = $(this).data("name")
        dataregionid = $(this).data("region_id")
        dataregionid = $(this).data("id")
        $(".ardes_input").val(datavalu);
        $("#region").val(dataregionid);
        $("#city").val(datacityid);
        $(".adresbox_drop").slideUp(500)
    })
    var swiper = new Swiper(".galerycontswiper", {
        slidesPerView: 'auto',
        loop: false,
    });
    $(".select_item").click(function(){
      if($(this).children("p").data("name")=="Другое"){
        $(this).parent().parent().hide()
        $(".select_title_svg_wrapper .select_title").text(" ")
        $(this).parent().parent().siblings(".hideninpandshow").show()
        $(this).parent().parent().siblings(".hideninpandshow").focus();
      }
      if($(this).children("p").data("name")=="Есть"){
        $(this).parent().parent().hide()
        $(".select_title_svg_wrapper .select_title").text(" ")
        $(this).parent().parent().siblings(".hideninpandshow").show()
        $(this).parent().parent().siblings(".hideninpandshow").focus();
      }
    })
    $(".hideninpandshow").on("input", function(){
      if($(this).val()==""){
        $(this).hide();
        $(".select_title_svg_wrapper .select_title").text(" ");
        $(this).siblings(".select_wrapper").show()
      }
    })
</script>
