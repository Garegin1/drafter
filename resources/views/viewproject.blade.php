@include('include.header')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Main</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/website/css/main.css' )}}">
    <link rel="icon" href="img/logo.png" sizes="32x32">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&amp;display=swap" rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&amp;display=swap"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&amp;display=swap"
        rel="stylesheet">

<body>
<div class="big_Container product_single">
    <div class="product_single_cont">
        <div class="product_single_cont_box">
            <div class="product_single_cont_box_top">
                <div class="product_single_cont_box_top_img"><img src="{{ asset('/website/images/b.png' )}}"></div>
                <div class="product_single_cont_box_top_text">
                    <div class="status">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M1.375 7.53424V1.78411C1.375 1.57615 1.45761 1.37671 1.60466 1.22966C1.75171 1.08261 1.95115 1 2.15911 1H17.8413C18.0492 1 18.2487 1.08261 18.3957 1.22966C18.5428 1.37671 18.6254 1.57615 18.6254 1.78411V7.53424C18.6254 15.7689 11.6364 18.4971 10.2409 18.9597C10.0849 19.0134 9.91545 19.0134 9.75945 18.9597C8.36395 18.4971 1.375 15.7689 1.375 7.53424Z"
                                stroke="#31CB6F" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M14.3127 6.48828L8.56253 11.977L5.6875 9.23266" stroke="#31CB6F" stroke-width="1.5"
                                  stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <div class="status_stars"><img src="img/starfill.svg"><img src="img/starfill.svg"><img
                                src="img/starfill.svg"><img src="img/starfill.svg"><img src="img/star.svg"></div>
                    </div>
                    <h1>Спортмастер</h1>
                    <div class="price"><span>Цена:</span>
                        <p>8 200 000 ₽</p></div>
                    <div class="info_item">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M12 11C13.1046 11 14 10.1046 14 9C14 7.89543 13.1046 7 12 7C10.8954 7 10 7.89543 10 9C10 10.1046 10.8954 11 12 11Z"
                                stroke="#848993" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path
                                d="M17.3952 11.3571C15.372 15.8571 12 21 12 21C12 21 8.62797 15.8571 6.60476 11.3571C4.58154 6.85714 7.95357 3 12 3C16.0464 3 19.4185 6.85714 17.3952 11.3571Z"
                                stroke="#848993" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <p>Анапа, Краснодарский край</p></div>
                    <div class="info_item">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M8.4 7.55556H15.6M8.4 7.55556H4.8C4.32261 7.55556 3.86477 7.74286 3.52721 8.07625C3.18964 8.40965 3 8.86184 3 9.33333V18.2222C3 18.6937 3.18964 19.1459 3.52721 19.4793C3.86477 19.8127 4.32261 20 4.8 20H19.2C19.6774 20 20.1352 19.8127 20.4728 19.4793C20.8104 19.1459 21 18.6937 21 18.2222V9.33333C21 8.86184 20.8104 8.40965 20.4728 8.07625C20.1352 7.74286 19.6774 7.55556 19.2 7.55556H15.6H8.4ZM8.4 7.55556V4.53333C8.4 4.39188 8.45689 4.25623 8.55816 4.15621C8.65943 4.05619 8.79678 4 8.94 4H15.06C15.2032 4 15.3406 4.05619 15.4418 4.15621C15.5431 4.25623 15.6 4.39188 15.6 4.53333V7.55556H8.4Z"
                                stroke="#848993" stroke-width="1.5"/>
                        </svg>
                        <p>Торговля</p></div>
                    <div class="info_item">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="12" cy="12" r="8.25" stroke="#848993" stroke-width="1.5"/>
                            <path d="M8.5 12.5L11.0349 15.4574C11.2609 15.721 11.6801 15.6798 11.8503 15.3772L16 8"
                                  stroke="#848993" stroke-width="1.5" stroke-linecap="round"/>
                        </svg>
                        <p>Действующий</p></div>
                    <div class="info_item">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M12 14C13.1046 14 14 13.1046 14 12C14 10.8954 13.1046 10 12 10C10.8954 10 10 10.8954 10 12C10 13.1046 10.8954 14 12 14Z"
                                stroke="#848993" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path
                                d="M3 7.35714C3 6.8646 3.18964 6.39223 3.52721 6.04394C3.86477 5.69566 4.32261 5.5 4.8 5.5H19.2C19.6774 5.5 20.1352 5.69566 20.4728 6.04394C20.8104 6.39223 21 6.8646 21 7.35714V16.6429C21 17.1354 20.8104 17.6078 20.4728 17.9561C20.1352 18.3043 19.6774 18.5 19.2 18.5H4.8C4.32261 18.5 3.86477 18.3043 3.52721 17.9561C3.18964 17.6078 3 17.1354 3 16.6429V7.35714Z"
                                stroke="#848993" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path
                                d="M3 9.5C3.95478 9.5 4.87045 9.07857 5.54558 8.32843C6.22072 7.57828 6.6 6.56087 6.6 5.5"
                                stroke="#848993" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path
                                d="M20.6016 9.5C19.6468 9.5 18.7311 9.07857 18.056 8.32843C17.3808 7.57828 17.0016 6.56087 17.0016 5.5"
                                stroke="#848993" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path
                                d="M17.3984 18.5C17.3984 17.4391 17.7777 16.4217 18.4529 15.6716C19.128 14.9214 20.0437 14.5 20.9984 14.5"
                                stroke="#848993" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path
                                d="M7 18.5C7 17.4391 6.62071 16.4217 5.94558 15.6716C5.27045 14.9214 4.35478 14.5 3.4 14.5"
                                stroke="#848993" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <p>1 240 000 ₽/мес</p></div>
                    <a class="number">+7 (999) 312 07 14</a>
                    <div class="product_single_cont_box_top_text_flex"><a>Позвонить</a><a>Связаться в чате</a></div>
                </div>
            </div>
            <div class="product_single_cont_box_bottom"><p>Продаётся номер «под ключ» в действующем камерном бутик-отеле
                    Vertical под международным брендом Best Western. Бутик- Отель расположен в исторической части
                    Москвы, в пешей доступности: 400 метров от метро Таганская. Только в июле: стоимость номера 7 250
                    000 рублей. Доходность объекта: 13 % годовых. Продаётся номер «под ключ» в действующем камерном
                    бутик-отеле Vertical под международным брендом Best Western. Бутик- Отель расположен в исторической
                    части Москвы, в пешей доступности: 400 метров от метро Таганская. Только в июле: стоимость номера 7
                    250 000 рублей. Доходность объекта: 13 % годовых.</p>
                <p>Тот самый случай, когда вы становитесь собственником недвижимости в центре Москвы, а квадратные метры
                    приносят доход без вашего участия! Обращайтесь!</p></div>
        </div>
        <div class="product_single_cont_info">
            <div class="product_single_cont_info_title">
                <div class="product_single_cont_info_title_flex"><img src="img/infotitle.svg"><h4>Общая информация и
                        контакты</h4></div>
            </div>
            <div class="product_single_cont_info_flex">
                <div class="product_single_cont_info_flex_box">
                    <div class="product_single_cont_info_flex_item"><h4>Дата основания компании</h4>
                        <p>26.07.2021</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Активность бизнеса</h4>
                        <p>Действующий</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Причина продажи</h4>
                        <p>Переезд</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Ссылка на видео</h4><a>https://www.loom.com/share/f30b110dd83c4bda88ac62e87c7acc68</a>
                    </div>
                    <div class="product_single_cont_info_flex_item"><h4>Адрес</h4><a>Анапа, Краснодарский край</a></div>
                    <div class="product_single_cont_info_flex_item"><h4>Сайт</h4><a>https://www.loom.com</a></div>
                    <div class="product_single_cont_info_flex_item"><h4>Соц.сети</h4><a>https://www.instagram/sportmaster1234.com</a><a>https://www.instagram/sportmaster1234.com</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_single_cont_info">
            <div class="product_single_cont_info_title">
                <div class="product_single_cont_info_title_flex"><img src="img/infotitle1.svg"><h4>Сведения о
                        деятельности</h4></div>
            </div>
            <div class="product_single_cont_info_flex">
                <div class="product_single_cont_info_flex_box">
                    <div class="product_single_cont_info_flex_item"><h4>Сфера деятельности</h4>
                        <p>Торговля</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Организационно-правовая форма</h4>
                        <p>ООО</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Другие направления деятельности компании</h4>
                        <p>Логистика, консалтинг</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Оказываемые услуги</h4>
                        <p>Спортивный массаж, консалтинг, логистика</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Продаваемый товар</h4>
                        <p>Спорттовары</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Количество работников</h4>
                        <p>4</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Количество руководящего персонала</h4>
                        <p>1</p></div>
                </div>
            </div>
        </div>
        <div class="product_single_cont_info">
            <div class="product_single_cont_info_title">
                <div class="product_single_cont_info_title_flex"><img src="img/infotitle2.svg"><h4>Сведения об
                        объекте</h4></div>
            </div>
            <div class="product_single_cont_info_flex">
                <div class="product_single_cont_info_flex_box">
                    <div class="product_single_cont_info_flex_item"><h4>Объект, в котором осуществляется
                            деятельность</h4>
                        <p>Торговый центр</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Назначение объекта недвижимости, где
                            осуществляется деятельность</h4>
                        <p>Нежилое</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Форма собственности</h4>
                        <p>Аренда</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Этаж</h4>
                        <p>4</p></div>
                </div>
            </div>
        </div>
        <div class="product_single_cont_info">
            <div class="product_single_cont_info_title">
                <div class="product_single_cont_info_title_flex"><img src="img/infotitle3.svg"><h4>Сведения об
                        имуществе</h4></div>
            </div>
            <div class="product_single_cont_info_flex">
                <div class="product_single_cont_info_flex_box">
                    <div class="product_single_cont_info_flex_item"><h4>Объекты недвижимости, которые входят в
                            продажу</h4>
                        <p>Складское помещение</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Оборудование, продаваемое при продаже</h4>
                        <p>Касса, стеллажи, вешалки</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Общая площадь, м2</h4>
                        <p>120</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Наличие лицензий, сертификатов</h4>
                        <p>Лицензия СРО №765/3-11 до 15.08.2022, </p>
                        <p>Сертификат соответсвия №CN13-765/3-11 до 15.08.2022</p></div>
                </div>
            </div>
        </div>
        <div class="product_single_cont_info">
            <div class="product_single_cont_info_title">
                <div class="product_single_cont_info_title_flex"><img src="img/infotitle4.svg"><h4>Финансовая
                        информация</h4></div>
            </div>
            <div class="product_single_cont_info_flex">
                <div class="product_single_cont_info_flex_box">
                    <div class="product_single_cont_info_flex_item fullwidth"><h4>Система налогооблажения</h4>
                        <p>ЕСХН</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Средний чек</h4>
                        <p>2400 ₽</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Средний оборот в месяц</h4>
                        <p>1 240 000 ₽</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Фонд заработной платы</h4>
                        <p>120 000 ₽</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Аренда</h4>
                        <p>80 000 ₽</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Стоимость коммунальных платежей</h4>
                        <p>24 000 ₽</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Общие расходы</h4>
                        <p>224 000 ₽</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Чистая прибыль в месяц</h4>
                        <p>1 016 000 ₽</p></div>
                    <div class="product_single_cont_info_flex_item"><h4>Срок окупаемости бизнеса</h4>
                        <p>Нет</p></div>
                    <div class="product_single_cont_info_flex_item fullwidth"><h4>Наличие задолженностей и штрафов</h4>
                        <p>800 000 ₽ (штраф от санэпидемстанции за грызунов на складе), 24 000 ₽ (долг арендодателю за
                            2020 год), 800 000 ₽ (штраф от санэпидемстанции за грызунов на складе)</p></div>
                </div>
            </div>
        </div>
        <a class="editbtn">Редактировать</a></div>
</div>
@include('include.footer')
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script type="text/javascript" src="js/index.js"></script>
</body>
</html>
