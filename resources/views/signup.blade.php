@include('include.header')
<link rel="stylesheet" href="{{ asset('/website/css/aplication_form.css' )}}">

<main>
   <section class="log_in">
      <form action="{{ route('signup') }}" method="post" class="log_in_wrapper">
         <h1 class="log_in_title">Регистрация</h1>
         <div class="log_in_inputs_wrapper">
         <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
         @if ($errors->has('name'))
         <span class="error_text">{{$errors->first('name')}}</span>
         <div class="log_in_input_wrapper error_text_input">
         @else
         <div class="log_in_input_wrapper">
            @endif
            <input type="text" placeholder="Имя" name="name" class="log_in_input"
            @if(session()->has('aft_r')) value="{{session()->get('aft_r')['name']}}"
            @else value="{{old('name')}}" @endif>
         </div>
         {{--
         <div class="tooltip">
            ?--}}
            {{--
            <div class="tooltiptext">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
            --}}
            {{--
         </div>
         --}}
         @if ($errors->has('surname'))
         <span class="error_text">{{$errors->first('surname')}}</span>
         <div class="log_in_input_wrapper error_text_input">
         @else
         <div class="log_in_input_wrapper">
            @endif
            <input type="text" placeholder="Фамилия" name="surname" class="log_in_input"
            @if(session()->has('aft_r')) value="{{session()->get('aft_r')['surname']}}"
            @else value="{{old('surname')}}" @endif >
         </div>
         @if ($errors->has('phone'))
         <span class="error_text">{{$errors->first('phone')}}</span>
         <div class="log_in_input_wrapper error_text_input">
         @else
         <div class="log_in_input_wrapper">
            @endif
            <input id='yourphone' type="text" placeholder="телефон"
            name="phone" class="log_in_input"
            @if(session()->has('aft_r')) value="{{session()->get('aft_r')['phone']}}"
            @else value="{{old('phone')}}" @endif >
         </div>
         @if(session()->has('error_email'))
         <span class="error_text">{{ session()->get('error_email') }}</span>
         @endif
         @if ($errors->has('email'))
         <span class="error_text">{{$errors->first('email')}}</span>
         <div class="log_in_input_wrapper error_text_input">
         @else
         <div class="log_in_input_wrapper">
            @endif
            <input type="text" placeholder="e-mail" name="email"
            class="log_in_input"
            @if(session()->has('aft_r')) value="{{session()->get('aft_r')['email']}}"
            @else value="{{old('email')}}" @endif >

               <!-- <img src="{{ asset('images/glaz.png') }}" alt="" class="glaz show"> -->

         </div>
         @if ($errors->has('password'))
         <span
            class="error_text">{{$errors->first('password')}}</span>
         <div class="log_in_input_wrapper error_text_input">

             @else
            <div class="log_in_input_wrapper">
            @endif
                <div>
            <input type="password"
               placeholder="Пароль"
               name="password"
               class="log_in_input p_input" id="password_input">

               <img src="{{ asset('images/glaz.png') }}" alt="" class="glaz show">
                    <div
                        class="aplication_form_hint_wrapper hint" style="right: -48px;top: 9px;display: none">
                        <div
                            class="aplication_form_hint_svg" style="margin-right: 0">
                            <svg width="16" height="16"
                                 viewBox="0 0 16 16"
                                 fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <circle cx="8" cy="8"
                                        r="7.5"
                                        stroke="#E5D6A0"/>
                                <path
                                    d="M7.25177 9.93452H8.2745V9.88339C8.29155 8.82657 8.56427 8.36634 9.31427 7.89759C10.0643 7.44163 10.5075 6.78538 10.5075 5.84362C10.5075 4.51407 9.53587 3.54248 8.03587 3.54248C6.65518 3.54248 5.56001 4.39475 5.49609 5.84362H6.56996C6.63388 4.83793 7.337 4.42884 8.03587 4.42884C8.837 4.42884 9.48473 4.95725 9.48473 5.79248C9.48473 6.47004 9.09694 6.95583 8.59837 7.25839C7.76314 7.76549 7.26456 8.25981 7.25177 9.88339V9.93452ZM7.79723 12.4573C8.2191 12.4573 8.56427 12.1121 8.56427 11.6902C8.56427 11.2683 8.2191 10.9232 7.79723 10.9232C7.37535 10.9232 7.03018 11.2683 7.03018 11.6902C7.03018 12.1121 7.37535 12.4573 7.79723 12.4573Z"
                                    fill="#E5D6A0"/>
                            </svg>
                        </div>
                        <div
                            class="aplication_form_hint_box_wrapper" style="top: -3px">
                            <p class="aplication_form_hint_text">
                                Пароль должен состоять из латинских символов, содержать в себе не буквы заглавного и прописного регистра и не менее 1 цифры
                            </p>
                        </div>
                    </div>
                </div>
         </div>
         @if ($errors->has('password_confirmation'))
         <span
            class="error_text">{{$errors->first('password_confirmation')}}</span>
         <div class="log_in_input_wrapper error_text_input">
            @else
            <div class="log_in_input_wrapper">
               @endif
               <input type="password"
                  placeholder="Повторите пароль"
                  name="password_confirmation"
                  class="log_in_input p_input" id="confirm_password">
               <img src="{{ asset('images/glaz.png') }}" alt="" class="glaz show">
            </div>

         </div>
         @if ($errors->has('policy'))
         <span
            class="error_text">{{$errors->first('policy')}}</span>
         @endif
         <div
            class="sign_up_check_input_wrapper">
            <input type="checkbox"
               class="sign_up_check_input"
               hidden id="sign_up_input"
               name="policy">
            <label for="sign_up_input"
               class="sign_up_input_label"></label>
            <span
               class="sign_up_input_text"> <span>Я принимаю</span> <a
               href="#"
               class="open_terms_of_use">условия использования и пользовательское соглашение</a>  </span>
         </div>
         <button class="sign_up_btn2 sign_up_btn2_unique" type="submit" > Далее </button>
      </form>
   </section>
</main>


@include('include.footer')

<script type="text/javascript">
   $(document).ready(function(){

            $('input').attr('autocomplete','off')

           $(".sign_up_btn2 ").css({'opacity': '0.2'})
           $(".sign_up_btn2 ").attr('disabled',true)



      // $('#yourphone').inputmask("+7(999) 999-9999");

      $('#password_input').focus(function (){
          $('.hint').css({
                      'display': 'flex',
          })
          $('#password_input').css( 'outline', 'none');
      })
       $('#password_input').on('keyup', function () {

           let password_input = $(this).val();
           let reg = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$');
           if (reg.test(password_input)) {
               $('#password_input ').css('border','1px solid #e5d6a0')

           } else if (password_input == '')
           {
               $('#password_input ').css('border','1px solid #e5d6a0')

           } else {
               $('#password_input ').css('border','1px solid #c34d4d')
           }
       })


       $('#confirm_password').on('keyup',function (){
           let pass = $('#password_input').val()
           let conf_pass = $(this).val()
            if(conf_pass == pass){
                $(".sign_up_btn2 ").css({'opacity': 'unset'})
                $(".sign_up_btn2 ").attr('disabled',false)
           }

       })





      // $(document).on('input','.p_input', function(){

      //    var password         = $('#password_input').val();
      //    var confirm_password = $('#confirm_password').val();

      //    console.log(validate(password), validate(confirm_password),password,confirm_password)

      //    if (validate(password) || validate(confirm_password) ) {

      //       // password == confirm_password

      //       console.log('super')

      //       if (validate(password)) {
      //          $('#password_input').parent().removeClass('error_text_input')
      //          $('#password_input').parent().find('.error_text').remove()

      //       }

      //       if ( validate(confirm_password) ) {


      //          $('#confirm_password').parent().removeClass('error_text_input')
      //          $('#confirm_password').parent().find('.error_text').remove()


      //       }







      //    }  else if(validate(password) && validate(confirm_password)) {


      //       if (validate(password)) {
      //          $('#password_input').parent().removeClass('error_text_input')
      //          $('#password_input').parent().find('.error_text').remove()

      //       }

      //       if ( validate(confirm_password) ) {


      //          $('#confirm_password').parent().removeClass('error_text_input')
      //          $('#confirm_password').parent().find('.error_text').remove()


      //       }



      //       $('.sign_up_btn2_unique').attr('type', 'submit')
      //       $('.sign_up_btn2_unique').removeClass('disabled_button');


      //    } else {

      //       if (!validate(password) && password.length > 0) {
      //          $('#password_input').parent().addClass('error_text_input')

      //          if (!$('#password_input').parent().find('span').hasClass('error_text')) {

      //             $('#password_input').parent().prepend(
      //                '<span class="error_text"> Минимум один верхний регистр  <br> Минимум одна строчная английская буква<br>Минимум 6 в длину</span>'
      //             )
      //          }
      //       }

      //       if (!validate(confirm_password) && confirm_password.length > 0) {
      //          $('#confirm_password').parent().addClass('error_text_input')

      //           if (!$('#confirm_password').parent().find('span').hasClass('error_text')) {

      //             $('#confirm_password').parent().prepend(
      //                '<span class="error_text"> Минимум один верхний регистр  <br> Минимум одна строчная английская буква<br>Минимум 6 в длину</span>'
      //             )
      //          }

      //       }

      //       $('.sign_up_btn2_unique').attr('type', 'button')
      //       $('.sign_up_btn2_unique').addClass('disabled_button');
      //    }

      // })



      // function validate(value)
      // {

      //   var value = value;
      //   var rgex = /^(?=.*?[A-Z])(?=.*?[0-9]).{6,}$/;
      //   var result = rgex.test(value);
      //   return result;
      //   console.log( "phone:"+result );

      // }




   })







</script>
