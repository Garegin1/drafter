@include('include.header')
<link rel="stylesheet" href="{{ asset('/website/css/my_companies.css' )}}">
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css"/>
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
<link rel="stylesheet" href="{{ asset('/website/css/add.css' )}}">
<main>
    <section class="companies">
        <div class="companies_wrapper">
            <div class="k1">
                <a href='/' class="back_link">
                    <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9 1L0.999999 9L9 17" stroke="#E5D6A0" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round"/>
                    </svg>&nbsp;&nbsp;&nbsp;
                    Назад</a>
            </div>
            @if(isset($investment))
                <form action="" class="about_investment_search_wrapper ">
                    <div class="investment_search_items_wrapper">
                        <div class="investment_search_stars_items_wrapper">
                            <div class="investment_search_item">
                                <div class="investment_search_img">
                                    @if($investment->company_logo != '')
                                        <img src="{{ asset('storage/uploads/'.$investment->company_logo) }}" alt="">
                                    @else
                                        <img src="{{ asset('website/images/avatardefault.svg') }}" alt="">
                                    @endif
                                </div>
                                <div class="bookmark_svg_info_wrapper">
                                    <div class="bookmark_svg_wrapper">
                                        @auth
                                            @if( check_bookmark($investment->id, 'investment'))

                                                <div class="small_item_svg_wrapper added_to_bookmark showBookmark"
                                                     data-id="{{ $investment->id }}" data-type="investment">
                                                    <svg width="20" height="26" viewBox="0 0 34 40" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M33 39L17 28.4444L1 39V5.22222C1 4.10242 1.48163 3.02848 2.33894 2.23666C3.19625 1.44484 4.35901 1 5.57143 1H28.4286C29.641 1 30.8038 1.44484 31.6611 2.23666C32.5184 3.02848 33 4.10242 33 5.22222V39Z"
                                                            fill="#E5D6A0" stroke="#E5D6A0" stroke-width="2"
                                                            stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                                <div class="small_item_svg_wrapper add_to_bookmarks"
                                                     data-id="{{ $investment->id }}" data-type="investment">
                                                    <svg width="20" height="26" viewBox="0 0 20 26" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M19 25L10 18.3333L1 25V3.66667C1 2.95942 1.27092 2.28115 1.75315 1.78105C2.23539 1.28095 2.88944 1 3.57143 1H16.4286C17.1106 1 17.7646 1.28095 18.2468 1.78105C18.7291 2.28115 19 2.95942 19 3.66667V25Z"
                                                            stroke="#E5D6A0" stroke-width="2" stroke-linecap="round"
                                                            stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            @else
                                                <div class="small_item_svg_wrapper add_to_bookmarks showBookmark"
                                                     data-id="{{ $investment->id }}" data-type="investment">
                                                    <svg width="20" height="26" viewBox="0 0 20 26" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M19 25L10 18.3333L1 25V3.66667C1 2.95942 1.27092 2.28115 1.75315 1.78105C2.23539 1.28095 2.88944 1 3.57143 1H16.4286C17.1106 1 17.7646 1.28095 18.2468 1.78105C18.7291 2.28115 19 2.95942 19 3.66667V25Z"
                                                            stroke="#E5D6A0" stroke-width="2" stroke-linecap="round"
                                                            stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                                <div class="small_item_svg_wrapper added_to_bookmark"
                                                     data-id="{{ $investment->id }}" data-type="investment">
                                                    <svg width="20" height="26" viewBox="0 0 34 40" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M33 39L17 28.4444L1 39V5.22222C1 4.10242 1.48163 3.02848 2.33894 2.23666C3.19625 1.44484 4.35901 1 5.57143 1H28.4286C29.641 1 30.8038 1.44484 31.6611 2.23666C32.5184 3.02848 33 4.10242 33 5.22222V39Z"
                                                            fill="#E5D6A0" stroke="#E5D6A0" stroke-width="2"
                                                            stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                            @endif
                                        @endauth
                                        @guest
                                            <div class="small_item_svg_wrapper add_to_bookmarks"
                                                 data-id="{{ $investment->id }}" data-type="investment">
                                                <svg width="20" height="26" viewBox="0 0 34 40" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M33 39L17 28.4444L1 39V5.22222C1 4.10242 1.48163 3.02848 2.33894 2.23666C3.19625 1.44484 4.35901 1 5.57143 1H28.4286C29.641 1 30.8038 1.44484 31.6611 2.23666C32.5184 3.02848 33 4.10242 33 5.22222V39Z"
                                                        stroke="#E5D6A0" stroke-width="2" stroke-linecap="round"
                                                        stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                        @endguest


                                    </div>
                                    <p class="bookmark_info">Добавить в избранное</p>

                                </div>
                            </div>
                            <div class="investment_search_item  info_second_item">
                                <div class="search_title_stars_wrapper">
                                    <h2 class="search_title">{{ $investment->company_name }}</h2>
                                    <div class="star_svg_wrapper first_star_wrapper">
                                        <div class="svg_wrapper">
                                            <svg width="19" height="20" viewBox="0 0 19 20" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M1 7.53424V1.78411C1 1.57615 1.08261 1.37671 1.22966 1.22966C1.37671 1.08261 1.57615 1 1.78411 1H17.4663C17.6742 1 17.8737 1.08261 18.0207 1.22966C18.1678 1.37671 18.2504 1.57615 18.2504 1.78411V7.53424C18.2504 15.7689 11.2614 18.4971 9.86593 18.9597C9.70993 19.0134 9.54045 19.0134 9.38445 18.9597C7.98895 18.4971 1 15.7689 1 7.53424Z"
                                                    stroke="#848993" stroke-width="1.5" stroke-linecap="round"
                                                    stroke-linejoin="round"/>
                                                <path
                                                    d="M8.88194 11.6156H10.5801V11.4529C10.5891 10.5197 10.9027 10.0842 11.6107 9.62957C12.4486 9.09838 12.9952 8.39491 12.9952 7.27032C12.9952 5.59538 11.7317 4.61914 9.95283 4.61914C8.32633 4.61914 7.00004 5.52839 6.95972 7.44259H8.78785C8.81472 6.66256 9.3569 6.24621 9.94387 6.24621C10.5488 6.24621 11.0372 6.67692 11.0372 7.3421C11.0372 7.969 10.6115 8.38534 10.0604 8.75861C9.30761 9.26588 8.88642 9.77792 8.88194 11.4529V11.6156ZM9.76464 14.6783C10.3382 14.6783 10.8355 14.1662 10.84 13.5298C10.8355 12.9029 10.3382 12.3908 9.76464 12.3908C9.17318 12.3908 8.68479 12.9029 8.68927 13.5298C8.68479 14.1662 9.17318 14.6783 9.76464 14.6783Z"
                                                    fill="#848993"/>
                                                <path
                                                    d="M8.87965 12.6719C8.67698 12.8656 8.57564 13.1515 8.57564 13.5299C8.57564 13.8947 8.67923 14.1784 8.8864 14.3811C9.09358 14.5792 9.38632 14.6783 9.76464 14.6783C10.134 14.6783 10.4222 14.577 10.6294 14.3743C10.8365 14.1672 10.9401 13.8857 10.9401 13.5299C10.9401 13.1606 10.8365 12.8768 10.6294 12.6787C10.4267 12.4805 10.1385 12.3814 9.76464 12.3814C9.37732 12.3814 9.08232 12.4782 8.87965 12.6719Z"
                                                    fill="#848993"/>
                                            </svg>
                                        </div>
                                        <div class="jq-ry-group-wrapper" style="width: 110px">
                                            <div class="jq-ry-normal-group jq-ry-group">
                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                     viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                     width="32px" height="32px" fill="#848993"><polygon
                                                        points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                     viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                     width="32px" height="32px" fill="#848993"
                                                     style="margin-left: 0px;"><polygon
                                                        points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                     viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                     width="32px" height="32px" fill="#848993"
                                                     style="margin-left: 0px;"><polygon
                                                        points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                     viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                     width="32px" height="32px" fill="#848993"
                                                     style="margin-left: 0px;"><polygon
                                                        points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                     viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                     width="32px" height="32px" fill="#848993"
                                                     style="margin-left: 0px;"><polygon
                                                        points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                            </div>
                                            <div class="active_star" style="width:70%">
                                                <div class="jq-ry-rated-group jq-ry-group">
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                                                         viewBox="0 12.705 512 486.59" x="0px" y="0px"
                                                                                                         xml:space="preserve" width="32px" height="32px" fill="#FF8D4E"><polygon
                                                                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                         viewBox="0 12.705 512 486.59" x="0px" y="0px"
                                                         xml:space="preserve" width="32px" height="32px" fill="#FF8D4E"
                                                         style="margin-left: 0px;"><polygon
                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                         viewBox="0 12.705 512 486.59" x="0px" y="0px"
                                                         xml:space="preserve" width="32px" height="32px" fill="#FF8D4E"
                                                         style="margin-left: 0px;"><polygon
                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                         viewBox="0 12.705 512 486.59" x="0px" y="0px"
                                                         xml:space="preserve" width="32px" height="32px" fill="#FF8D4E"
                                                         style="margin-left: 0px;"><polygon
                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                         viewBox="0 12.705 512 486.59" x="0px" y="0px"
                                                         xml:space="preserve" width="32px" height="32px" fill="#FF8D4E"
                                                         style="margin-left: 0px;"><polygon
                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <p class="investment_search_info1">Ставка:</p>
                                <p class="investment_search_info2">{{ $investment->percentage }}%</p>
                                <div class="investment_search_info_svg_wrapper special_svg_invest_wrapper">
                                    <div class="info">
                                        <p class="investment_search_info5">Всего инвестиций</p>
                                        <p class="investment_search_info3">
                                            {{ number_format($investment->am_collected, 0, ',', ' ')   }}
                                            из {{ number_format($investment->am_required, 0, ',', ' ')   }} &#8381;</p>
                                    </div>

                                    <div class="investment_search_info4_svg_wrapper">
                                        <div class="investment_search_info4_svg">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M24 23.9999V20.9496C24 19.3316 23.3679 17.7798 22.2426 16.6357C21.1174 15.4916 19.5913 14.8489 18 14.8489H6C4.4087 14.8489 2.88258 15.4916 1.75736 16.6357C0.632141 17.7798 0 19.3316 0 20.9496V23.9999"
                                                    fill="#E5D6A0"/>
                                                <path
                                                    d="M12.0007 12.2014C15.3144 12.2014 18.0007 9.47004 18.0007 6.10071C18.0007 2.73138 15.3144 0 12.0007 0C8.68702 0 6.00073 2.73138 6.00073 6.10071C6.00073 9.47004 8.68702 12.2014 12.0007 12.2014Z"
                                                    fill="#E5D6A0"/>
                                            </svg>

                                        </div>
                                        <p class="investment_search_info4">{{ count($investment->invests) }}</p>
                                    </div>

                                </div>
                                <div class="investment_search_loading_wrapper">
                                    <div class="investment_search_loading_child"
                                         style="width: {{ $investment->am_collected/$investment->am_required*100 }}%"></div>
                                </div>
                            </div>
                        </div>
                        <div class="star_svg_wrapper hidden_star_wrapper">
                            <div class="svg_wrapper">
                                <svg width="19" height="20" viewBox="0 0 19 20" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M1 7.53424V1.78411C1 1.57615 1.08261 1.37671 1.22966 1.22966C1.37671 1.08261 1.57615 1 1.78411 1H17.4663C17.6742 1 17.8737 1.08261 18.0207 1.22966C18.1678 1.37671 18.2504 1.57615 18.2504 1.78411V7.53424C18.2504 15.7689 11.2614 18.4971 9.86593 18.9597C9.70993 19.0134 9.54045 19.0134 9.38445 18.9597C7.98895 18.4971 1 15.7689 1 7.53424Z"
                                        stroke="#848993" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                    <path
                                        d="M8.88194 11.6156H10.5801V11.4529C10.5891 10.5197 10.9027 10.0842 11.6107 9.62957C12.4486 9.09838 12.9952 8.39491 12.9952 7.27032C12.9952 5.59538 11.7317 4.61914 9.95283 4.61914C8.32633 4.61914 7.00004 5.52839 6.95972 7.44259H8.78785C8.81472 6.66256 9.3569 6.24621 9.94387 6.24621C10.5488 6.24621 11.0372 6.67692 11.0372 7.3421C11.0372 7.969 10.6115 8.38534 10.0604 8.75861C9.30761 9.26588 8.88642 9.77792 8.88194 11.4529V11.6156ZM9.76464 14.6783C10.3382 14.6783 10.8355 14.1662 10.84 13.5298C10.8355 12.9029 10.3382 12.3908 9.76464 12.3908C9.17318 12.3908 8.68479 12.9029 8.68927 13.5298C8.68479 14.1662 9.17318 14.6783 9.76464 14.6783Z"
                                        fill="#848993"/>
                                    <path
                                        d="M8.87965 12.6719C8.67698 12.8656 8.57564 13.1515 8.57564 13.5299C8.57564 13.8947 8.67923 14.1784 8.8864 14.3811C9.09358 14.5792 9.38632 14.6783 9.76464 14.6783C10.134 14.6783 10.4222 14.577 10.6294 14.3743C10.8365 14.1672 10.9401 13.8857 10.9401 13.5299C10.9401 13.1606 10.8365 12.8768 10.6294 12.6787C10.4267 12.4805 10.1385 12.3814 9.76464 12.3814C9.37732 12.3814 9.08232 12.4782 8.87965 12.6719Z"
                                        fill="#848993"/>
                                </svg>
                            </div>
                            <div class="jq-ry-group-wrapper">
                                <div class="jq-ry-normal-group jq-ry-group">
                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59"
                                         x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#848993"><polygon
                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59"
                                         x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#848993"
                                         style="margin-left: 0px;"><polygon
                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59"
                                         x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#848993"
                                         style="margin-left: 0px;"><polygon
                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59"
                                         x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#848993"
                                         style="margin-left: 0px;"><polygon
                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59"
                                         x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#848993"
                                         style="margin-left: 0px;"><polygon
                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                </div>
                                <div class="active_star" style="width:70%">
                                    <div class="jq-ry-rated-group jq-ry-group">
                                        <!--?xml version="1.0" encoding="utf-8"?-->
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                             width="32px" height="32px" fill="#FF8D4E"><polygon
                                                points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                        <!--?xml version="1.0" encoding="utf-8"?-->
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                             width="32px" height="32px" fill="#FF8D4E" style="margin-left: 0px;"><polygon
                                                points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                        <!--?xml version="1.0" encoding="utf-8"?-->
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                             width="32px" height="32px" fill="#FF8D4E" style="margin-left: 0px;"><polygon
                                                points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                        <!--?xml version="1.0" encoding="utf-8"?-->
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                             width="32px" height="32px" fill="#FF8D4E" style="margin-left: 0px;"><polygon
                                                points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                        <!--?xml version="1.0" encoding="utf-8"?-->
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                             width="32px" height="32px" fill="#FF8D4E" style="margin-left: 0px;"><polygon
                                                points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="about_project_progress_items_wrapper special_secondly_progress_projects">
                        <div class="about_project_progress_item_titles_wrapper">
                            <p class="about_project_progress_item_main_title active" id="special_title1"
                               data-id="open_div1">О проекте</p>
                            <p class="about_project_progress_item_main_title" id="special_title2" data-id="open_div2">
                                Ход работы</p>
                        </div>
                        <div class="about_project_progress_item open" id="open_div1">
                            @if($add_images->count() > 0)

                                <h2 class="about_project_progress_item_title">Фото</h2>
                                <div class="swiper_btn_slides_wrapper swiper_slider_special_second">

                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-container" id="first_swiper_container">
                                        <div class="swiper-wrapper">

                                            @foreach($add_images as $image)
                                                <div class="swiper-slide">
                                                    <div class="slider_slide_img">
                                                        <img
                                                            src="{{ asset('/storage/investment_gallery\\').$image->name }}"
                                                            alt="">
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                </div>
                            @endif
                            <div class="description_wrapper">
                                <p class="description_title">Описание</p>
                                <div class="small_item_show_more_text_wrapper">
                                    <p class="small_item_show_more_text description_info">
                                        {{ $investment->description }}
                                                                        <span class="small_item_show_more_text2">
                                                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                                                                   </span>
                                    </p>
                                    <button class="read_more_btn" type="button">Читать полностью</button>
                                </div>
                            </div>
                            <div class="form_btns_wrapper">
                                @auth()
                                    @if($investment->user_id != auth()->user()->id)

                                        <button class="form_btn open_invest_modal" type="button">Инвестировать</button>
                                    @else
                                        <div class="form_btns_wrapper">
                                            <button class="form_btn form_first_btn" type="button">Редактировать</button>
                                            <a href="{{ route('investors.show',$investment->id) }}" class="form_btn">Инвесторы</a>
                                        </div>

                                    @endif

                                @endauth
                                @guest()
                                    <div class="form_btns_wrapper">
                                        <button class="form_btn form_first_btn" type="button"><a
                                                href="{{ route('login2') }}"> зарегистрироваться </a></button>
                                    </div>
                                @endguest
                            </div>

                        </div>
                        <div class="about_project_progress_item" id="open_div2">
                            <div class="first_project_swiper">

                                <div class="swiper-container" id="second_swiper_container">
                                    <h2 class="about_project_progress_item_title">12.03.2021</h2>
                                    <div class="swiper-wrapper">
                                        @if($add_images)
                                            @foreach($add_images as $image)
                                                <div class="swiper-slide">
                                                    <div class="slider_slide_img">
                                                        <img
                                                            src="{{ asset('/storage/investment_gallery\\').$image->name }}"
                                                            alt="">
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>

                                </div>
                                <!-- If we need navigation buttons -->
                                <div class="swiper-button-prev swiper_button_prev_second"></div>
                                <div class="swiper-button-next swiper_button_next_second"></div>
                                <div class="small_item_show_more_text_wrapper">
                                    <p class="small_item_show_more_text description_info">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat<span class="dots">...</span>
                                        <span class="small_item_show_more_text2">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                           </span>
                                    </p>
                                    <button class="read_more_btn" type="button">Читать полностью</button>
                                </div>
                            </div>
                            <div class="second_project_swiper">
                                <div class="slider_bts_img_wrapper">
                                    <div class="swiper-button-prev swiper_button_prev_third"></div>
                                    <div class="swiper-container" id="third_swiper_container">
                                        <h2 class="about_project_progress_item_title">12.03.2021</h2>
                                        <div class="swiper-wrapper">
                                            @if($add_images)
                                                @foreach($add_images as $image)
                                                    <div class="swiper-slide">
                                                        <div class="slider_slide_img">
                                                            <img
                                                                src="{{ asset('/storage/investment_gallery\\').$image->name }}"
                                                                alt="">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>

                                    <div class="swiper-button-next swiper_button_next_third"></div>
                                </div>
                                <div class="small_item_show_more_text_wrapper">
                                    <p class="small_item_show_more_text description_info">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat<span class="dots">...</span>
                                        <span class="small_item_show_more_text2">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                       </span>
                                    </p>
                                    <button class="read_more_btn " type="button">Читать полностью</button>
                                </div>
                                @auth()
                                    <div class="form_btns_wrapper">
                                        <button class="form_btn  form_first_btn">

                                            Связаться
                                        </button>
                                        <button class="form_btn open_invest_modal" type="button">

                                            Инвестировать
                                        </button>
                                    </div>
                                @endauth
                            </div>


                        </div>

                    </div>


                </form>

            @elseif(isset($business))

                                <form action="" class="about_investment_search_wrapper ">
                                    <div class="investment_search_items_wrapper">
                                        <div class="investment_search_stars_items_wrapper">
                                            <div class="investment_search_item">
                                                <div class="investment_search_img">
                                                    @if($business->company_logo != '')
                                                        <img src="{{ asset('storage/uploads/'.$business->company_logo) }}" alt="">
                                                    @else
                                                        <img src="{{ asset('website/images/avatardefault.svg') }}" alt="">
                                                    @endif
                                                </div>

                                            </div>
                                            <div class="investment_search_item  info_second_item">
                                                <div class="search_title_stars_wrapper">
                                                    <h2 class="search_title">{{ $business->company_name }}</h2>
                                                    <div class="star_svg_wrapper first_star_wrapper">
                                                        <div class="svg_wrapper">
                                                            <svg width="19" height="20" viewBox="0 0 19 20" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M1 7.53424V1.78411C1 1.57615 1.08261 1.37671 1.22966 1.22966C1.37671 1.08261 1.57615 1 1.78411 1H17.4663C17.6742 1 17.8737 1.08261 18.0207 1.22966C18.1678 1.37671 18.2504 1.57615 18.2504 1.78411V7.53424C18.2504 15.7689 11.2614 18.4971 9.86593 18.9597C9.70993 19.0134 9.54045 19.0134 9.38445 18.9597C7.98895 18.4971 1 15.7689 1 7.53424Z"
                                                                    stroke="#848993" stroke-width="1.5" stroke-linecap="round"
                                                                    stroke-linejoin="round"/>
                                                                <path
                                                                    d="M8.88194 11.6156H10.5801V11.4529C10.5891 10.5197 10.9027 10.0842 11.6107 9.62957C12.4486 9.09838 12.9952 8.39491 12.9952 7.27032C12.9952 5.59538 11.7317 4.61914 9.95283 4.61914C8.32633 4.61914 7.00004 5.52839 6.95972 7.44259H8.78785C8.81472 6.66256 9.3569 6.24621 9.94387 6.24621C10.5488 6.24621 11.0372 6.67692 11.0372 7.3421C11.0372 7.969 10.6115 8.38534 10.0604 8.75861C9.30761 9.26588 8.88642 9.77792 8.88194 11.4529V11.6156ZM9.76464 14.6783C10.3382 14.6783 10.8355 14.1662 10.84 13.5298C10.8355 12.9029 10.3382 12.3908 9.76464 12.3908C9.17318 12.3908 8.68479 12.9029 8.68927 13.5298C8.68479 14.1662 9.17318 14.6783 9.76464 14.6783Z"
                                                                    fill="#848993"/>
                                                                <path
                                                                    d="M8.87965 12.6719C8.67698 12.8656 8.57564 13.1515 8.57564 13.5299C8.57564 13.8947 8.67923 14.1784 8.8864 14.3811C9.09358 14.5792 9.38632 14.6783 9.76464 14.6783C10.134 14.6783 10.4222 14.577 10.6294 14.3743C10.8365 14.1672 10.9401 13.8857 10.9401 13.5299C10.9401 13.1606 10.8365 12.8768 10.6294 12.6787C10.4267 12.4805 10.1385 12.3814 9.76464 12.3814C9.37732 12.3814 9.08232 12.4782 8.87965 12.6719Z"
                                                                    fill="#848993"/>
                                                            </svg>
                                                        </div>
                                                        <div class="jq-ry-group-wrapper">
                                                            <div class="jq-ry-normal-group jq-ry-group">
                                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                                     width="32px" height="32px" fill="#848993"><polygon
                                                                        points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                                     width="32px" height="32px" fill="#848993"
                                                                     style="margin-left: 0px;"><polygon
                                                                        points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                                     width="32px" height="32px" fill="#848993"
                                                                     style="margin-left: 0px;"><polygon
                                                                        points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                                     width="32px" height="32px" fill="#848993"
                                                                     style="margin-left: 0px;"><polygon
                                                                        points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                                <!--?xml version="1.0" encoding="utf-8"?-->
                                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                                     width="32px" height="32px" fill="#848993"
                                                                     style="margin-left: 0px;"><polygon
                                                                        points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                            </div>
                                                            <div class="active_star" style="width:70%">
                                                                <div class="jq-ry-rated-group jq-ry-group">
                                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                         viewBox="0 12.705 512 486.59" x="0px" y="0px"
                                                                         xml:space="preserve" width="32px" height="32px" fill="#FF8D4E"><polygon
                                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                         viewBox="0 12.705 512 486.59" x="0px" y="0px"
                                                                         xml:space="preserve" width="32px" height="32px" fill="#FF8D4E"
                                                                         style="margin-left: 0px;"><polygon
                                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                         viewBox="0 12.705 512 486.59" x="0px" y="0px"
                                                                         xml:space="preserve" width="32px" height="32px" fill="#FF8D4E"
                                                                         style="margin-left: 0px;"><polygon
                                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                         viewBox="0 12.705 512 486.59" x="0px" y="0px"
                                                                         xml:space="preserve" width="32px" height="32px" fill="#FF8D4E"
                                                                         style="margin-left: 0px;"><polygon
                                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                         viewBox="0 12.705 512 486.59" x="0px" y="0px"
                                                                         xml:space="preserve" width="32px" height="32px" fill="#FF8D4E"
                                                                         style="margin-left: 0px;"><polygon
                                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="bookmark_svg_info_wrapper">
                                                    <div class="bookmark_svg_wrapper">

                                                        @auth
                                                            @if( check_bookmark($business->id, 'investment'))

                                                                <div class="small_item_svg_wrapper added_to_bookmark showBookmark"
                                                                     data-id="{{ $business->id }}" data-type="business">
                                                                    <svg width="20" height="26" viewBox="0 0 34 40" fill="none"
                                                                         xmlns="http://www.w3.org/2000/svg">
                                                                        <path
                                                                            d="M33 39L17 28.4444L1 39V5.22222C1 4.10242 1.48163 3.02848 2.33894 2.23666C3.19625 1.44484 4.35901 1 5.57143 1H28.4286C29.641 1 30.8038 1.44484 31.6611 2.23666C32.5184 3.02848 33 4.10242 33 5.22222V39Z"
                                                                            fill="#E5D6A0" stroke="#E5D6A0" stroke-width="2"
                                                                            stroke-linecap="round" stroke-linejoin="round"/>
                                                                    </svg>
                                                                </div>
                                                                <div class="small_item_svg_wrapper add_to_bookmarks"
                                                                     data-id="{{ $business->id }}" data-type="business">
                                                                    <svg width="20" height="26" viewBox="0 0 20 26" fill="none"
                                                                         xmlns="http://www.w3.org/2000/svg">
                                                                        <path
                                                                            d="M19 25L10 18.3333L1 25V3.66667C1 2.95942 1.27092 2.28115 1.75315 1.78105C2.23539 1.28095 2.88944 1 3.57143 1H16.4286C17.1106 1 17.7646 1.28095 18.2468 1.78105C18.7291 2.28115 19 2.95942 19 3.66667V25Z"
                                                                            stroke="#E5D6A0" stroke-width="2" stroke-linecap="round"
                                                                            stroke-linejoin="round"/>
                                                                    </svg>
                                                                </div>
                                                            @else
                                                                <div class="small_item_svg_wrapper add_to_bookmarks showBookmark"
                                                                     data-id="{{ $business->id }}" data-type="business">
                                                                    <svg width="20" height="26" viewBox="0 0 20 26" fill="none"
                                                                         xmlns="http://www.w3.org/2000/svg">
                                                                        <path
                                                                            d="M19 25L10 18.3333L1 25V3.66667C1 2.95942 1.27092 2.28115 1.75315 1.78105C2.23539 1.28095 2.88944 1 3.57143 1H16.4286C17.1106 1 17.7646 1.28095 18.2468 1.78105C18.7291 2.28115 19 2.95942 19 3.66667V25Z"
                                                                            stroke="#E5D6A0" stroke-width="2" stroke-linecap="round"
                                                                            stroke-linejoin="round"/>
                                                                    </svg>
                                                                </div>

                                                                <div class="small_item_svg_wrapper added_to_bookmark"
                                                                     data-id="{{ $business->id }}" data-type="business">
                                                                    <svg width="20" height="26" viewBox="0 0 34 40" fill="none"
                                                                         xmlns="http://www.w3.org/2000/svg">
                                                                        <path
                                                                            d="M33 39L17 28.4444L1 39V5.22222C1 4.10242 1.48163 3.02848 2.33894 2.23666C3.19625 1.44484 4.35901 1 5.57143 1H28.4286C29.641 1 30.8038 1.44484 31.6611 2.23666C32.5184 3.02848 33 4.10242 33 5.22222V39Z"
                                                                            fill="#E5D6A0" stroke="#E5D6A0" stroke-width="2"
                                                                            stroke-linecap="round" stroke-linejoin="round"/>
                                                                    </svg>
                                                                </div>
                                                            @endif
                                                        @endauth
                                                        @guest
                                                            <div class="small_item_svg_wrapper add_to_bookmarks"
                                                                 data-id="{{ $business->id }}" data-type="business">
                                                                <svg width="20" height="26" viewBox="0 0 34 40" fill="none"
                                                                     xmlns="http://www.w3.org/2000/svg">
                                                                    <path
                                                                        d="M33 39L17 28.4444L1 39V5.22222C1 4.10242 1.48163 3.02848 2.33894 2.23666C3.19625 1.44484 4.35901 1 5.57143 1H28.4286C29.641 1 30.8038 1.44484 31.6611 2.23666C32.5184 3.02848 33 4.10242 33 5.22222V39Z"
                                                                        stroke="#E5D6A0" stroke-width="2" stroke-linecap="round"
                                                                        stroke-linejoin="round"/>
                                                                </svg>
                                                            </div>
                                                        @endguest

                                                    </div>
                                                    <p class="bookmark_info">Добавить в избранное</p>

                                                </div>
                                                <p class="investment_search_info1">Цена:</p>
                                                <p class="investment_search_info2">
                                                    {{ number_format($business->cost, 0, ',', ' ') }}<span>&#8381;</span>
                                                </p>
                                                <div class="investment_search_info_svg_wrapper">
                                                    <div class="info">
                                                        <p class="investment_search_info5">Город</p>
                                                        <p class="investment_search_info3"> {{ $business->city }}</p>
                                                    </div>

                                                    <div class="investment_search_info4_svg_wrapper">
                                                        <div class="investment_search_info4_svg">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M24 23.9999V20.9496C24 19.3316 23.3679 17.7798 22.2426 16.6357C21.1174 15.4916 19.5913 14.8489 18 14.8489H6C4.4087 14.8489 2.88258 15.4916 1.75736 16.6357C0.632141 17.7798 0 19.3316 0 20.9496V23.9999"
                                                                    fill="#E5D6A0"/>
                                                                <path
                                                                    d="M12.0007 12.2014C15.3144 12.2014 18.0007 9.47004 18.0007 6.10071C18.0007 2.73138 15.3144 0 12.0007 0C8.68702 0 6.00073 2.73138 6.00073 6.10071C6.00073 9.47004 8.68702 12.2014 12.0007 12.2014Z"
                                                                    fill="#E5D6A0"/>
                                                            </svg>

                                                        </div>
                                                        <p class="investment_search_info4">0</p>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="star_svg_wrapper hidden_star_wrapper">
                                            <div class="svg_wrapper">
                                                <svg width="19" height="20" viewBox="0 0 19 20" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M1 7.53424V1.78411C1 1.57615 1.08261 1.37671 1.22966 1.22966C1.37671 1.08261 1.57615 1 1.78411 1H17.4663C17.6742 1 17.8737 1.08261 18.0207 1.22966C18.1678 1.37671 18.2504 1.57615 18.2504 1.78411V7.53424C18.2504 15.7689 11.2614 18.4971 9.86593 18.9597C9.70993 19.0134 9.54045 19.0134 9.38445 18.9597C7.98895 18.4971 1 15.7689 1 7.53424Z"
                                                        stroke="#848993" stroke-width="1.5" stroke-linecap="round"
                                                        stroke-linejoin="round"/>
                                                    <path
                                                        d="M8.88194 11.6156H10.5801V11.4529C10.5891 10.5197 10.9027 10.0842 11.6107 9.62957C12.4486 9.09838 12.9952 8.39491 12.9952 7.27032C12.9952 5.59538 11.7317 4.61914 9.95283 4.61914C8.32633 4.61914 7.00004 5.52839 6.95972 7.44259H8.78785C8.81472 6.66256 9.3569 6.24621 9.94387 6.24621C10.5488 6.24621 11.0372 6.67692 11.0372 7.3421C11.0372 7.969 10.6115 8.38534 10.0604 8.75861C9.30761 9.26588 8.88642 9.77792 8.88194 11.4529V11.6156ZM9.76464 14.6783C10.3382 14.6783 10.8355 14.1662 10.84 13.5298C10.8355 12.9029 10.3382 12.3908 9.76464 12.3908C9.17318 12.3908 8.68479 12.9029 8.68927 13.5298C8.68479 14.1662 9.17318 14.6783 9.76464 14.6783Z"
                                                        fill="#848993"/>
                                                    <path
                                                        d="M8.87965 12.6719C8.67698 12.8656 8.57564 13.1515 8.57564 13.5299C8.57564 13.8947 8.67923 14.1784 8.8864 14.3811C9.09358 14.5792 9.38632 14.6783 9.76464 14.6783C10.134 14.6783 10.4222 14.577 10.6294 14.3743C10.8365 14.1672 10.9401 13.8857 10.9401 13.5299C10.9401 13.1606 10.8365 12.8768 10.6294 12.6787C10.4267 12.4805 10.1385 12.3814 9.76464 12.3814C9.37732 12.3814 9.08232 12.4782 8.87965 12.6719Z"
                                                        fill="#848993"/>
                                                </svg>
                                            </div>
                                            <div class="jq-ry-group-wrapper">
                                                <div class="jq-ry-normal-group jq-ry-group">
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59"
                                                         x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#848993"><polygon
                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59"
                                                         x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#848993"
                                                         style="margin-left: 0px;"><polygon
                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59"
                                                         x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#848993"
                                                         style="margin-left: 0px;"><polygon
                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59"
                                                         x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#848993"
                                                         style="margin-left: 0px;"><polygon
                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59"
                                                         x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#848993"
                                                         style="margin-left: 0px;"><polygon
                                                            points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                </div>
                                                <div class="active_star" style="width:107%">
                                                    <div class="jq-ry-rated-group jq-ry-group" style="width: 7rem">
                                                        <!--?xml version="1.0" encoding="utf-8"?-->
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                             viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                             width="32px" height="32px" fill="#FF8D4E"><polygon
                                                                points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                        <!--?xml version="1.0" encoding="utf-8"?-->
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                             viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                             width="32px" height="32px" fill="#FF8D4E" style="margin-left: 0px;"><polygon
                                                                points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                        <!--?xml version="1.0" encoding="utf-8"?-->
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                             viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                             width="32px" height="32px" fill="#FF8D4E" style="margin-left: 0px;"><polygon
                                                                points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                        <!--?xml version="1.0" encoding="utf-8"?-->
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                             viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                             width="32px" height="32px" fill="#FF8D4E" style="margin-left: 0px;"><polygon
                                                                points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                        <!--?xml version="1.0" encoding="utf-8"?-->
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                             viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve"
                                                             width="32px" height="32px" fill="#FF8D4E" style="margin-left: 0px;"><polygon
                                                                points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "></polygon></svg>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="about_project_progress_items_wrapper">
                                        <div class="about_project_progress_item open" id="open_div1">
                                            <h2 class="about_project_progress_item_title">Фото</h2>
                                            <div class="swiper_btn_slides_wrapper swiper_slider_special_second">

                                                <div class="swiper-button-prev"></div>
                                                <div class="swiper-container" id="first_swiper_container">
                                                    <div class="swiper-wrapper">
                                                        @if($add_images->count() == 1)
                                                            <img src="{{ asset('/storage/business_gallery\\').$add_images[0]['name'] }}"
                                                                 alt="">
                                                        @else
                                                            @foreach($add_images as $image)
                                                                <div class="swiper-slide">
                                                                    <div class="slider_slide_img">
                                                                        <img src="{{ asset('/storage/business_gallery\\').$image->name }}"
                                                                             alt="">
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endif



                                                    </div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                            </div>

                                            <div class="description_wrapper">
                                                <p class="description_title">Описание</p>
                                                <div class="small_item_show_more_text_wrapper">
                                                    <p class="small_item_show_more_text description_info" style="white-space: pre-line;display: table-cell;">
                                                        {{$business->description }}
                                                    </p>
                                                    <button class="read_more_btn" type="button">Читать полностью</button>
                                                </div>
                                            </div>
                                            @auth()
                                                <div class="form_btns_wrapper">
                                                    <button class="form_btn form_first_btn"><a
                                                            href="{{ route('business.purchase',$business->id) }}"> Связаться</a>
                                                    </button>
                                                    <button class="form_btn form_first_btn"><a
                                                            href="{{ route('business.sentence',$business->id) }}"> Предложения</a>
                                                    </button>
                                                </div>

                                            @endauth
                                            @guest()
                                                <div class="form_btns_wrapper">
                                                    <button class="form_btn form_first_btn"><a href="{{ route('login2') }}">
                                                            Зарегистрироватся </a></button>
                                                </div>
                                            @endauth


                                        </div>

                                    </div>


                                </form>


            @endif
        </div>
    </section>
</main>
{{--@auth()--}}
{{--    @if(isset($investment))--}}
{{--        @if($investment->user_id != auth()->user()->id)--}}
{{--            <div id="invest" class="modal @if(!session('invest_success')) hide_modal @endif">--}}
{{--                <div class="modal-content">--}}
{{--                    <span class="close">×</span>--}}
{{--                    <div class="service">--}}
{{--                        <div class="service_items_wrapper">--}}
{{--                            @if(session('invest_success'))--}}
{{--                                <label for="invest_price">{{ session('invest_success')  }}</label>--}}
{{--                            @else--}}
{{--                                <form action="{{ route('invest.store') }}" method="post" class="service_item">--}}
{{--                                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>--}}
{{--                                    <label for="invest_price">Сколько вы хотите инвестировать?</label>--}}
{{--                                    <input type="hidden" name="investment_id" value="{{ $investment->id }}">--}}
{{--                                    <input id="invest_price" name="invest_price" type="text"--}}
{{--                                           placeholder="Сумма инвестирования"/>--}}
{{--                                    <button class="service_item_link">Инвестировать</button>--}}
{{--                                </form>--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endif--}}
{{--    @endif--}}
{{--@endauth--}}

@include('include.footer')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="{{ asset('website/js/companies.js') }}"></script>
<script src="{{ asset('website/js/confirm.js') }}"></script>
