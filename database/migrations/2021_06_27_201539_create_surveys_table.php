<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->string('name');
            $table->string('date_foundation_company');
            $table->string('business_activity');
            $table->string('reason_for_sale');
            $table->string('link_video')->nullable();
            $table->integer('price');
            $table->string('field_of_activity');
            $table->string('organizational_and_legal_form');
            $table->string('other_activities_of_company')->nullable();
            $table->string('services_provided');
            $table->string('selling_product')   ;
            $table->string('amount_of_workers')->nullable();
            $table->string('number_of_management_personnel')->nullable();
            $table->string('object_in_which_the_activity_is_carried_out');
            $table->string('purpose_of_the_real_estate_object_where_activity_is_carried_out');
            $table->string('type_of_ownership');
            $table->string('floor')->nullable();
            $table->string('properties_for_sale')->nullable();
            $table->string('equipment_sold_at_salefloor')->nullable();
            $table->string('total_area');
            $table->string('availability_of_licenses_certificates')->nullable();
            $table->string('taxation_system');
            $table->string('average_check');
            $table->string('average_monthly_turnover');
            $table->string('salary_fund')->nullable();
            $table->string('rent')->nullable();
            $table->string('cost_of_utility_bills')->nullable();
            $table->string('general_expenses');
            $table->string('net_profit_per_month');
            $table->string('business_payback_period');
            $table->string('debts_and_fines');
            $table->string('address');
            $table->string('region_id');
            $table->string('city_id');
            $table->string('website')->nullable();
            $table->string('social_website')->nullable();
            $table->string('phone_number');
            $table->string('barring_calls')->nullable();
            $table->string('additional_information');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
