<?php
namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        User::insert(
            [
        		"name" => 'Техническая' ,
				"surname" => 'Поддержка',
				"fullname" => 'Техническая Поддержка',
				"phone" => '+746659806',
				"email" => "support@drafter.ru",
				"role_id" => 4,
				"email_verified_at" => null,
				"password" => '$2y$12$OtAgPAZn76iiWYMOAQm5cuTyf7OT4RyUcfawMqnn3JKSHigjrccZO',
				"two_factor_secret" => null,
				"two_factor_recovery_codes" =>null ,
				"remember_token" => null,
				"current_team_id" => null,
				"profile_photo_path" => null,
				"created_at" => '2021-05-31 20:14:04',
				"updated_at" => '2021-07-11 10:48:58',
				"status" => 1,
				"is_verified_email" => 1,
				"verified_token" => null ,
				"last_logout_date" => null,
            ]
        );
    }
}
